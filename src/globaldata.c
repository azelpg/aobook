/*$
aobook
Copyright (c) 2014-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * グローバルデータ
 *****************************************/

#include "mlk_gui.h"
#include "mlk_str.h"
#include "mlk_string.h"
#include "mlk_font.h"
#include "mlk_list.h"
#include "mlk_key.h"

#include "def_global.h"
#include "def_style.h"
#include "text.h"
#include "font.h"
#include "style.h"
#include "layout.h"
#include "bookmark.h"
#include "trid.h"

//--------------

extern const uint16_t g_menudata[];

//ショートカットキーデフォルト値
static const ShortcutKey g_shortcutkey_def[] =
{
	{TRID_MENU_FILE_OPEN, 'O' | MLK_ACCELKEY_CTRL},
	{TRID_MENU_FILE_NEXTFILE, 'N' | MLK_ACCELKEY_CTRL},
	{TRID_MENU_FILE_PREVFILE, 'P' | MLK_ACCELKEY_CTRL},
	{TRID_MENU_FILE_RELOAD, MKEY_F5},
	{TRID_MENU_PAGE_NEXT, MKEY_LEFT},
	{TRID_MENU_PAGE_PREV, MKEY_RIGHT},
	{TRID_MENU_PAGE_TOP, MKEY_HOME},
	{TRID_MENU_PAGE_BOTTOM, MKEY_END},
	{TRID_MENU_PAGE_PAGENO, 'P'},
	{TRID_MENU_PAGE_LINENO, 'L'},
	{TRID_MENU_PAGE_CAPTION, 'C'},
	{TRID_MENU_PAGE_ADD_BOOKMARK_GLOBAL, 'G' | MLK_ACCELKEY_CTRL},
	{TRID_MENU_PAGE_ADD_BOOKMARK_LOCAL, 'L' | MLK_ACCELKEY_CTRL},
	{TRID_MENU_VIEW_BOOKMARK, 'B'},
	{TRID_MENU_OPT_STYLE, 'S'},
	{TRID_MENU_OPT_ENV, 'E'},
	{0,0}
};

//--------------


//============================
// sub
//============================


/* 現在のファイルを閉じる時 */

static void _on_close_file(GlobalData *p)
{
	char m[32];

	if(GLOBAL_IS_EMPTY_TEXT) return;

	//履歴に行位置をセット

	mIntToStr(m, LayoutGetPageLineNo(p->curpage));

	mStrReplaceSplitText(&p->strRecentFile[0], '\t', 2, m);
}

/* ファイル履歴配列にセット
 *
 * filename: ファイル名部分のテキスト
 * strtext: セットするテキスト */

static void _set_recentfile(mStr *array,int arraynum,const char *filename,mStr *strtext)
{
	mStr str = MSTR_INIT;
	int i,find;

	//履歴内に同じファイルがあるか

	for(i = 0, find = -1; i < arraynum; i++)
	{
		mStrGetSplitText(&str, array[i].buf, '\t', 0);
	
		if(mStrPathCompareEq(&str, filename))
		{
			find = i;
			break;
		}
	}

	mStrFree(&str);

	//セット (find = -1 で追加、それ以外は置換)

	mStrArraySetRecent(array, arraynum, find, strtext->buf);
}

/* ショートカットキー設定で除外するメニュー項目 */

static int _is_exclude_sckey(int id)
{
	if(id >= 0xfff0) return 1;

	id &= 0x3fff;

	return (id < TRID_MENU_SCKEY_TOP || id >= TRID_MENU_SCKEY_END
		|| id == TRID_MENU_FILE_RECENTFILE || id == TRID_MENU_VIEW_STYLE
		|| id == TRID_MENU_OPT_ABOUT);
}

/* メニューデータから編集用のショートカットキーデータ作成 */

static void _create_sckey_data(GlobalData *p)
{
	const uint16_t *pmenu;
	ShortcutKey *buf;
	int num;

	//個数計算

	for(pmenu = g_menudata, num = 0; *pmenu != 0xffff; pmenu++)
	{
		if(!_is_exclude_sckey(*pmenu))
			num++;
	}

	num++;

	//確保

	p->sckey_num = num;

	p->sckey = buf = (ShortcutKey *)mMalloc0(sizeof(ShortcutKey) * num);
	if(!buf) return;

	//ID セット

	for(pmenu = g_menudata; *pmenu != 0xffff; pmenu++)
	{
		if(!_is_exclude_sckey(*pmenu))
		{
			buf->id = *pmenu & 0x3fff;
			buf++;
		}
	}
}


//============================


/** グローバルデータ解放 */

void GlobalDataFree(void)
{
	GlobalData *p = g_globaldat;

	//スタイル

	StyleWork_free(p->style);

	//レイアウトデータ

	LayoutFree(p->layout);

	//しおりデータ

	mListDeleteAll(&p->list_bkm_global);
	mListDeleteAll(&p->list_bkm_local);

	//mStr

	mStrFree(&p->strFileName);
	mStrFree(&p->strOpenDir);
	mStrFree(&p->strBkmLocalDir);
	
	mStrArrayFree(p->strRecentFile, RECENTFILE_NUM);
	mStrArrayFree(p->strTool, TOOLITEM_NUM);

	//

	mFree(p->textbuf.buf);
	mFree(p->sckey);

	mFontFree(p->font_kenten);

	//

	mFree(p);
}

/** グローバルデータ確保 */

mlkbool GlobalDataNew(void)
{
	GlobalData *p;
	mStr str = MSTR_INIT;

	//確保

	p = (GlobalData *)mMalloc0(sizeof(GlobalData));
	if(!p) return FALSE;

	g_globaldat = p;

	//スタイル

	p->style = (StyleWork *)mMalloc0(sizeof(StyleWork));

	//レイアウトデータ

	p->layout = LayoutAlloc();
	if(!p->layout) return FALSE;

	//ショートカットキーデータ

	_create_sckey_data(p);

	//しおり

	Bookmark_init();

	//圏点用フォント

	mGuiGetPath_data(&str, "kenten.otf");

	p->font_kenten = mFontCreate_file(FontGetSystem(), str.buf, 0);

	mStrFree(&str);

	return TRUE;
}

/** アプリ終了時 */

void GlobalFinish(void)
{
	_on_close_file(GDAT);
}

/** ショートカットキーをデフォルト値にセット (起動時) */

void GlobalSetSckeyDefault(void)
{
	const ShortcutKey *ps;
	ShortcutKey *pd;
	uint32_t id;

	for(ps = g_shortcutkey_def; ps->id; ps++)
	{
		id = ps->id;
	
		for(pd = GDAT->sckey; pd->id; pd++)
		{
			if(pd->id == id)
			{
				pd->key = ps->key;
				break;
			}
		}
	}
}

/** テキストデータ (内部データ) をセット
 *
 * buf: NULL で空にする */

void GlobalSetTextData(mBufSize *buf)
{
	GlobalData *p = GDAT;

	_on_close_file(p);

	//現在のデータを解放

	if(p->textbuf.buf)
	{
		mFree(p->textbuf.buf);
		
		p->textbuf.buf = NULL;

		//ページ・タイトルデータ解放

		LayoutFreeData(p->layout);

		p->curpage = NULL;
	}

	//セット

	if(buf)
		p->textbuf = *buf;
}

/** ファイルの履歴を追加
 *
 * code: 文字コード
 * line: 行番号 */

void GlobalAddRecentFile(const char *filename,int code,int line)
{
	mStr str = MSTR_INIT;

	//ファイル名 + '\t' + 文字コード名 + '\t' + 行数

	mStrSetFormat(&str, "%s\t%s\t%d",
		filename, TextGetCodeName(code), line);

	_set_recentfile(GDAT->strRecentFile, RECENTFILE_NUM, filename, &str);

	mStrFree(&str);
}

/** ファイル履歴から情報取得
 *
 * no: 履歴のインデックス位置
 * strfname: ファイル名のパスが入る
 * pcode,pline: 文字コードと行番号が入る */

void GlobalGetRecentFileInfo(int no,mStr *strfname,int *pcode,int *pline)
{
	mStr *src,str = MSTR_INIT;
	int i;

	src = GDAT->strRecentFile + no;

	//ファイル名

	mStrGetSplitText(strfname, src->buf, '\t', 0);

	//文字コード (文字列 -> 番号)

	mStrGetSplitText(&str, src->buf, '\t', 1);

	*pcode = -1;

	for(i = 0; i < TEXT_CODE_NUM; i++)
	{
		if(mStrCompareEq_case(&str, TextGetCodeName(i)))
		{
			*pcode = i;
			break;
		}
	}

	//行番号

	mStrGetSplitText(&str, src->buf, '\t', 2);

	*pline = mStrToInt(&str);

	//

	mStrFree(&str);
}
