/*$
aobook
Copyright (c) 2014-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/********************************
 * テキスト変換 (UTF-16BE)
 ********************************/

#include "mlk.h"
#include "mlk_charset.h"

#include "text.h"


//---------------

//文字コード判定の最大バイト数
#define _CHECK_MAXSIZE  1024

static const char *g_codename[] = {
	"Shift-JIS", "EUC-JP", "UTF-8", "UTF-16LE", "UTF-16BE"
};

//---------------


//===========================
// 文字コード判定
//===========================


/* UTF-8 のチェック */

static int _check_utf8(uint8_t *buf,uint32_t size)
{
	uint8_t *p = buf,c;
	int i,len;

	while(size && p - buf < _CHECK_MAXSIZE)
	{
		c = *(p++);
		size--;

		if(c < 0x80)
			//ASCII
			continue;
		else if(c <= 0xc1 || c >= 0xfe)
			//この範囲は、UTF-8 では先頭バイトに来ない
			return FALSE;
		else if(c <= 0xfd)
		{
			//---- 2byte 以上
			
			//2バイト目以降のバイト数
			
			if(c >= 0xfc) len = 5;
			else if(c >= 0xf8) len = 4;
			else if(c >= 0xf0) len = 3;
			else if(c >= 0xe0) len = 2;
			else len = 1;

			//データが足りない

			if(size < len) return FALSE;

			//2バイト目以降が 0x80-0xBF の範囲内か

			for(i = len; i; i--, p++)
			{
				if(*p < 0x80 || *p > 0xbf) return FALSE;
			}

			size -= len;
		}
	}

	return TRUE;
}

/* Shift-JIS のチェック */

static int _check_sjis(uint8_t *buf,uint32_t size)
{
	uint8_t *p = buf,c,c2;

	while(size && p - buf < _CHECK_MAXSIZE)
	{
		c = *(p++);
		size--;

		if(c < 0x80 || (c >= 0xa1 && c <= 0xdf))
			//ASCII/半角カナ
			continue;
		else if((c >= 0x81 && c <= 0x9f) || (c >= 0xe0 && c <= 0xfc))
		{
			//2byte文字

			if(size < 1) return FALSE;

			c2 = *(p++);
			size--;

			if(!((c2 >= 0x40 && c2 <= 0x7e) || (c2 >= 0x80 && c2 <= 0xfc)))
				return FALSE;
		}
		else
			return FALSE;
	}

	return TRUE;
}

/* EUC-JP のチェック */

static int _check_eucjp(uint8_t *buf,uint32_t size)
{
	uint8_t *p = buf,c,c2;
	int ret = 1;

	while(size && p - buf < _CHECK_MAXSIZE)
	{
		c = *(p++);
		size--;

		if(c < 0x80)
			//ASCII
			continue;
		else if(c >= 0xa1 && c <= 0xfe)
		{
			//漢字など (2byte)

			if(size < 1) return FALSE;

			c2 = *(p++);
			size--;

			if(c2 < 0xa1 || c2 > 0xfe) return FALSE;
		}
		else if(c == 0x8e)
		{
			//半角カナ (2byte)

			if(size < 1) return FALSE;

			c2 = *(p++);
			size--;

			if(c2 < 0xa1 || c2 > 0xdf) return FALSE;
		}
		else if(c == 0x8f)
		{
			//補助漢字 (3byte)

			if(size < 2) return FALSE;

			if((*p >= 0xa1 && *p <= 0xfe) || (p[1] >= 0xa1 && p[1] <= 0xfe))
			{
				p += 2;
				size -= 2;
			}
			else
				return FALSE;
		}
		else
			return FALSE;
	}

	return ret;
}

/* 文字コード自動判定
 *
 * return: 文字コード */

static int _auto_charcode(uint8_t *buf,uint32_t size)
{
	uint8_t *p = buf;
	int sjis,euc;

	//------ BOM 判定

	//UTF-16

	if(size >= 2)
	{
		if(p[0] == 0xff && p[1] == 0xfe)
			return TEXT_CODE_UTF16LE;
		else if(p[0] == 0xfe && p[1] == 0xff)
			return TEXT_CODE_UTF16BE;
	}

	//UTF-8

	if(size >= 3 && p[0] == 0xef && p[1] == 0xbb && p[2] == 0xbf)
		return TEXT_CODE_UTF8;

	//------ UTF8/Shift-JIS/EUC/UTF16LE 判定

	if(_check_utf8(buf, size))
		//UTF-8
		return TEXT_CODE_UTF8;
	else
	{
		//UTF-8 ではない
	
		sjis = _check_sjis(buf, size);
		euc = _check_eucjp(buf, size);

		if(!sjis && !euc)
			return TEXT_CODE_UTF16LE;
		else if(sjis && !euc)
			return TEXT_CODE_SHIFTJIS;
		else
			return TEXT_CODE_EUCJP;
	}
}


//=======================
// メイン
//=======================


/** テキストを UTF-16BE に変換
 *
 * code: 文字コード (負の値で自動判定)
 * return: ソースの文字コード (-1 でエラー) */

int TextConvert_to_utf16be(mBufSize *src,mBufSize *dst,int code)
{
	void *buf;
	int size;

	//文字コード自動判定

	if(code < 0)
		code = _auto_charcode((uint8_t *)src->buf, src->size);

	//変換 (終端に 0 は追加しない)

	buf = mConvertCharset(src->buf, src->size,
		TextGetCodeName_def(code), "UTF-16BE", &size, 0);

	if(!buf) return -1;

	//

	dst->buf = buf;
	dst->size = size;

	return code;
}

/** 文字コード名取得 (aobook での表示用)
 *
 * code: 負の値で、自動 */

const char *TextGetCodeName(int code)
{
	if(code < 0)
		return "auto";
	else
		return g_codename[code];
}

/** 文字コード名取得 (厳密な定義名)
 *
 * ツールでのコード名指定時にも使う。 */

const char *TextGetCodeName_def(int code)
{
	if(code == TEXT_CODE_SHIFTJIS)
		return "CP932";
	else
		return g_codename[code];
}

