/*$
aobook
Copyright (c) 2014-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * 環境設定のページ
 *****************************************/

#include "mlk_gui.h"
#include "mlk_widget_def.h"
#include "mlk_widget.h"
#include "mlk_window.h"
#include "mlk_event.h"
#include "mlk_str.h"
#include "mlk_columnitem.h"

#include "mlk_pager.h"
#include "mlk_button.h"
#include "mlk_label.h"
#include "mlk_lineedit.h"
#include "mlk_inputaccel.h"
#include "mlk_conflistview.h"

#include "def_global.h"
#include "trid.h"

#include "pv_envdlg.h"


//-----------------

//ボタン動作
typedef struct
{
	mConfListView *clv;
}_pagedata_button;

//ツール
typedef struct
{
	mListView *lv;
}_pagedata_tool;

//ショートカットキー
typedef struct
{
	mConfListView *clv;
}_pagedata_sckey;

//-----------------

enum
{
	TRID_BUTTON_ACT = 100,
	TRID_BUTTON_NAME,

	TRID_TOOL_ADD = 200,
	TRID_TOOL_DEL,
	TRID_TOOL_EDIT,
	TRID_TOOL_TITLE = 220,
	TRID_TOOL_NAME,
	TRID_TOOL_COMMAND,
	TRID_TOOL_SHORTCUTKEY,
	TRID_TOOL_COMMAND_HELP,

	TRID_SCKEY_CLEAR = 300,
	TRID_SCKEY_ALL_CLEAR
};

enum
{
	WID_SCKEY_CLEAR = 100,
	WID_SCKEY_ALL_CLEAR
};

//-----------------


//**********************************
// ツール編集ダイアログ
//**********************************


typedef struct
{
	MLK_DIALOG_DEF

	mLineEdit *le_name,
		*le_cmd;
	mInputAccel *inputacc;
}_dialog_tool;


/* イベント */

static int _tooldialog_event(mWidget *wg,mEvent *ev)
{
	_dialog_tool *p = (_dialog_tool *)wg;

	if(ev->type == MEVENT_NOTIFY)
	{
		switch(ev->notify.id)
		{
			//キーをクリア
			case 100:
				mInputAccelSetKey(p->inputacc, 0);
				return 1;

			//OK
			case MLK_WID_OK:
				//名前,コマンドが空なら終了しない
				if(mLineEditIsEmpty(p->le_name)
					|| mLineEditIsEmpty(p->le_cmd))
					return 1;
				break;
		}
	}

	return mDialogEventDefault_okcancel(wg, ev);
}

/* ダイアログ作成 */

static _dialog_tool *_tooldialog_create(mWindow *parent,const char *defstr)
{
	_dialog_tool *p;
	mWidget *ct,*ct2;
	mStr str = MSTR_INIT;

	p = (_dialog_tool *)mDialogNew(parent, sizeof(_dialog_tool), MTOPLEVEL_S_DIALOG_NORMAL);
	if(!p) return NULL;

	p->wg.event = _tooldialog_event;
	p->ct.sep = 11;

	mContainerSetPadding_same(MLK_CONTAINER(p), 8);

	mToplevelSetTitle(MLK_TOPLEVEL(p), MLK_TR(TRID_TOOL_TITLE));

	//-------

	ct = mContainerCreateGrid(MLK_WIDGET(p), 2, 7, 7, MLF_EXPAND_W, 0);

	//ツール名

	mLabelCreate(ct, MLF_RIGHT | MLF_MIDDLE, 0, 0, MLK_TR(TRID_TOOL_NAME));

	p->le_name = mLineEditCreate(ct, 0, MLF_EXPAND_W, 0, 0);

	//コマンド

	mLabelCreate(ct, MLF_RIGHT | MLF_MIDDLE, 0, 0, MLK_TR(TRID_TOOL_COMMAND));

	p->le_cmd = mLineEditCreate(ct, 0, MLF_EXPAND_W, 0, 0);

	mWidgetNew(ct, 0);
	mLabelCreate(ct, 0, 0, MLABEL_S_BORDER, MLK_TR(TRID_TOOL_COMMAND_HELP));

	//ショートカットキー

	mLabelCreate(ct, MLF_RIGHT | MLF_MIDDLE, 0, 0, MLK_TR(TRID_TOOL_SHORTCUTKEY));

	ct2 = mContainerCreateHorz(ct, 6, MLF_EXPAND_W, 0);

	p->inputacc = mInputAccelCreate(ct2, 0, MLF_EXPAND_W | MLF_MIDDLE, 0, 0);

	mButtonCreate(ct2, 100, 0, 0, 0, MLK_TR(TRID_SCKEY_CLEAR));

	//----- OK/キャンセル

	mContainerCreateButtons_okcancel(MLK_WIDGET(p), MLK_MAKE32_4(0,8,0,0));

	//----- 初期値

	if(defstr)
	{
		//ツール名

		mStrGetSplitText(&str, defstr, '\t', 0);
		mLineEditSetText(p->le_name, str.buf);

		//コマンド

		mStrGetSplitText(&str, defstr, '\t', 1);
		mLineEditSetText(p->le_cmd, str.buf);

		//ショートカットキー

		mStrGetSplitText(&str, defstr, '\t', 2);
		mInputAccelSetKey(p->inputacc, mStrToInt(&str));

		mStrFree(&str);
	}

	return p;
}

/* 終了時 */

static void _tooldialog_ok(_dialog_tool *p,mStr *strdst)
{
	mStr str = MSTR_INIT;

	//ツール名

	mLineEditGetTextStr(p->le_name, strdst);
	mStrAppendChar(strdst, '\t');

	//コマンド

	mLineEditGetTextStr(p->le_cmd, &str);

	mStrAppendStr(strdst, &str);
	mStrAppendChar(strdst, '\t');

	//ショートカットキー

	mStrAppendInt(strdst, mInputAccelGetKey(p->inputacc));

	mStrFree(&str);
}

/** ダイアログ実行 */

static mlkbool _run_tool_dialog(mWindow *parent,mStr *strdst)
{
	_dialog_tool *p;
	int ret;

	p = _tooldialog_create(parent, strdst->buf);
	if(!p) return FALSE;

	mWindowResizeShow_initSize(MLK_WINDOW(p));

	ret = mDialogRun(MLK_DIALOG(p), FALSE);
	if(ret)
		_tooldialog_ok(p, strdst);

	mWidgetDestroy(MLK_WIDGET(p));

	return ret;
}


//**********************************
// ボタン動作
//**********************************


/* 値取得関数 */

static int _button_getvalue(int type,intptr_t no,mConfListViewValue *val,void *param)
{
	((EnvOptData *)param)->bttact[no] = val->select;

	return 0;
}

/* データ取得 */

static mlkbool _button_getdata(mPager *p,void *pagedat,void *dst)
{
	_pagedata_button *pd = (_pagedata_button *)pagedat;

	mConfListView_getValues(pd->clv, _button_getvalue, dst);

	return TRUE;
}

/* データセット */

static mlkbool _button_setdata(mPager *p,void *pagedat,void *src)
{
	_pagedata_button *pd = (_pagedata_button *)pagedat;
	EnvOptData *ps = (EnvOptData *)src;
	mConfListViewValue val;
	int i;

	for(i = 0; i < BUTTONACT_BTT_NUM; i++)
	{
		val.select = ps->bttact[i];
		
		mConfListView_setItemValue_atIndex(pd->clv, i, &val);
	}

	return TRUE;
}

/** "ボタン操作" ページ作成 */

mlkbool EnvDlg_createPage_button(mPager *p,mPagerInfo *info)
{
	_pagedata_button *pd;
	mConfListView *clv;
	int i;

	pd = (_pagedata_button *)mMalloc0(sizeof(_pagedata_button));
	if(!pd) return FALSE;

	info->pagedat = pd;
	info->setdata = _button_setdata;
	info->getdata = _button_getdata;

	//

	pd->clv = clv = mConfListViewNew(MLK_WIDGET(p), 0, 0);

	mWidgetSetInitSize_fontHeightUnit(MLK_WIDGET(clv), 22, 0);

	for(i = 0; i < BUTTONACT_BTT_NUM; i++)
	{
		mConfListView_addItem_select(clv, MLK_TR(TRID_BUTTON_NAME + i), 0,
			MLK_TR(TRID_BUTTON_ACT), FALSE, i);
	}

	mListViewSetColumnWidth_auto(MLK_LISTVIEW(clv), 0);

	return TRUE;
}


//**********************************
// ツール
//**********************************


/* データセット */

static mlkbool _tool_setdata(mPager *p,void *pagedat,void *src)
{
	_pagedata_tool *pd = (_pagedata_tool *)pagedat;
	mStr *pstr,str = MSTR_INIT;
	int i;

	pstr = ((EnvOptData *)src)->strtool;

	for(i = 0; i < TOOLITEM_NUM; i++)
	{
		if(mStrIsEmpty(pstr + i)) break;

		mStrGetSplitText(&str, pstr[i].buf, '\t', 0);

		mListViewAddItem_text_copy(pd->lv, str.buf);
	}

	mStrFree(&str);

	return TRUE;
}

/* 追加/編集 */

static void _tool_addedit(mPager *p,_pagedata_tool *pd,mlkbool is_edit)
{
	EnvOptData *penv;
	mColumnItem *pi;
	mStr *pstr,str = MSTR_INIT;
	int n;

	if(!is_edit)
	{
		//追加

		pi = NULL;

		n = mListViewGetItemNum(pd->lv);
		if(n == TOOLITEM_NUM) return;
	}
	else
	{
		//編集
		
		pi = mListViewGetFocusItem(pd->lv);
		if(!pi) return;

		n = mListViewGetItemIndex(pd->lv, pi);
	}

	//

	penv = (EnvOptData *)mPagerGetDataPtr(p);

	pstr = penv->strtool + n;

	//ダイアログ

	if(!_run_tool_dialog(p->wg.toplevel, pstr))
		return;

	//リスト

	mStrGetSplitText(&str, pstr->buf, '\t', 0);

	if(pi)
		mListViewSetItemText_copy(pd->lv, pi, str.buf);
	else
	{
		pi = mListViewAddItem_text_copy(pd->lv, str.buf);

		mListViewSetFocusItem(pd->lv, pi);
	}

	mStrFree(&str);

	//

	penv->fchange_tool = TRUE;
}

/* 削除 */

static void _tool_del(mPager *p,_pagedata_tool *pd)
{
	EnvOptData *penv;
	mColumnItem *pi;

	pi = mListViewGetFocusItem(pd->lv);
	if(!pi) return;

	penv = (EnvOptData *)mPagerGetDataPtr(p);

	//データを詰める

	mStrArrayShiftUp(penv->strtool,
		mListViewGetItemIndex(pd->lv, pi),
		TOOLITEM_NUM - 1);

	penv->fchange_tool = TRUE;

	//アイテム削除

	mListViewDeleteItem_focus(pd->lv);
}

/* イベント */

static int _tool_event(mPager *p,mEvent *ev,void *pagedat)
{
	if(ev->type == MEVENT_NOTIFY)
	{
		switch(ev->notify.id)
		{
			//追加
			case TRID_TOOL_ADD:
				_tool_addedit(p, (_pagedata_tool *)pagedat, FALSE);
				break;
			//削除
			case TRID_TOOL_DEL:
				_tool_del(p, (_pagedata_tool *)pagedat);
				break;
			//編集
			case TRID_TOOL_EDIT:
				_tool_addedit(p, (_pagedata_tool *)pagedat, TRUE);
				break;
		}
	
		return 1;
	}

	return 0;
}

/** "ツール" ページ作成 */

mlkbool EnvDlg_createPage_tool(mPager *p,mPagerInfo *info)
{
	_pagedata_tool *pd;
	mWidget *ct;
	int i;

	pd = (_pagedata_tool *)mMalloc0(sizeof(_pagedata_tool));
	if(!pd) return FALSE;

	info->pagedat = pd;
	info->setdata = _tool_setdata;
	info->event = _tool_event;

	//-----

	//ボタン

	ct = mContainerCreateHorz(MLK_WIDGET(p), 2, 0, MLK_MAKE32_4(0,0,0,6));

	for(i = 0; i < 3; i++)
		mButtonCreate(ct, TRID_TOOL_ADD + i, 0, 0, 0, MLK_TR(TRID_TOOL_ADD + i));

	//リスト

	pd->lv = mListViewCreate(MLK_WIDGET(p), 0, MLF_EXPAND_WH, 0,
		MLISTVIEW_S_AUTO_WIDTH, MSCROLLVIEW_S_HORZVERT_FRAME);

	return TRUE;
}


//**********************************
// ショートカットキー
//**********************************


/* 値取得関数 */

static int _sckey_getvalue(int type,intptr_t index,mConfListViewValue *val,void *param)
{
	ShortcutKey *buf = (ShortcutKey *)param;

	buf[index].key = val->key;

	return 0;
}

/* データ取得 */

static mlkbool _sckey_getdata(mPager *p,void *pagedat,void *dst)
{
	_pagedata_sckey *pd = (_pagedata_sckey *)pagedat;

	mConfListView_getValues(pd->clv, _sckey_getvalue, ((EnvOptData *)dst)->sckey);

	return TRUE;
}

/* データセット */

static mlkbool _sckey_setdata(mPager *p,void *pagedat,void *src)
{
	_pagedata_sckey *pd = (_pagedata_sckey *)pagedat;
	ShortcutKey *sckey;
	mConfListView *clv;
	mColumnItem *pi;
	mConfListViewValue val;

	clv = pd->clv;
	sckey = ((EnvOptData *)src)->sckey;

	pi = mListViewGetTopItem(MLK_LISTVIEW(clv));

	for(; pi; pi = MLK_COLUMNITEM(pi->i.next), sckey++)
	{
		val.key = sckey->key;
		
		mConfListView_setItemValue(clv, pi, &val);
	}

	return TRUE;
}

/* イベント */

static int _sckey_event(mPager *p,mEvent *ev,void *pagedat)
{
	if(ev->type == MEVENT_NOTIFY)
	{
		_pagedata_sckey *pd = (_pagedata_sckey *)pagedat;
	
		switch(ev->notify.id)
		{
			//キーをクリア
			case WID_SCKEY_CLEAR:
				{
				mConfListViewValue val;

				val.key = 0;

				mConfListView_setItemValue(pd->clv, NULL, &val);
				}
				break;
			//すべてクリア
			case WID_SCKEY_ALL_CLEAR:
				mConfListView_clearAll_accel(pd->clv);
				break;
		}
	
		return 1;
	}

	return 0;
}

/** "ショートカットキー" ページ作成 */

mlkbool EnvDlg_createPage_sckey(mPager *p,mPagerInfo *info)
{
	_pagedata_sckey *pd;
	mWidget *ct;
	mConfListView *clv;
	ShortcutKey *psc;
	int index,i;

	pd = (_pagedata_sckey *)mMalloc0(sizeof(_pagedata_sckey));
	if(!pd) return FALSE;

	info->pagedat = pd;
	info->setdata = _sckey_setdata;
	info->getdata = _sckey_getdata;
	info->event = _sckey_event;

	//----- リスト

	pd->clv = clv = mConfListViewNew(MLK_WIDGET(p), 0, 0);

	//アイテム追加

	MLK_TRGROUP(TRGROUP_ID_MENU);

	for(psc = GDAT->sckey, index = 0; psc->id; psc++, index++)
		mConfListView_addItem_accel(clv, MLK_TR(psc->id), 0, index);

	MLK_TRGROUP(TRGROUP_ID_DLG_ENV);

	//

	mListViewSetColumnWidth_auto(MLK_LISTVIEW(clv), 0);

	//--- ボタン

	ct = mContainerCreateHorz(MLK_WIDGET(p), 4, MLF_RIGHT, MLK_MAKE32_4(0,8,0,0));

	for(i = 0; i < 2; i++)
		mButtonCreate(ct, WID_SCKEY_CLEAR + i, 0, 0, 0, MLK_TR(TRID_SCKEY_CLEAR + i));

	return TRUE;
}

