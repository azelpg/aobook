/*$
aobook
Copyright (c) 2014-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/***********************************
 * レイアウト操作関数
 ***********************************/

#include <string.h>

#include "mlk_gui.h"
#include "mlk_widget_def.h"
#include "mlk_popup_progress.h"
#include "mlk_list.h"
#include "mlk_font.h"
#include "mlk_buf.h"
#include "mlk_pixbuf.h"

#include "def_global.h"
#include "def_style.h"
#include "style.h"
#include "layout.h"
#include "pv_layout.h"


#define HEIGHTBUF_SIZE  (0x10000 / 8)


//==========================
// sub
//==========================


/* LayoutWork を解放 */

static void _layoutwork_free(LayoutWork *p)
{
	mFree(p->u32_nohead.buf);
	mFree(p->u32_nobottom.buf);
	mFree(p->u32_hanging.buf);
	mFree(p->u32_nosep.buf);
	mFree(p->u32_replace.buf);

	mBufFree(&p->buf_title);

	mFree(p);
}

/* LayoutWork を作成
 *
 * img: 描画時の描画先。NULL で最初のレイアウトのみ。 */

static LayoutWork *_create_layoutwork(LayoutDat *info,mPixbuf *img)
{
	LayoutWork *p;
	StyleWork *style;
	StyleDef *st;
	int linesp;

	style = GDAT->style;
	st = &style->b;

	//

	p = (LayoutWork *)mMalloc0(sizeof(LayoutWork));
	if(!p) return NULL;

	p->text = (uint8_t *)GDAT->textbuf.buf;
	p->buf_hflags = info->buf_hflags;
	p->img = img;
	p->style = style;
	p->stdef = st;

	p->u32_nohead.buf = StyleDef_createLayoutChars_nohead(st, &p->u32_nohead.len);
	p->u32_nobottom.buf = StyleDef_createLayoutChars_nobottom(st, &p->u32_nobottom.len);
	p->u32_hanging.buf = StyleDef_createLayoutChars_hanging(st, &p->u32_hanging.len);
	p->u32_nosep.buf = StyleDef_createLayoutChars_nosep(st, &p->u32_nosep.len);
	p->u32_replace.buf = StyleDef_createLayoutChars_replace(st, &p->u32_replace.len);

	p->font_main = style->font_main;
	p->font_ruby = style->font_ruby;
	p->font_bold = style->font_bold;
	p->font_kenten = (GDAT->font_kenten)? GDAT->font_kenten: p->font_ruby;

	p->fontmain_h = mFontGetVertHeight(p->font_main);
	p->fontbold_h = mFontGetVertHeight(p->font_bold);
	p->fontruby_h = mFontGetVertHeight(p->font_ruby);
	p->fontkenten_h = mFontGetVertHeight(p->font_kenten);

	if(img)
		p->pixcol_text = mRGBtoPix(st->col_text);

	//

	linesp = (int)(p->fontmain_h * (st->line_space / 100.0) + 0.5);

	p->line_width = p->fontmain_h + linesp;

	p->pageW = st->lines * p->line_width;
	p->pageH = p->fontmain_h * st->chars + st->char_space * (st->chars - 1);

	p->text_right_x = p->pageW - p->line_width;

	//最初のレイアウト時

	if(!img)
		p->plist_title = &info->list_title;

	return p;
}

/* ページ位置の補正 */

static PageInfoItem *_page_adjust(LayoutDat *p,PageInfoItem *pi)
{
	if(!pi)
		//NULL なら終端
		return LayoutGetPage_homeEnd(p, TRUE);
	else if(GDAT->style->b.pages == 2 && pi && (pi->pageinfo.pageno & 1))
		//見開きの場合、奇数位置なら一つ戻る
		return (PageInfoItem *)pi->i.prev;
	else
		return pi;
}


//==========================
// ページ操作
//==========================


/** 先頭/終端のページ取得 */

PageInfoItem *LayoutGetPage_homeEnd(LayoutDat *p,mlkbool end)
{
	PageInfoItem *pi;

	pi = (PageInfoItem *)((end)? p->list_page.bottom: p->list_page.top);

	return (pi)? _page_adjust(p, pi): NULL;
}

/** ページ番号からページ取得 */

PageInfoItem *LayoutGetPage_pageno(LayoutDat *p,int page)
{
	mListItem *pi;
	int i;

	for(i = 0, pi = p->list_page.top; pi && i < page; i++, pi = pi->next);

	return _page_adjust(p, (PageInfoItem *)pi);
}

/** 行番号からページを取得 */

PageInfoItem *LayoutGetPage_lineno(LayoutDat *p,int line,mlkbool wrap_top)
{
	PageInfoItem *pi,*next;

	//wrap_top: 行番号指定の場合 TRUE、ページ位置の代わりとして使う場合は FALSE。
	// 行番号指定の場合、その行の先頭からページが見えるように。
	// ページ位置の代わりとして使う場合、その行が折り返している場合は、
	// 折り返しの先頭を含まない方がページ位置として正しい。
	// (複数ページにまたがって折り返している場合は、折り返し後の最初のページとなる)

	for(pi = (PageInfoItem *)p->list_page.top; pi; pi = next)
	{
		next = (PageInfoItem *)pi->i.next;

		//次がない = 最後のページ。
		//各ページの先頭の行番号から範囲を検索

		if(!next
			|| (pi->pageinfo.lineno <= line && line < next->pageinfo.lineno)
			|| (wrap_top && line == next->pageinfo.lineno && next->pageinfo.wrap_num))
				//その行が次のページに折り返している場合、前のページを指定
			break;
	}

	return _page_adjust(p, pi);
}

/** 指定方向に指定数移動したページを取得 */

PageInfoItem *LayoutGetPage_move(LayoutDat *p,PageInfoItem *pi,int dir)
{
	int i;

	dir *= GDAT->style->b.pages;

	if(dir < 0)
	{
		//前方向
		for(i = -dir; pi->i.prev && i > 0; i--, pi = (PageInfoItem *)pi->i.prev);
	}
	else
	{
		//次方向
		for(i = dir; pi->i.next && i > 0; i--, pi = (PageInfoItem *)pi->i.next);
	}

	return _page_adjust(p, pi);
}

/** ページ番号取得 */

int LayoutGetPageNo(PageInfoItem *pi)
{
	return (pi)? pi->pageinfo.pageno: 0;
}

/** ページの先頭のテキスト行番号取得 */

int LayoutGetPageLineNo(PageInfoItem *pi)
{
	return (pi)? pi->pageinfo.lineno: 0;
}


//=========================
//
//=========================


/** レイアウト情報解放 (再レイアウトごと) */

void LayoutFreeData(LayoutDat *p)
{
	mListDeleteAll(&p->list_page);
	mListDeleteAll(&p->list_title);
}

/** レイアウトデータ解放 */

void LayoutFree(LayoutDat *p)
{
	LayoutFreeData(p);

	mFree(p->buf_hflags);
	mFree(p);
}

/** レイアウトデータ確保 */

LayoutDat *LayoutAlloc(void)
{
	LayoutDat *p;

	p = (LayoutDat *)mMalloc0(sizeof(LayoutDat));
	if(!p) return NULL;

	p->buf_hflags = (uint8_t *)mMalloc(HEIGHTBUF_SIZE);

	return p;
}

/** 最初のレイアウト処理
 *
 * ページの情報を作成 + 見出しの抽出 */

void LayoutRunFirst(LayoutDat *info,mPopupProgress *prog)
{
	LayoutWork *p;
	LayoutFirst lf;
	PageInfoItem *pi;
	uint8_t *topbuf;
	uint32_t textsize;

	//現在のデータを解放

	LayoutFreeData(info);

	//LayoutWork 作成

	p = _create_layoutwork(info, NULL);
	if(!p) return;

	p->pfirst = &lf;

	mMemset0(p->buf_hflags, HEIGHTBUF_SIZE);

	//見出し文字列用バッファ

	mBufAlloc(&p->buf_title, 1024, 1024);

	//LayoutFirst 初期化

	mMemset0(&lf, sizeof(LayoutFirst));

	lf.curpage.src = p->text;

	//各ページ情報セット

	topbuf = p->text;
	textsize = GDAT->textbuf.size;

	while(layout_page(p, &lf))
	{
		//ページ追加
		
		pi = (PageInfoItem *)mListAppendNew(&info->list_page, sizeof(PageInfoItem));

		if(pi)
		{
			lf.curpage.pageno = lf.pagenum;
		
			pi->pageinfo = lf.curpage;
		
			lf.pagenum++;
		}

		lf.curpage = lf.nextpage;

		//進捗

		mPopupProgressThreadSetPos(prog,
			(int)((double)(p->text - topbuf) / textsize * 100 + 0.5));
	}

	//ページ数

	info->page_num = lf.pagenum;

	//見出し個数

	memcpy(info->title_num, p->title_num, sizeof(int) * 3);

	//解放

	_layoutwork_free(p);
}

/** ページを描画
 *
 * page: NULL で先頭ページ */

void LayoutDrawPage(LayoutDat *info,mPixbuf *img,PageInfoItem *page)
{
	LayoutWork *p;
	PageInfoItem *next;

	if(!page) page = (PageInfoItem *)info->list_page.top;
	if(!page) return;

	p = _create_layoutwork(info, img);
	if(!p) return;

	p->pagenum = info->page_num;

	//描画

	if(p->stdef->pages == 1)
		//単一ページ
		layout_drawpage(p, &page->pageinfo, -1);
	else
	{
		//見開き
		
		layout_drawpage(p, &page->pageinfo, 0);

		next = (PageInfoItem *)page->i.next;
		if(next) layout_drawpage(p, &next->pageinfo, 1);
	}

	_layoutwork_free(p);
}

