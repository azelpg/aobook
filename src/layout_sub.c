/*$
aobook
Copyright (c) 2014-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/**********************************
 * レイアウト:サブ関数
 **********************************/

#include <string.h>

#include "mlk.h"
#include "mlk_list.h"
#include "mlk_buf.h"
#include "mlk_unicode.h"

#include "def_style.h"
#include "pv_layout.h"
#include "layout.h"



/** UTF-32 文字列内に c が存在するか
 *
 * 値は小さい順に並んでいる。 */

mlkbool layout_find_utf32(uint32_t c,UnicodeChars *uc)
{
	uint32_t *buf,mc;
	int low,high,mid;

	if(!uc->buf) return FALSE;

	buf = uc->buf;
	low = 0;
	high = uc->len - 1;

	while(low <= high)
	{
		mid = (low + high) / 2;
		mc = buf[mid];

		if(mc == c)
			return TRUE;
		else if(mc < c)
			low = mid + 1;
		else
			high = mid - 1;
	}

	return FALSE;
}

/** 縦中横の対象文字か */

mlkbool layout_ischar_tatetyuyoko(uint32_t c)
{
	return ((c >= '0' && c <= '9') || c == '!' || c == '?');
}

/** pi が行末に来てはならない文字か */

mlkbool layout_is_nobottom(LayoutWork *p,CharItem *pi)
{
	uint32_t c = pi->code;

	//行末禁則

	if(layout_find_utf32(c, &p->u32_nobottom))
		return TRUE;

	//分割禁止

	if(layout_find_utf32(c, &p->u32_nosep)
		&& pi->i.next
		&& ((CharItem *)pi->i.next)->code == c)
		return TRUE;

	//くの字点上

	if(c == 0x3033 || c == 0x3034) return TRUE;

	return FALSE;
}

/** 文字を置き換え
 *
 * pdst: 対象となる文字のポインタ
 * return: 置き換えが行われたか */

mlkbool layout_replace_char(LayoutWork *p,uint32_t *pdst)
{
	uint32_t *buf,*pmid,c;
	int low,high,mid;

	if(!p->u32_replace.buf) return FALSE;

	c = *pdst;

	buf = p->u32_replace.buf;
	low = 0;
	high = p->u32_replace.len - 1;

	while(low <= high)
	{
		mid = (low + high) / 2;
		pmid = buf + mid * 2;

		if(*pmid == c)
		{
			*pdst = pmid[1];
			return TRUE;
		}
		else if(*pmid < c)
			low = mid + 1;
		else
			high = mid - 1;
	}

	return FALSE;
}

/** ルビ位置設定時の情報取得
 *
 * return: 親文字列が折り返しているか */

int layout_getrubyinfo(LayoutWork *p,RubyItem *pi)
{
	CharItem *pic;
	int i,wrap;

	wrap = 0;

	for(pic = pi->char_top, i = pi->charlen; i > 0; i--, pic = (CharItem *)pic->i.next)
	{
		if(pic->flags & CHARITEM_F_WRAP_TOP)
		{
			if(pic == pi->char_top)
				//親文字列の先頭が、折り返しの先頭
				wrap |= 2;
			else
				wrap |= 1;
		}
	}

	return wrap;
}

/** pi と next のルビの親文字列が連続しているか */

mlkbool layout_is_ruby_connect(RubyItem *pi,RubyItem *next)
{
	CharItem *pic;
	int i;

	if(!pi || !next) return FALSE;

	//pi の親文字列の次の位置

	for(pic = pi->char_top, i = pi->charlen; i > 0; i--, pic = (CharItem *)pic->i.next);

	//next の先頭と等しいか

	return (pic == next->char_top);
}

/** 見出しの終わりコマンド時、現在の文字列を見出しリストに追加 */

void layout_append_titlelist(LayoutWork *p)
{
	TitleItem *pi;
	uint8_t *ps;
	char *buf;
	int len,type;

	if(p->buf_title.cursize == 0) return;

	ps = p->buf_title.buf;

	//1byte 目は見出しタイプ

	type = *(ps++);

	//UTF32 -> UTF8

	buf = mUTF32toUTF8_alloc((uint32_t *)ps, (p->buf_title.cursize - 1) / 4, &len);
	if(!buf) return;

	//アイテム追加

	pi = (TitleItem *)mListAppendNew(p->plist_title, sizeof(TitleItem) + len);
	if(pi)
	{
		pi->type = type;
		pi->pageno = p->pfirst->pagenum;

		memcpy(pi->text, buf, len);

		//各タイプの個数を加算
		
		(p->title_num[type])++;
	}

	mFree(buf);

	//文字列バッファリセット

	mBufReset(&p->buf_title);
}
