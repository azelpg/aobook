/*$
aobook
Copyright (c) 2014-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/***********************************
 * テキストを内部データに変換
 ***********************************/

#include "mlk.h"
#include "mlk_buf.h"
#include "mlk_list.h"

#include "textconv.h"
#include "def_textdata.h"


//--------------------

//改*

static const CmdMatchDef g_match_kai[] = {
	{u"丁", COMMAND_KAITYO, CMDMATCHDEF_F_NO_ENTER, 0},
	{u"ページ", COMMAND_KAIPAGE, CMDMATCHDEF_F_NO_ENTER, 0},
	{u"頁", COMMAND_KAIPAGE, CMDMATCHDEF_F_NO_ENTER, 0},
	{u"見開き", COMMAND_KAIMIHIRAKI, CMDMATCHDEF_F_NO_ENTER, 0},
	{0,0,0,0}
};

//「*」*

static const CmdMatchDef g_match_kagi[] = {
	{u"は縦中横", COMMAND_TATETYUYOKO_START, 0, 0},
	{u"は横組み", COMMAND_YOKOGUMI_START, 0, 0},
	{u"は太字", COMMAND_BOLD_START, 0, 0},

	{u"は大見出し", COMMAND_TITLE_START, CMDMATCHDEF_F_HAVE_VAL1, TITLE_TYPE_DAI},
	{u"は中見出し", COMMAND_TITLE_START, CMDMATCHDEF_F_HAVE_VAL1, TITLE_TYPE_TYUU},
	{u"は小見出し", COMMAND_TITLE_START, CMDMATCHDEF_F_HAVE_VAL1, TITLE_TYPE_SYOU},

	{u"に傍点", COMMAND_BOUTEN_START, CMDMATCHDEF_F_HAVE_VAL1, BOUTEN_TYPE_GOMA},
	{u"に白ゴマ傍点", COMMAND_BOUTEN_START, CMDMATCHDEF_F_HAVE_VAL1, BOUTEN_TYPE_SIROGOMA},
	{u"に丸傍点", COMMAND_BOUTEN_START, CMDMATCHDEF_F_HAVE_VAL1, BOUTEN_TYPE_MARU},
	{u"に白丸傍点", COMMAND_BOUTEN_START, CMDMATCHDEF_F_HAVE_VAL1, BOUTEN_TYPE_SIROMARU},
	{u"に黒三角傍点", COMMAND_BOUTEN_START, CMDMATCHDEF_F_HAVE_VAL1, BOUTEN_TYPE_KUROSANKAKU},
	{u"に白三角傍点", COMMAND_BOUTEN_START, CMDMATCHDEF_F_HAVE_VAL1, BOUTEN_TYPE_SIROSANKAKU},
	{u"に二重丸傍点", COMMAND_BOUTEN_START, CMDMATCHDEF_F_HAVE_VAL1, BOUTEN_TYPE_NIJUUMARU},
	{u"に蛇の目傍点", COMMAND_BOUTEN_START, CMDMATCHDEF_F_HAVE_VAL1, BOUTEN_TYPE_JYANOME},
	{u"にばつ傍点", COMMAND_BOUTEN_START, CMDMATCHDEF_F_HAVE_VAL1, BOUTEN_TYPE_BATU},

	{u"に傍線", COMMAND_BOUSEN_START, CMDMATCHDEF_F_HAVE_VAL1, BOUSEN_TYPE_NORMAL},
	{u"に二重傍線", COMMAND_BOUSEN_START, CMDMATCHDEF_F_HAVE_VAL1, BOUSEN_TYPE_DOUBLE},
	{u"に鎖線", COMMAND_BOUSEN_START, CMDMATCHDEF_F_HAVE_VAL1, BOUSEN_TYPE_KUSARI},
	{u"に破線", COMMAND_BOUSEN_START, CMDMATCHDEF_F_HAVE_VAL1, BOUSEN_TYPE_HASEN},
	{u"に波線", COMMAND_BOUSEN_START, CMDMATCHDEF_F_HAVE_VAL1, BOUSEN_TYPE_NAMISEN},
	{0,0,0,0}
};

//ここから*

static const CmdMatchDef g_match_kokokara[] = {
	{u"横組み", COMMAND_YOKOGUMI_START, CMDMATCHDEF_F_NO_ENTER, 0},
	{u"*字下げ", COMMAND_JISAGE_START, CMDMATCHDEF_F_NO_ENTER, 0},
	{u"*字下げ、折り返して*字下げ", COMMAND_JISAGE_WRAP_START, CMDMATCHDEF_F_NO_ENTER, 0},
	{u"改行天付き、折り返して*字下げ", COMMAND_JISAGE_TEN_WRAP_START, CMDMATCHDEF_F_NO_ENTER, 0},
	{u"地付き", COMMAND_JIAGE_START, CMDMATCHDEF_F_NO_ENTER, 0},
	{u"地から*字上げ", COMMAND_JIAGE_START, CMDMATCHDEF_F_NO_ENTER, 0},
	{u"太字", COMMAND_BOLD_START, CMDMATCHDEF_F_NO_ENTER, 0},
	{u"大見出し", COMMAND_TITLE_START, CMDMATCHDEF_F_NO_ENTER | CMDMATCHDEF_F_HAVE_VAL1, TITLE_TYPE_DAI},
	{u"中見出し", COMMAND_TITLE_START, CMDMATCHDEF_F_NO_ENTER | CMDMATCHDEF_F_HAVE_VAL1, TITLE_TYPE_TYUU},
	{u"小見出し", COMMAND_TITLE_START, CMDMATCHDEF_F_NO_ENTER | CMDMATCHDEF_F_HAVE_VAL1, TITLE_TYPE_SYOU},
	{0,0,0,0}
};

//ここで*

static const CmdMatchDef g_match_kokode[] = {
	{u"横組み終わり", COMMAND_YOKOGUMI_END, CMDMATCHDEF_F_NO_ENTER, 0},
	{u"字下げ終わり", COMMAND_JISAGE_END, CMDMATCHDEF_F_NO_ENTER, 0},
	{u"地付き終わり", COMMAND_JIAGE_END, CMDMATCHDEF_F_NO_ENTER, 0},
	{u"字上げ終わり", COMMAND_JIAGE_END, CMDMATCHDEF_F_NO_ENTER, 0},
	{u"太字終わり", COMMAND_BOLD_END, CMDMATCHDEF_F_NO_ENTER, 0},
	{u"大見出し終わり", COMMAND_TITLE_END, CMDMATCHDEF_F_NO_ENTER, 0},
	{u"中見出し終わり", COMMAND_TITLE_END, CMDMATCHDEF_F_NO_ENTER, 0},
	{u"小見出し終わり", COMMAND_TITLE_END, CMDMATCHDEF_F_NO_ENTER, 0},
	{0,0,0,0}
};

//ほか

static const CmdMatchDef g_match_etc[] = {
	{u"*字下げ", COMMAND_JISAGE_LINE, 0, 0},
	{u"縦中横", COMMAND_TATETYUYOKO_START, 0, 0},
	{u"縦中横終わり", COMMAND_TATETYUYOKO_END, 0, 0},
	{u"横組み", COMMAND_YOKOGUMI_START, 0, 0},
	{u"横組み終わり", COMMAND_YOKOGUMI_END, 0, 0},
	{u"太字", COMMAND_BOLD_START, 0, 0},
	{u"太字終わり", COMMAND_BOLD_END, 0, 0},
	{u"地付き", COMMAND_JIAGE_LINE, 0, 0},
	{u"地から*字上げ", COMMAND_JIAGE_LINE, 0, 0},

	{u"大見出し", COMMAND_TITLE_START, CMDMATCHDEF_F_HAVE_VAL1, TITLE_TYPE_DAI},
	{u"中見出し", COMMAND_TITLE_START, CMDMATCHDEF_F_HAVE_VAL1, TITLE_TYPE_TYUU},
	{u"小見出し", COMMAND_TITLE_START, CMDMATCHDEF_F_HAVE_VAL1, TITLE_TYPE_SYOU},
	{u"大見出し終わり", COMMAND_TITLE_END, 0, 0},
	{u"中見出し終わり", COMMAND_TITLE_END, 0, 0},
	{u"小見出し終わり", COMMAND_TITLE_END, 0, 0},

	{u"傍点", COMMAND_BOUTEN_START, CMDMATCHDEF_F_HAVE_VAL1, BOUTEN_TYPE_GOMA},
	{u"白ゴマ傍点", COMMAND_BOUTEN_START, CMDMATCHDEF_F_HAVE_VAL1, BOUTEN_TYPE_SIROGOMA},
	{u"丸傍点", COMMAND_BOUTEN_START, CMDMATCHDEF_F_HAVE_VAL1, BOUTEN_TYPE_MARU},
	{u"白丸傍点", COMMAND_BOUTEN_START, CMDMATCHDEF_F_HAVE_VAL1, BOUTEN_TYPE_SIROMARU},
	{u"黒三角傍点", COMMAND_BOUTEN_START, CMDMATCHDEF_F_HAVE_VAL1, BOUTEN_TYPE_KUROSANKAKU},
	{u"白三角傍点", COMMAND_BOUTEN_START, CMDMATCHDEF_F_HAVE_VAL1, BOUTEN_TYPE_SIROSANKAKU},
	{u"二重丸傍点", COMMAND_BOUTEN_START, CMDMATCHDEF_F_HAVE_VAL1, BOUTEN_TYPE_NIJUUMARU},
	{u"蛇の目傍点", COMMAND_BOUTEN_START, CMDMATCHDEF_F_HAVE_VAL1, BOUTEN_TYPE_JYANOME},
	{u"ばつ傍点", COMMAND_BOUTEN_START, CMDMATCHDEF_F_HAVE_VAL1, BOUTEN_TYPE_BATU},

	{u"傍点終わり", COMMAND_BOUTEN_END, 0, 0},
	{u"白ゴマ傍点終わり", COMMAND_BOUTEN_END, 0, 0},
	{u"丸傍点終わり", COMMAND_BOUTEN_END, 0, 0},
	{u"白丸傍点終わり", COMMAND_BOUTEN_END, 0, 0},
	{u"黒三角傍点終わり", COMMAND_BOUTEN_END, 0, 0},
	{u"白三角傍点終わり", COMMAND_BOUTEN_END, 0, 0},
	{u"二重丸傍点終わり", COMMAND_BOUTEN_END, 0, 0},
	{u"蛇の目傍点終わり", COMMAND_BOUTEN_END, 0, 0},
	{u"ばつ傍点終わり", COMMAND_BOUTEN_END, 0, 0},

	{u"傍線", COMMAND_BOUSEN_START, CMDMATCHDEF_F_HAVE_VAL1, BOUSEN_TYPE_NORMAL},
	{u"二重傍線", COMMAND_BOUSEN_START, CMDMATCHDEF_F_HAVE_VAL1, BOUSEN_TYPE_DOUBLE},
	{u"鎖線", COMMAND_BOUSEN_START, CMDMATCHDEF_F_HAVE_VAL1, BOUSEN_TYPE_KUSARI},
	{u"破線", COMMAND_BOUSEN_START, CMDMATCHDEF_F_HAVE_VAL1, BOUSEN_TYPE_HASEN},
	{u"波線", COMMAND_BOUSEN_START, CMDMATCHDEF_F_HAVE_VAL1, BOUSEN_TYPE_NAMISEN},

	{u"傍線終わり", COMMAND_BOUSEN_END, 0, 0},
	{u"二重傍線終わり", COMMAND_BOUSEN_END, 0, 0},
	{u"鎖線終わり", COMMAND_BOUSEN_END, 0, 0},
	{u"破線終わり", COMMAND_BOUSEN_END, 0, 0},
	{u"波線終わり", COMMAND_BOUSEN_END, 0, 0},

	{u"ページの左右中央", COMMAND_PAGE_CENTER, CMDMATCHDEF_F_NO_ENTER, 0},
	{0,0,0,0}
};

//--------------------


//================================
// ［＃...］ のサブ
//================================


/* ［＃...］ の文字列判定
 *
 * return: TRUE でコマンドを追加 */

static mlkbool _match_command(ConvertData *p,uint32_t *pt,CmdMatchData *dst)
{
	const CmdMatchDef *def_array = NULL;
	uint32_t c;
	mlkbool ret = FALSE;

	//結果を初期化

	mMemset0(dst, sizeof(CmdMatchData));

	dst->val1 = COMMAND_VALUE_NONE;
	dst->val2 = COMMAND_VALUE_NONE;

	//先頭文字から判断して各処理
	// :def_array != NULL で、配列データから検索する。
	// :def_array == NULL && ret != 0 で、検索済み。

	c = *(pt++);

	switch(c)
	{
		//"改*"
		case U'改':
			def_array = g_match_kai;
			break;

		//「*」*
		case U'「':
			if(convert_matchCmd_kagi(pt, g_match_kagi, dst))
			{
				//「」の中の文字列は、効果をかける対象の文字列。
				// 中間データの終端が同じ文字列かどうか比較し、
				// マッチすれば、dst->insert に対象の先頭位置をセット。 
				
				if(convert_compareCmdTextLast(p, dst))
					ret = TRUE;
			}
			break;

		//ここから* or ここで*
		case U'こ':
			if(convert_compareText16(pt, u"こから"))
			{
				//ここから*
				
				def_array = g_match_kokokara;
				pt += 3;
			}
			else if(convert_compareText16(pt, u"こで"))
			{
				//ここで*
				
				def_array = g_match_kokode;
				pt += 2;
			}
			break;

		//ほか
		default:
			pt--;
			def_array = g_match_etc;
			break;
	}

	//

	if(!def_array && ret)
		//マッチ済み
		return TRUE;
	else if(def_array && convert_matchCmd_array(pt, def_array, dst))
		//配列からマッチ
		return TRUE;
	else
		//どれともマッチしない場合は、挿絵のマッチ
		return convert_matchCmd_picture(pt, dst);
}


//================================
// 各注記処理
//================================


/* "［＃...］" の処理
 *
 * pptext: '＃' の位置 */

static mlkbool _proc_command(ConvertData *p,uint32_t **pptext,mlkbool *no_enter)
{
	uint32_t *pt;
	CmdMatchData dat;

	pt = *pptext + 1;

	//'］' 検索、0 に置き換え、次の位置へ

	if(!convert_text_kagiClose(pptext)) return FALSE;

	//"［］" 中の文字列判定
	// :マッチするものがなかった場合は全体を無視

	if(!_match_command(p, pt, &dat)) return TRUE;

	//地付き or 地からn字上げの場合、上げ数は +1 する

	if(dat.type == COMMAND_JIAGE_LINE || dat.type == COMMAND_JIAGE_START)
	{
		if(dat.val1 == COMMAND_VALUE_NONE)
			dat.val1 = 1;
		else
			dat.val1++;
	}

	//コマンド追加

	convert_addItem_command(p, &dat);

	//文章としての改行を追加しない

	*no_enter = ((dat.flags & CMDMATCHDEF_F_NO_ENTER) != 0);

	return TRUE;
}

/* ルビ文字列の処理 ('《' が来た時)
 *
 * pptext: '《' の次の位置。
 * pitop: '｜'による先頭指定があった位置。1 で指定なし。 */

static mlkbool _proc_ruby(ConvertData *p,uint32_t **pptext,ItemChar *pitop)
{
	uint32_t *pst,*pend;

	//pst = '《' の次

	pst = *pptext;

	//pend = '》'

	for(pend = pst; *pend && *pend != U'》'; pend++);

	if(!(*pend)) return FALSE;

	//《》内が空 -> 通常文字として扱う

	if(pst == pend) return FALSE;

	//次の位置

	*pptext = pend + 1;

	//すべての親文字列に、ルビ情報をセット

	if(pitop == (ItemChar *)1)
		//親の位置指定なし -> 範囲は自動で判定
		convert_setRubyInfo_auto(p, pst, pend - pst);
	else
		//'｜' から終端まで
		convert_setRubyInfo(p, pitop, pst, pend - pst);

	return TRUE;
}

/* "※［＃...］" の外字注記を処理
 *
 * <例>
 * ※［＃「てへん＋劣」、第3水準1-84-77］
 * ※［＃「口＋世」、U+546D、ページ数-行数］
 * ※［＃「土へん＋竒」、ページ数-行数］
 * ※［＃二の字点、1-2-22］
 *
 * pptext: '［' の位置。次の先頭位置が入る。
 * return: エラーの場合 FALSE */

static mlkbool _proc_gaiji(ConvertData *p,uint32_t **pptext)
{
	uint32_t *ps,c;
	int lv;

	ps = *pptext + 2;

	//'］' を検索し、0 に置き換え。pptext には次の位置

	if(!convert_text_kagiClose(pptext)) return FALSE;

	//最初の '、' までスキップ (「」内にあるものは除く)

	for(lv = 0; *ps; )
	{
		c = *(ps++);

		if(c == U'、')
		{
			if(lv == 0) break; 
		}
		else if(c == U'「')
			lv++;
		else if(c == U'」')
		{
			if(lv) lv--;
		}
	}

	//'、' がない場合は、注記全体を無効化する

	if(!(*ps)) return TRUE;

	//----- 文字を番号から取得

	c = 0;

	if(*ps == 'U' && ps[1] == '+')
	{
		//'U+XXXX' : Unicode 番号

		c = convert_text_to_hex(ps + 2);
	}
	else if(*ps >= '0' && *ps <= '9')
	{
		//数値の場合、'面-区-点' or 'ページ数-行数'

		c = convert_text_to_jis(ps);
	}
	else if(*ps == U'第')
	{
		//第3/第4水準 [例:'第3水準1-84-77']

		for(; *ps && *ps != U'準'; ps++);

		if(*ps)
			c = convert_text_to_jis(ps + 1);
	}

	//通常1文字として追加

	if(c && c <= 0x10ffff)
		convert_addItem_normal(p, c);

	return TRUE;
}


//================================
// main
//================================


/* 1行分を処理 (UTF-32 -> 中間データ)
 *
 * dst_no_enter: 最後が注記で、文章としての改行を無効にする場合は TRUE が入る */

static void _proc_line(ConvertData *p,mlkbool *dst_no_enter)
{
	uint32_t *ps,c;
	mListItem *rubytop = (mListItem *)1;
	mlkbool ret,no_enter = FALSE;

	//rubytop : ルビの親文字の先頭アイテム (実際はその次が親文字)
	//  1 で指定がない。
	//  行頭で'｜'が指定された場合、リストにアイテムがないため、rubytop = 0 となる。

	ps = (uint32_t *)p->linebuf.buf;

	while(1)
	{
		c = *(ps++);
		if(c == 0) break;

		ret = FALSE;
		no_enter = FALSE;

		//先頭文字から判定して、各処理
		// ret : TRUE で独自の処理を行った。FALSE で通常文字として扱う。

		switch(c)
		{
			//注記
			case U'［':
				ret = (*ps == U'＃' && _proc_command(p, &ps, &no_enter));
				break;
			//ルビ親文字列の先頭
			case U'｜':
				rubytop = p->list.bottom;
				ret = TRUE;
				break;
			//ルビ文字列
			case U'《':
				ret = _proc_ruby(p, &ps, (ItemChar *)rubytop);
				rubytop = (mListItem *)1; //親位置クリア
				break;
			//外字注記
			case U'※':
				ret = (*ps == U'［' && ps[1] == U'＃' && _proc_gaiji(p, &ps));
				break;
		}

		//通常文字として追加

		if(!ret)
			convert_addItem_normal(p, c);
	}

	*dst_no_enter = no_enter;
}

/* ConvertData 解放 */

static void _free_convert(ConvertData *p,mlkbool failed)
{
	//ソースのバッファを解放
	mFree(p->txtbuf);

	mBufFree(&p->linebuf);

	if(failed) mBufFree(&p->dstbuf);
}

/** UTF-16BE から内部用データに変換
 *
 * [!] src のバッファは常に解放される */

mlkbool TextConvert_to_internal(mBufSize *src,mBufSize *dst)
{
	ConvertData cv;
	uint32_t size;
	mlkbool no_enter;

	//変換用データ

	mMemset0(&cv, sizeof(ConvertData));

	cv.txtbuf = src->buf;
	cv.pcur   = (uint8_t *)src->buf;
	cv.remain = src->size;

	//各メモリ確保

	size = src->size * 2;
	size = (size + 4095) & (~4095);

	if(!mBufAlloc(&cv.dstbuf, size, 16 * 1024)
		|| !mBufAlloc(&cv.linebuf, 4 * 1024, 4096))
	{
		_free_convert(&cv, TRUE);
		return FALSE;
	}

	//1行ごとに処理

	while(convert_getLine(&cv))
	{
		_proc_line(&cv, &no_enter);

		convert_output_line(&cv);

		//文章としての改行追加

		if(!no_enter)
			mBufAppendByte(&cv.dstbuf, DATATYPE_ENTER);

		//行番号

		(cv.curline)++;
	}

	mBufAppendByte(&cv.dstbuf, DATATYPE_END);

	//解放

	_free_convert(&cv, FALSE);

	//

	mBufCutCurrent(&cv.dstbuf);

	dst->buf  = cv.dstbuf.buf;
	dst->size = cv.dstbuf.cursize;

	return TRUE;
}
