/*$
aobook
Copyright (c) 2014-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/********************************
 * レイアウト内部用
 ********************************/

typedef struct _StyleDef StyleDef;
typedef struct _StyleWork StyleWork;


/** 本文1文字のアイテム */

typedef struct
{
	mListItem i;
	
	uint32_t code;		//Unicode
	int16_t x,y,		//描画位置(余白含む)
		width,			//縦中横の文字幅 (px)
		height,			//文字高さ (px)。余白含む
		padding;		//テキスト描画時のy余白
	uint8_t chartype,	//文字の配置タイプ
		flags,
		bouten,			//傍点タイプ
		bousen; 		//傍線タイプ
	char horzcnt,		//縦中横の文字数
		horzchar[3];	//縦中横のASCII文字 (NULL なし)
}CharItem;

enum
{
	CHARITEM_TYPE_NORMAL,	//通常縦書き
	CHARITEM_TYPE_ROTATE,	//欧文横組み
	CHARITEM_TYPE_HORZ		//縦中横
};

enum
{
	CHARITEM_F_WRAP_TOP   = 1<<0,	//折り返しの先頭文字である
	CHARITEM_F_DAKUTEN    = 1<<1,	//濁点結合 (次の文字が濁点の場合、濁点文字を削除して、フラグをON)
	CHARITEM_F_HANDAKUTEN = 1<<2,	//半濁点結合
	CHARITEM_F_JIAGE      = 1<<3,	//地付き/地上げ
	CHARITEM_F_BOLD       = 1<<4	//太字
};

/** ルビアイテム (親文字列とは別のリストに) */

typedef struct
{
	mListItem i;

	CharItem *char_top;	//親文字列の先頭位置
	uint32_t *rubytxt;	//ルビ文字列先頭位置 (内部データの位置)
	uint16_t rubylen,	//ルビ文字数
		charlen;		//親文字列数
	int16_t x,y;		//描画位置 (基本的に親文字列と同じ位置)
	int32_t ruby_h,		//ルビ文字列全体の高さ (px)
		char_h;			//親文字列の高さ
}RubyItem;

enum
{
	RUBYITEM_CHARH_NO_PADDING = -1	//ルビの間に余白なし
};

/** 行の状態 (現在行のみに影響する) */

typedef struct
{
	CharItem *tatetyuyoko_top;	//縦中横の先頭文字
	uint8_t tatetyuyoko_num;	//縦中横の現在の文字数 (0:なし、1で開始、2〜で文字数+1)
	uint8_t jisage,	//字下げ数
		jiage,		//地付き/字上げ数 (+1)
		bouten,		//傍点 (0 でなし)
		bousen;		//傍線
}LineState;

/** ブロック型注記の状態 (値はそれぞれ 0 でなし) */

typedef struct
{
	uint8_t flags,
		jisage,			//字下げ
		jisage_wrap,	//折り返し以降の字下げ
		jiage,			//地付き/字上げ (1=地付き, 2〜=地からn字上げ)
		title;			//見出し (0:なし 1:大 2:中 3:小)
}BlockState;

enum
{
	BLOCKSTATE_F_YOKOGUMI = 1<<0,	//横組み
	BLOCKSTATE_F_BOLD = 1<<1		//太字
};

/** ページの状態 */

typedef struct
{
	mlkbool fcenter;	//ページの左右中央
	uint8_t *picture;	//挿絵コマンドのデータ位置 (コマンドタイプの次の位置)
}PageState;

/** ページ情報 */

typedef struct
{
	uint8_t *src;		//内部データの先頭位置
	BlockState blockstate;	//現在のブロック型注記の状態
	int pageno;			//ページ位置
	uint32_t lineno;	//先頭のソーステキストの行番号
	int16_t wrap_num,	//前ページの前行の折り返し行数 (次ページでずらす分)
		diffx;			//ページの左右中央時の先頭 X 位置
	uint8_t flags;
}PageInfo;

#define PAGEINFO_F_BLANK 1	//空白ページ

/** ページ情報のリストアイテム */

typedef struct _PageInfoItem
{
	mListItem i;
	PageInfo pageinfo;
}PageInfoItem;

/** 最初のレイアウト時用の作業データ */

typedef struct
{
	PageInfo curpage,	//現在のページ情報
		nextpage;		//次のページの情報
	int pagenum;		//全ページ数
}LayoutFirst;

/** 文字列挙データ */

typedef struct
{
	uint32_t *buf;
	int len;
}UnicodeChars;

/** レイアウト作業用データ */

typedef struct
{
	uint8_t *text,  	//内部データの現在位置
		*buf_hflags;	//本文高さ用フラグ
	StyleWork *style;	//スタイルデータ
	StyleDef *stdef;
	mPixbuf *img;		//描画時の描画先
	LayoutFirst *pfirst;	//最初のレイアウト用データ

	UnicodeChars u32_nohead,	//文字列挙 (Unicode 小さい順に並んでいる)
		u32_nobottom,
		u32_hanging,
		u32_nosep,
		u32_replace;

	mFont *font_main,
		*font_ruby,
		*font_bold,
		*font_kenten;

	uint32_t pixcol_text;	//本文の色

	int fontmain_h,		//フォントの高さ
		fontbold_h,
		fontruby_h,
		fontkenten_h,
		pagenum,		//全ページ数 (描画時)
		pageW,pageH,	//1ページ分のテキスト描画部分のサイズ (余白部分は除く)
		text_right_x,	//1ページの先頭行の X 位置
		line_width;		//行幅 (次の行までの px 数)

	int jisage_y,		//先頭行の字下げY位置 (px)
		jisage_wrap_y,	//折り返し時の字下げ Y位置
		jiage_bottom,	//地からn字上げの下端 Y位置
		curlineno;		//現在のテキスト行位置 (内部データで行情報が見つかるたびに更新)

	int title_num[3];	//見出しの各個数
	
	mList list_char,	//本文文字データ (1行分の作業用)
		list_ruby,		//ルビデータ
		*plist_title;	//見出しアイテムの出力リスト (最初のレイアウト時)
	mBuf buf_title;		//見出しの文字列用 (最初のレイアウト時)

	PageState pagestate;
	BlockState blockstate;
	LineState linestate;
}LayoutWork;

//------------------

/* layout_main.c */

mlkbool layout_page(LayoutWork *p,LayoutFirst *lf);
void layout_drawpage(LayoutWork *p,PageInfo *page,int pagepos);

/* layout_line.c */

mlkbool layout_getline(LayoutWork *p,int *cmd);

/* layout_draw.c */

void layout_draw_line(LayoutWork *p,int pagepos);
void layout_draw_pageinfo(LayoutWork *p,int pageno,int pos);
void layout_draw_picture(LayoutWork *p,int pagepos);

/* layout_sub.c */

mlkbool layout_find_utf32(uint32_t c,UnicodeChars *uc);
mlkbool layout_ischar_tatetyuyoko(uint32_t c);
mlkbool layout_is_nobottom(LayoutWork *p,CharItem *pi);
mlkbool layout_replace_char(LayoutWork *p,uint32_t *pdst);

int layout_getrubyinfo(LayoutWork *p,RubyItem *pi);
mlkbool layout_is_ruby_connect(RubyItem *pi,RubyItem *next);

void layout_append_titlelist(LayoutWork *p);
