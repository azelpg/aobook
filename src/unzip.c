/*$
aobook
Copyright (c) 2014-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/********************************
 * ZIP からテキスト読み込み
 ********************************/

#include <stdio.h>

#include "mlk.h"
#include "mlk_stdio.h"
#include "mlk_zlib.h"
#include "mlk_str.h"
#include "mlk_util.h"


//--------------------

typedef struct
{
	uint16_t method;	//圧縮タイプ
	uint32_t compsize,uncompsize;
}_fileinfo;

//--------------------


/* 次のローカルファイルヘッダ取得
 *
 * return: [0] 終了 [1] 有効なファイル [-1] 無効なファイル */

static int _next_localfile(FILE *fp,mStr *strname,_fileinfo *info)
{
	uint8_t buf[26];
	uint32_t sig;
	uint16_t flags,name_len,ex_len;
	
	//識別子

	if(mFILEreadLE32(fp, &sig) || sig != 0x04034B50)
		return 0;

	//ヘッダ読み込み

	if(fread(buf, 1, 26, fp) != 26) return 0;

	mGetBuf_format(buf, "2Shh8Siihh",
		&flags, &info->method, &info->compsize, &info->uncompsize,
		&name_len, &ex_len);

	//名前

	if(name_len && mStrResize(strname, name_len))
	{
		fread(strname->buf, 1, name_len, fp);
		mStrSetLen(strname, name_len);
	}
	else
	{
		if(fseek(fp, name_len, SEEK_CUR)) return 0;
		mStrEmpty(strname);
	}

	//拡張フィールドをスキップ

	if(fseek(fp, ex_len, SEEK_CUR))
		return 0;

	//有効なファイルか

	if(!(flags & 1) //暗号化
		&& (info->method == 0 || info->method == 8)	//無圧縮 or Deflate
		&& info->compsize
		&& info->uncompsize)
		return 1;
	else
		return -1;
}

/* deflate 展開 */

static mlkbool _decode_deflate(FILE *fp,_fileinfo *info,mBufSize *dst)
{
	mZlib *zlib;
	mlkerr ret;

	zlib = mZlibDecNew(-1, -15);
	if(!zlib) return FALSE;

	mZlibSetIO_stdio(zlib, fp);

	ret = mZlibDecReadOnce(zlib, dst->buf, dst->size, info->compsize);

	mZlibFree(zlib);

	return (ret == MLKERR_OK);
}

/* 展開 */

static mlkbool _decode(FILE *fp,_fileinfo *info,mBufSize *dst)
{
	uint32_t size = info->uncompsize;
	mlkbool ret;

	//メモリ確保

	dst->buf = mMalloc(size);
	if(!dst->buf) return FALSE;

	dst->size = size;

	//展開

	if(info->method == 0)
		//無圧縮
		ret = (fread(dst->buf, 1, size, fp) == size);
	else
		//deflate
		ret = _decode_deflate(fp, info, dst);

	//
	
	if(ret)
		return TRUE;
	else
	{
		mFree(dst->buf);
		return FALSE;
	}
}

/** ZIP 内のファイルを読み込み
 *
 * filename: NULL で最初の .txt ファイル。それ以外で、読み込むファイル名。 */

mlkbool extractZipFile(const char *zipname,const char *filename,mBufSize *dst)
{
	FILE *fp;
	_fileinfo info;
	mStr strname = MSTR_INIT;
	int ret;
	mlkbool result = FALSE;

	fp = fopen(zipname, "rb");
	if(!fp) return FALSE;

	//ファイルを検索

	while(1)
	{
		ret = _next_localfile(fp, &strname, &info);
		if(ret == 0) break;

		//有効なファイル

		if(ret == 1)
		{
			if(!filename)
			{
				//最初の .txt ファイル

				if(mStrPathCompareExtEq(&strname, "txt"))
					break;
			}
			else
			{
				//指定ファイル

				if(mStrPathCompareEq(&strname, filename))
					break;
			}
		}

		//次へ

		if(fseek(fp, info.compsize, SEEK_CUR))
		{
			ret = 0;
			break;
		}
	}

	mStrFree(&strname);

	//ファイルが見つかれば展開

	if(ret == 1)
		result = _decode(fp, &info, dst);

	fclose(fp);

	return result;
}
