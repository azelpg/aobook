/*$
aobook
Copyright (c) 2014-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/**********************************
 * main
 **********************************/

#include <stdio.h>
#include <string.h>

#include "mlk_gui.h"
#include "mlk_widget_def.h"
#include "mlk_window.h"
#include "mlk_str.h"
#include "mlk_iniread.h"
#include "mlk_iniwrite.h"

#include "def_global.h"
#include "def_style.h"
#include "func_widget.h"
#include "trid.h"
#include "font.h"
#include "style.h"
#include "bookmark.h"

#include "deftrans.h"


//----------------------

//初期化用設定データ

typedef struct
{
	mStr strStyle;
	mToplevelSaveState state_main;
}_initconfig;

//----------------------

GlobalData *g_globaldat;

#define CONFIG_FILENAME  "main.conf"

#define _HELP_TEXT "[usage] exe <file>\n\n" \
 "--help-mlk : show mlk options"

//----------------------


//===========================
// 設定ファイル書き込み
//===========================


/* ウィンドウ状態書き込み */

static void _save_config_winstate(FILE *fp,const char *key,mToplevel *win,mToplevelSaveState *state)
{
	mToplevelSaveState st;
	int32_t n[7];

	if(win)
		mToplevelGetSaveState(win, &st);
	else
		st = *state;

	n[0] = st.x;
	n[1] = st.y;
	n[2] = st.w;
	n[3] = st.h;
	n[4] = st.norm_x;
	n[5] = st.norm_y;
	n[6] = st.flags;

	mIniWrite_putNumbers(fp, key, n, 7, 4, FALSE);
}

/* 設定ファイル書き込み */

static void _save_config(void)
{
	FILE *fp;
	GlobalData *pg = GDAT;
	ShortcutKey *psc;
	int i;

	fp = mIniWrite_openFile_join(mGuiGetPath_config_text(), CONFIG_FILENAME);
	if(!fp) return;

	//-------- メイン

	mIniWrite_putGroup(fp, "main");

	mIniWrite_putInt(fp, "ver", 2);

	//メインウィンドウ

	_save_config_winstate(fp, "mainwin", MLK_TOPLEVEL(pg->mainwin), NULL); 

	//しおりウィンドウ

	_save_config_winstate(fp, "bkmwin", MLK_TOPLEVEL(pg->bkmwin), &pg->bkmwin_state);

	mIniWrite_putInt(fp, "bkmwin_tab", pg->bkmwin_tabno);

	//

	mIniWrite_putStr(fp, "opendir", &pg->strOpenDir);
	mIniWrite_putStr(fp, "bkmlocaldir", &pg->strBkmLocalDir);
	mIniWrite_putStr(fp, "style", &pg->style->b.str_stylename);

	mIniWrite_putInt(fp, "viewflags", pg->viewflags);

	//------ ファイル履歴

	mIniWrite_putGroup(fp, "recentfile");
	mIniWrite_putStrArray(fp, 0, pg->strRecentFile, RECENTFILE_NUM);

	//------ ボタン動作

	mIniWrite_putGroup(fp, "button");

	for(i = 0; i < BUTTONACT_BTT_NUM; i++)
		mIniWrite_putInt_keyno(fp, i, pg->bttact[i]);
	
	//------- ショートカットキー

	mIniWrite_putGroup(fp, "shortcutkey");

	for(psc = pg->sckey; psc->id; psc++)
	{
		if(psc->key)
			mIniWrite_putHex_keyno(fp, psc->id, psc->key);
	}

	//------ ツール

	mIniWrite_putGroup(fp, "tool");
	mIniWrite_putStrArray(fp, 0, pg->strTool, TOOLITEM_NUM);

	//------ mlk

	mGuiWriteIni_system(fp);

	fclose(fp);
}


//===========================
// 設定ファイル読み込み
//===========================


/* ショートカットキー読み込み */

static void _read_config_shortcutkey(mIniRead *ini)
{
	ShortcutKey *pd;
	int id;
	uint32_t key;

	if(!mIniRead_setGroup(ini, "shortcutkey"))
	{
		//グループが存在しない場合は、デフォルト値をセット

		GlobalSetSckeyDefault();
	}
	else
	{
		//読み込み "id=key(hex)"

		while(mIniRead_getNextItem_keyno_int32(ini, &id, &key, TRUE))
		{
			//ID から検索してセット
			
			for(pd = GDAT->sckey; pd->id; pd++)
			{
				if(id == pd->id)
				{
					pd->key = key;
					break;
				}
			}
		}
	}
}

/* ボタン動作読み込み */

static void _read_config_buttonact(mIniRead *ini)
{
	int key;
	uint32_t num;
	const uint8_t def[] = {
		BUTTONACT_ACT_PAGE_NEXT, BUTTONACT_ACT_PAGE_PREV,
		BUTTONACT_ACT_PAGE_NEXT, BUTTONACT_ACT_PAGE_PREV,
		BUTTONACT_ACT_PAGE_NEXT, BUTTONACT_ACT_PAGE_PREV
	};

	if(!mIniRead_setGroup(ini, "button"))
	{
		//グループがない場合はデフォルト値をコピー

		memcpy(GDAT->bttact, def, BUTTONACT_BTT_NUM);
	}
	else
	{
		//"配列番号=動作番号"
		
		while(mIniRead_getNextItem_keyno_int32(ini, &key, &num, FALSE))
		{
			if(key < BUTTONACT_BTT_NUM)
				GDAT->bttact[key] = num;
		}
	}
}

/* ウィンドウ状態読み込み */

static void _read_config_winstate(mIniRead *ini,const char *key,mToplevelSaveState *state)
{
	int32_t n[7];

	//[!] state はゼロクリアされているので、初期値は設定しなくてよい

	if(mIniRead_getNumbers(ini, key, n, 7, 4, FALSE) == 7)
	{
		state->x = n[0];
		state->y = n[1];
		state->w = n[2];
		state->h = n[3];
		state->norm_x = n[4];
		state->norm_y = n[5];
		state->flags = n[6];
	}
}

/* 設定ファイル読み込み */

static void _read_config(_initconfig *dst)
{
	mIniRead *ini;
	GlobalData *pg = GDAT;

	mIniRead_loadFile_join(&ini, mGuiGetPath_config_text(), CONFIG_FILENAME);
	if(!ini) return;

	//------- main

	mIniRead_setGroup(ini, "main");

	//バージョンが2以外なら、空にする

	if(mIniRead_getInt(ini, "ver", 0) != 2)
		mIniRead_setEmpty(ini);

	//メインウィンドウ

	_read_config_winstate(ini, "mainwin", &dst->state_main);

	//しおりウィンドウ

	_read_config_winstate(ini, "bkmwin", &pg->bkmwin_state);

	pg->bkmwin_tabno = mIniRead_getInt(ini, "bkmwin_tab", 0);

	//

	mIniRead_getTextStr(ini, "opendir", &pg->strOpenDir, NULL);
	mIniRead_getTextStr(ini, "bkmlocaldir", &pg->strBkmLocalDir, NULL);
	mIniRead_getTextStr(ini, "style", &dst->strStyle, "default");

	pg->viewflags = mIniRead_getInt(ini, "viewflags", VIEWFLAGS_TOOLBAR | VIEWFLAGS_PAGENO);

	//-------- ファイル履歴

	mIniRead_setGroup(ini, "recentfile");
	mIniRead_getTextStrArray(ini, 0, pg->strRecentFile, RECENTFILE_NUM);

	//------ ボタン動作

	_read_config_buttonact(ini);

	//------- ショートカットキー

	_read_config_shortcutkey(ini);

	//----- ツール

	if(mIniRead_setGroup(ini, "tool"))
		mIniRead_getTextStrArray(ini, 0, pg->strTool, TOOLITEM_NUM);
	
	//---- mlk 情報

	mGuiReadIni_system(ini);

	//

	mIniRead_end(ini);
}


//===========================
// 初期化
//===========================


/** 初期化 */

static int _init(int argc,char **argv,int toparg)
{
	_initconfig conf;

	//設定ファイルディレクトリ作成

	mGuiCreateConfigDir(NULL);

	//フォント初期化

	if(!FontInit()) return 1;

	//グローバルデータ作成

	if(!GlobalDataNew()) return 1;

	//設定ファイル読み込み

	mMemset0(&conf, sizeof(_initconfig));

	_read_config(&conf);

	//スタイル読み込み

	StyleWork_readStyle(GDAT->style, conf.strStyle.buf);

	mStrFree(&conf.strStyle);

	//グローバルしおり読み込み

	BkmGlobal_loadFile();

	//メインウィンドウ作成・表示

	MainWindowNew(&conf.state_main);

	//しおりウィンドウ表示

	if(GDAT->viewflags & VIEWFLAGS_BOOKMARK)
		BkmWindow_new();

	//ファイルを開く

	if(toparg < argc)
	{
		//引数のファイル

		mStr str = MSTR_INIT;

		mStrSetText_locale(&str, argv[toparg], -1);
		
		MainWindow_loadText(GDAT->mainwin, str.buf, -1, 0, FALSE);

		mStrFree(&str);
	}

	return 0;
}


//===========================
// メイン
//===========================


/** 終了処理 */

static void _finish(void)
{
	//現在の情報を記録

	GlobalFinish();

	//設定ファイル保存

	_save_config();

	BkmGlobal_saveFile();

	//グローバルデータ解放

	GlobalDataFree();

	//フォント終了

	FontEnd();
}

/** 初期化メイン */

static int _init_main(int argc,char **argv)
{
	int top,i;

	if(mGuiInit(argc, argv, &top)) return 1;

	//"--help"

	for(i = top; i < argc; i++)
	{
		if(strcmp(argv[i], "--help") == 0)
		{
			puts(_HELP_TEXT);
			mGuiEnd();
			return 1;
		}
	}

	//

	mGuiSetWMClass("aobook", "aobook");

	//パスセット

	mGuiSetPath_data_exe("../share/aobook");
	mGuiSetPath_config_home(".config/aobook");

	//翻訳データ

	mGuiLoadTranslation(g_deftransdat, NULL, NULL);

	//バックエンド初期化

	if(mGuiInitBackend()) return 1;

	//初期化

	if(_init(argc, argv, top))
	{
		mError("failed initialize\n");
		return 1;
	}

	return 0;
}

/** メイン */

int main(int argc,char **argv)
{
	//初期化
	
	if(_init_main(argc, argv)) return 1;

	//実行

	mGuiRun();

	//終了

	_finish();

	mGuiEnd();

	return 0;
}
