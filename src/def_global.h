/*$
aobook
Copyright (c) 2014-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/********************************
 * グローバル定義
 ********************************/

#define MAINWINDOW(p)  ((MainWindow *)(p))

typedef struct _MainWindow MainWindow;
typedef struct _Toolbar Toolbar;
typedef struct _BkmWindow BkmWindow;
typedef struct _StyleDef StyleDef;
typedef struct _StyleWork StyleWork;
typedef struct _LayoutDat LayoutDat;
typedef struct _PageInfoItem PageInfoItem;


/*---- macro ----*/

#define GDAT           g_globaldat
#define APPNAME        "aobook"
#define RECENTFILE_NUM 10	//最近使ったファイルの数
#define TOOLITEM_NUM   10	//ツールの最大数
#define STYLE_MAXNUM   50	//スタイルの最大数

#define GLOBAL_IS_EMPTY_TEXT  (!g_globaldat->textbuf.buf) //テキストが空か

/*---- enum ----*/

//ボタン動作:ボタン
enum
{
	BUTTONACT_BTT_LEFT,
	BUTTONACT_BTT_RIGHT,
	BUTTONACT_BTT_SCROLL_UP,
	BUTTONACT_BTT_SCROLL_DOWN,
	BUTTONACT_BTT_SCROLL_LEFT,
	BUTTONACT_BTT_SCROLL_RIGHT,

	BUTTONACT_BTT_NUM
};

//ボタン動作:動作
enum
{
	BUTTONACT_ACT_NONE,
	BUTTONACT_ACT_PAGE_NEXT,
	BUTTONACT_ACT_PAGE_PREV,

	BUTTONACT_ACT_NUM
};

//表示フラグ
enum
{
	VIEWFLAGS_BOOKMARK = 1<<0,
	VIEWFLAGS_TOOLBAR = 1<<1,
	VIEWFLAGS_PAGENO = 1<<2
};

/*---- struct ----*/

typedef struct
{
	uint32_t id,key;
}ShortcutKey;

typedef struct
{
	MainWindow *mainwin;
	BkmWindow *bkmwin;	//しおりウィンドウ (NULL で非表示状態)

	mToplevelSaveState bkmwin_state;
	int bkmwin_tabno;

	mStr strFileName,	//開いているファイル名
		strOpenDir,		//テキストを開く時のディレクトリ
		strRecentFile[RECENTFILE_NUM],	//最近使ったファイル
		strBkmLocalDir,			//ローカルしおりのディレクトリ
		strTool[TOOLITEM_NUM];	//ツール (タブで区切る。[0]ツール名 [1]コマンド [2]ショートカットキー(数値))

	uint32_t viewflags;		//表示フラグ
	uint8_t bttact[BUTTONACT_BTT_NUM],	//ボタン動作
		fchange_bkm_global;		//しおり[グローバル]が変更されたか

	ShortcutKey *sckey;	//ショートカットキー設定データ (id=0 で終端)
	int sckey_num;		//ショートカットキーのバッファ個数 (終端の 0 含む)

	//

	mBufSize textbuf;	//テキストの内部データ (サイズは進捗用に使われる)
	int text_charcode;	//テキストの文字コード

	mList list_bkm_global,	//しおりデータ
		list_bkm_local;

	mFont *font_kenten;		//圏点用フォント

	StyleWork *style;		//現在のスタイルデータ (str_stylename が現在のスタイル名)
	LayoutDat *layout;		//レイアウトデータ
	PageInfoItem *curpage;	//現在のページ

	mlkbool is_in_thread,	//スレッド中かどうか
		is_file_zip;		//開いているファイルが ZIP か
}GlobalData;

extern GlobalData *g_globaldat;

/*---- func ----*/

mlkbool GlobalDataNew(void);
void GlobalDataFree(void);

void GlobalFinish(void);
void GlobalSetSckeyDefault(void);
void GlobalSetTextData(mBufSize *buf);

void GlobalAddRecentFile(const char *filename,int code,int line);
void GlobalGetRecentFileInfo(int no,mStr *strfname,int *code,int *line);

