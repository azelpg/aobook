/*$
aobook
Copyright (c) 2014-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * MainWindow [メインウィンドウ]
 *****************************************/

#include "mlk_gui.h"
#include "mlk_widget_def.h"
#include "mlk_widget.h"
#include "mlk_window.h"
#include "mlk_event.h"
#include "mlk_menubar.h"
#include "mlk_menu.h"
#include "mlk_accelerator.h"
#include "mlk_sysdlg.h"

#include "mlk_str.h"
#include "mlk_iniread.h"

#include "def_global.h"
#include "def_widget.h"
#include "def_style.h"
#include "func_widget.h"
#include "bookmark.h"
#include "text.h"
#include "style.h"
#include "layout.h"
#include "trid.h"

#include "def_appicon.h"
#include "def_menudata.h"

//----------------------

#define _VERSION_TEXT  APPNAME " ver 2.0.3\n\nCopyright (c) 2014-2022 Azel"

//----------------------

static int _event_handle(mWidget *wg,mEvent *ev);

//canvas/toolbar
mWidget *createCanvasWidget(mWidget *parent);
Toolbar *createToolbarWidget(mWidget *parent);

//filedialog.c
mlkbool OpenDialog_textFile(mWindow *parent,const char *initdir,mStr *strdst,int *retcode);

//dlg_caption.c
int CaptionDlg(mWindow *parent);

//----------------------


//========================
// sub
//========================


/* アクセラレータのキーをセット */

static void _set_accelkey(MainWindow *p)
{
	mAccelerator *acc;
	ShortcutKey *psc;
	mStr str = MSTR_INIT;
	int i;
	uint32_t key;

	acc = p->top.accelerator;

	mAcceleratorClear(acc);

	//メインメニュー

	for(psc = GDAT->sckey; psc->id; psc++)
	{
		if(psc->key)
			mAcceleratorAdd(acc, psc->id, psc->key, NULL);
	}

	//ツール

	for(i = 0; i < TOOLITEM_NUM; i++)
	{
		if(mStrIsEmpty(GDAT->strTool + i)) break;

		mStrGetSplitText(&str, GDAT->strTool[i].buf, '\t', 2);
		key = mStrToInt(&str);
		
		if(key)
			mAcceleratorAdd(acc, MAINWIN_CMDID_TOOL + i, key, NULL);
	}

	mStrFree(&str);
}

/* 設定でショートカットキーが変更された時 */

static void _change_shortcutkey(MainWindow *p)
{
	mMenu *menu;
	ShortcutKey *ps;

	menu = mToplevelGetMenu_menubar(MLK_TOPLEVEL(p));

	for(ps = GDAT->sckey; ps->id; ps++)
		mMenuSetItemShortcutKey(menu, ps->id, ps->key);
}

/* ツールのメニュー項目セット */

static void _set_tool_menu(MainWindow *p)
{
	mStr str = MSTR_INIT;
	mMenu *menu;
	int i,key;

	//メニュー

	menu = mToplevelGetMenu_menubar(MLK_TOPLEVEL(p));
	menu = mMenuGetItemSubmenu(menu, TRID_MENU_TOP_TOOL);

	//セット

	mMenuDeleteAll(menu);

	for(i = 0; i < TOOLITEM_NUM; i++)
	{
		if(mStrIsEmpty(GDAT->strTool + i)) break;

		//キー

		mStrGetSplitText(&str, GDAT->strTool[i].buf, '\t', 2);
		key = mStrToInt(&str);

		//名前

		mStrGetSplitText(&str, GDAT->strTool[i].buf, '\t', 0);

		//

		mMenuAppend(menu, MAINWIN_CMDID_TOOL + i, str.buf, key, MMENUITEM_F_COPYTEXT);
	}

	mStrFree(&str);
}

/* スタイルメニューをセット (初期時) */

static void _set_init_style_menu(MainWindow *p,mMenu *menu)
{
	mIniRead *ini;
	mStr *pstrname;
	const char *name;
	int i,num,flags;

	pstrname = &GDAT->style->b.str_stylename;

	//スタイル名を項目に追加

	num = StyleConf_openRead(&ini);

	for(i = 0; i < num; i++)
	{
		if(!mIniRead_setGroupNo(ini, i)) break;

		name = mIniRead_getText(ini, "name", NULL);
		if(!name) break;

		flags = MMENUITEM_F_COPYTEXT | MMENUITEM_F_RADIO_TYPE;

		if(mStrCompareEq(pstrname, name))
			flags |= MMENUITEM_F_CHECKED;

		mMenuAppend(menu, MAINWIN_CMDID_STYLE + i, name, 0, flags);
	}

	mIniRead_end(ini);

	//スタイルが一つもない場合

	if(mMenuGetNum(menu) == 0)
	{
		mMenuAppend(menu, MAINWIN_CMDID_STYLE, pstrname->buf, 0,
			MMENUITEM_F_COPYTEXT | MMENUITEM_F_RADIO_TYPE | MMENUITEM_F_CHECKED);
	}
}


//=========================
// 作成 - sub
//=========================


/* メニュー作成 */

static void _create_menu(MainWindow *p)
{
	mMenuBar *bar;
	mMenu *menu,*menu_top;
	ShortcutKey *psc;
	uint32_t f;

	bar = mMenuBarNew(MLK_WIDGET(p), 0, 0);

	mToplevelAttachMenuBar(MLK_TOPLEVEL(p), bar);

	//データから項目をセット

	MLK_TRGROUP(TRGROUP_ID_MENU);

	mMenuBarCreateMenuTrArray16(bar, g_menudata);

	menu_top = mMenuBarGetMenu(bar);

	//チェック

	p->menu_view = menu = mMenuGetItemSubmenu(menu_top, TRID_MENU_TOP_VIEW);
	f = GDAT->viewflags;

	mMenuSetItemCheck(menu, TRID_MENU_VIEW_BOOKMARK, f & VIEWFLAGS_BOOKMARK);
	mMenuSetItemCheck(menu, TRID_MENU_VIEW_TOOLBAR, f & VIEWFLAGS_TOOLBAR);
	mMenuSetItemCheck(menu, TRID_MENU_VIEW_PAGENO, f & VIEWFLAGS_PAGENO);

	//ショートカットキーセット

	for(psc = GDAT->sckey; psc->id; psc++)
	{
		if(psc->key)
			mMenuSetItemShortcutKey(menu_top, psc->id, psc->key);
	}

	//ファイル履歴サブメニュー

	p->menu_recentfile = mMenuNew();

	mMenuSetItemSubmenu(menu_top, TRID_MENU_FILE_RECENTFILE, p->menu_recentfile);

	MainWindow_setRecentFileMenu(p);

	//スタイルメニュー

	p->menu_style = mMenuNew();

	mMenuSetItemSubmenu(menu_top, TRID_MENU_VIEW_STYLE, p->menu_style);

	_set_init_style_menu(p, p->menu_style);

	//ツールサブニュー

	mMenuSetItemSubmenu(menu_top, TRID_MENU_TOP_TOOL, mMenuNew());

	_set_tool_menu(p);
}

/* アクセラレータ作成 */

static void _create_accel(MainWindow *p)
{
	mAccelerator *accel;

	accel = mAcceleratorNew();

	mToplevelAttachAccelerator(MLK_TOPLEVEL(p), accel);

	mAcceleratorSetDefaultWidget(accel, MLK_WIDGET(p));

	_set_accelkey(p);
}


//=========================
// main
//=========================


/* 破棄ハンドラ */

static void _destroy_handle(mWidget *p)
{
	mToplevelDestroyAccelerator(MLK_TOPLEVEL(p));
}

/** メインウィンドウ作成・表示 */

void MainWindowNew(mToplevelSaveState *state)
{
	MainWindow *p;

	//ウィンドウ
	
	p = (MainWindow *)mToplevelNew(NULL, sizeof(MainWindow),
			MTOPLEVEL_S_NORMAL | MTOPLEVEL_S_NO_INPUT_METHOD);
	if(!p) return;
	
	GDAT->mainwin = p;

	p->wg.destroy = _destroy_handle;
	p->wg.event = _event_handle;

	p->wg.fstate |= MWIDGET_STATE_ENABLE_DROP;
	p->wg.foption |= MWIDGET_OPTION_NO_DRAW_BKGND; //背景は各ウィジェットで描画

	//タイトル

	mToplevelSetTitle(MLK_TOPLEVEL(p), APPNAME);

	//アイコン

	mToplevelSetIconPNG_buf(MLK_TOPLEVEL(p), g_appicon_png, sizeof(g_appicon_png));

	//メニュー

	_create_menu(p);

	//アクセラレータ

	_create_accel(p);

	//キャンバスウィジェット作成

	p->wg_canvas = createCanvasWidget(MLK_WIDGET(p));

	//ツールバーウィジェット作成

	p->toolbar = createToolbarWidget(MLK_WIDGET(p));

	//状態復元

	if(!state->w || !state->h)
		MainWindow_setWindowSize(p);
	else
		mToplevelSetSaveState(MLK_TOPLEVEL(p), state);

	//表示

	mWidgetShow(MLK_WIDGET(p), 1);
}

/** 画面更新 */

void MainWindow_update(MainWindow *p)
{
	mWidgetRedraw(p->wg_canvas);
}

/** ウィンドウタイトルセット */

void MainWindow_setTitle(MainWindow *p)
{
	mStr str = MSTR_INIT;

	if(mStrIsEmpty(&GDAT->strFileName))
		//空
		mStrSetText(&str, APPNAME);
	else
	{
		//"filename [charcode]"
		
		mStrPathGetBasename(&str, GDAT->strFileName.buf);
		mStrAppendText(&str, " [");
		mStrAppendText(&str, TextGetCodeName(GDAT->text_charcode));
		mStrAppendChar(&str, ']');
	}

	mToplevelSetTitle(MLK_TOPLEVEL(p), str.buf);

	mStrFree(&str);
}

/** ファイル履歴のメニューをセット */

void MainWindow_setRecentFileMenu(MainWindow *p)
{
	mStr str = MSTR_INIT,fname = MSTR_INIT;
	int i,code,line;

	//クリア

	mMenuDeleteAll(p->menu_recentfile);

	//セット

	for(i = 0; i < RECENTFILE_NUM; i++)
	{
		if(mStrIsEmpty(GDAT->strRecentFile + i)) break;

		GlobalGetRecentFileInfo(i, &fname, &code, &line);

		mStrSetFormat(&str, "%s [%s] %d",
			fname.buf, TextGetCodeName(code), line + 1);
		
		mMenuAppendText_copy(p->menu_recentfile, MAINWIN_CMDID_RECENTFILE + i, str.buf, str.len);
	}

	mStrFree(&str);
	mStrFree(&fname);
}

/** しおり一覧表示のチェックをセット */

void MainWindow_setMenuCheck_bookmark(MainWindow *p)
{
	mMenuSetItemCheck(p->menu_view, TRID_MENU_VIEW_BOOKMARK,
		GDAT->viewflags & VIEWFLAGS_BOOKMARK);
}


//========================
// コマンド
//========================


/* 開く */

static void _cmd_open(MainWindow *p)
{
	mStr str = MSTR_INIT;
	int code;

	//ファイル名取得

	if(!OpenDialog_textFile(MLK_WINDOW(p), GDAT->strOpenDir.buf, &str, &code))
		return;

	//ディレクトリを記録

	mStrPathGetDir(&GDAT->strOpenDir, str.buf);

	//読み込み

	MainWindow_loadText(p, str.buf, code, 0, FALSE);

	mStrFree(&str);
}

/* ページ番号を指定して移動 */

static void _cmd_move_pageno_dlg(MainWindow *p)
{
	mStr str = MSTR_INIT;
	int no;

	if(GLOBAL_IS_EMPTY_TEXT) return;

	MLK_TRGROUP(TRGROUP_ID_DIALOG);

	mStrSetFormat(&str, "%s (%d-%d)",
		MLK_TR(TRID_DIALOG_PAGENO), 1, GDAT->layout->page_num);
	
	if(mSysDlg_inputTextNum(MLK_WINDOW(p),
		MLK_TR(TRID_DIALOG_PAGENO), str.buf, 0,
		1, GDAT->layout->page_num, &no))
	{
		MainWindow_movePage(p, no - 1);
	}

	mStrFree(&str);
}

/* 行番号を指定して移動 */

static void _cmd_move_lineno_dlg(MainWindow *p)
{
	const char *text;
	int no;

	if(GLOBAL_IS_EMPTY_TEXT) return;

	MLK_TRGROUP(TRGROUP_ID_DIALOG);

	text = MLK_TR(TRID_DIALOG_LINENO);

	if(mSysDlg_inputTextNum(MLK_WINDOW(p),
		text, text, 0,
		1, INT32_MAX, &no))
	{
		MainWindow_movePage_lineno(p, no - 1, TRUE);
	}
}

/* 見出し一覧 */

static void _cmd_move_caption(MainWindow *p)
{
	int page;

	if(GLOBAL_IS_EMPTY_TEXT) return;

	page = CaptionDlg(MLK_WINDOW(p));

	if(page != -1)
		MainWindow_movePage(p, page);
}

/* スタイル変更 */

static void _cmd_style_change(MainWindow *p,int no)
{
	StyleWork_readStyle(GDAT->style, mMenuGetItemText_atIndex(p->menu_style, no));

	MainWindow_text_layout(p);
	MainWindow_setWindowSize(p);
}

/* スタイル設定 */

static void _cmd_opt_style(MainWindow *p)
{
	int ret;

	ret = StyleOptDialog(MLK_WINDOW(p));
	if(!ret) return;

	//再レイアウト

	if(ret & STYLEDLG_F_LAYOUT)
		MainWindow_text_layout(p);

	//画面更新

	if(ret & STYLEDLG_F_UPDATE)
	{
		MainWindow_setWindowSize(p);
		MainWindow_update(p);
	}
}

/* 環境設定 */

static void _cmd_opt_env(MainWindow *p)
{
	int f;

	f = EnvOptDialog(MLK_WINDOW(p));

	//メニューのショートカットキー変更

	if(f & ENVDLG_F_UPDATE_KEY)
		_change_shortcutkey(p);

	//アクセラレータの再セット

	if(f & (ENVDLG_F_UPDATE_KEY | ENVDLG_F_UPDATE_TOOL))
		_set_accelkey(p);

	//ツールのメニュー

	if(f & ENVDLG_F_UPDATE_TOOL)
		_set_tool_menu(p);
}


//========================
// イベント
//========================


/* COMMAND イベント */

static void _event_command(mWidget *wg,mEvent *ev)
{
	MainWindow *p = MAINWINDOW(wg);
	int id = ev->cmd.id;

	//ファイル履歴

	if(id >= MAINWIN_CMDID_RECENTFILE && id < MAINWIN_CMDID_RECENTFILE + RECENTFILE_NUM)
	{
		MainWindow_loadText_recent(p, id - MAINWIN_CMDID_RECENTFILE);
		return;
	}

	//ツール

	if(id >= MAINWIN_CMDID_TOOL && id < MAINWIN_CMDID_TOOL + TOOLITEM_NUM)
	{
		MainWindow_execTool(p, id - MAINWIN_CMDID_TOOL);
		return;
	}

	//スタイル

	if(id >= MAINWIN_CMDID_STYLE && id < MAINWIN_CMDID_STYLE + STYLE_MAXNUM)
	{
		_cmd_style_change(p, id - MAINWIN_CMDID_STYLE);
		return;
	}

	//

	switch(id)
	{
		//---- ファイル

		//開く
		case TRID_MENU_FILE_OPEN:
			_cmd_open(p);
			break;
		//次のファイル
		case TRID_MENU_FILE_NEXTFILE:
			MainWindow_loadText_nextprev(p, TRUE);
			break;
		//前のファイル
		case TRID_MENU_FILE_PREVFILE:
			MainWindow_loadText_nextprev(p, FALSE);
			break;
		//再読み込み
		case TRID_MENU_FILE_RELOAD:
			MainWindow_reloadFile(p);
			break;
		//終了
		case TRID_MENU_FILE_EXIT:
			mGuiQuit();
			break;

		//---- ページ
		
		//次のページ
		case TRID_MENU_PAGE_NEXT:
			MainWindow_movePage(p, PAGENO_NEXT);
			break;
		//前のページ
		case TRID_MENU_PAGE_PREV:
			MainWindow_movePage(p, PAGENO_PREV);
			break;
		//先頭ページ
		case TRID_MENU_PAGE_TOP:
			MainWindow_movePage(p, PAGENO_HOME);
			break;
		//終端ページ
		case TRID_MENU_PAGE_BOTTOM:
			MainWindow_movePage(p, PAGENO_END);
			break;
		//ページ番号指定
		case TRID_MENU_PAGE_PAGENO:
			_cmd_move_pageno_dlg(p);
			break;
		//行番号指定
		case TRID_MENU_PAGE_LINENO:
			_cmd_move_lineno_dlg(p);
			break;
		//見出し
		case TRID_MENU_PAGE_CAPTION:
			_cmd_move_caption(p);
			break;
		//グローバルに追加
		case TRID_MENU_PAGE_ADD_BOOKMARK_GLOBAL:
			BkmWindow_addGlobal(GDAT->bkmwin);
			break;
		//ローカルに追加
		case TRID_MENU_PAGE_ADD_BOOKMARK_LOCAL:
			BkmWindow_addLocal(GDAT->bkmwin);
			break;

		//---- 表示

		//しおり一覧
		case TRID_MENU_VIEW_BOOKMARK:
			BkmWindow_toggle(GDAT->bkmwin);
			break;
		//ツールバー
		case TRID_MENU_VIEW_TOOLBAR:
			GDAT->viewflags ^= VIEWFLAGS_TOOLBAR;

			mWidgetShow(MLK_WIDGET(p->toolbar), -1);
			mWidgetReLayout(MLK_WIDGET(p));
			break;
		//ページ情報
		case TRID_MENU_VIEW_PAGENO:
			GDAT->viewflags ^= VIEWFLAGS_PAGENO;

			MainWindow_update(p);
			break;

		//---- 設定ほか

		//環境設定
		case TRID_MENU_OPT_ENV:
			_cmd_opt_env(p);
			break;
		//スタイル設定
		case TRID_MENU_OPT_STYLE:
			_cmd_opt_style(p);
			break;
		//バージョン情報
		case TRID_MENU_OPT_ABOUT:
			mSysDlg_about(MLK_WINDOW(wg), _VERSION_TEXT);
			break;
	}
}

/* イベントハンドラ */

int _event_handle(mWidget *wg,mEvent *ev)
{
	switch(ev->type)
	{
		//コマンド
		case MEVENT_COMMAND:
			_event_command(wg, ev);
			break;
		//ファイルドロップ
		case MEVENT_DROP_FILES:
			MainWindow_loadText(MAINWINDOW(wg), *(ev->dropfiles.files), -1, 0, FALSE);
			break;
	
		//閉じるボタン
		case MEVENT_CLOSE:
			mGuiQuit();
			break;
		
		default:
			return FALSE;
	}

	return TRUE;
}

