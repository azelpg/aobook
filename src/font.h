/*$
aobook
Copyright (c) 2014-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/********************************
 * フォント
 ********************************/

enum
{
	FONT_CREATE_F_RUBY = 1<<0		//ルビ用
};

enum
{
	FONT_DRAW_F_ROTATE = 1<<0,		//(縦書き) 90度回転
	FONT_DRAW_F_RUBY = 1<<1,		//(縦書き) ルビ文字置換
	FONT_DRAW_F_WIDTH_1_2 = 1<<2,	//1/2幅に置換 (横書き)
	FONT_DRAW_F_WIDTH_1_3 = 1<<3,	//1/3幅に置換
	FONT_DRAW_F_PRINT = 1<<4,		//印刷標準字体に置換
	FONT_DRAW_F_DAKUTEN_VERT = 1<<5	//濁点用、高さ調整
};

mlkbool FontInit(void);
void FontEnd(void);

mFontSystem *FontGetSystem(void);
mFont *FontCreate(const char *text,uint32_t flags);

int FontGetTextWidth_horz(mFont *p,const char *text,int len,uint32_t flags);
void FontDrawText_horz(mFont *p,mPixbuf *img,int x,int y,const char *text,int len,mRgbCol col,uint32_t flags);

int FontGetTextWidth_vert(mFont *p,const uint32_t *text,int len,uint32_t flags);
int FontDrawText_vert(mFont *p,mPixbuf *img,int x,int y,const uint32_t *text,int len,mRgbCol col,uint32_t flags);
