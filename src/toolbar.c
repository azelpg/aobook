/*$
aobook
Copyright (c) 2014-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * Toolbar [ツールバー]
 *****************************************/

#include "mlk_gui.h"
#include "mlk_widget_def.h"
#include "mlk_widget.h"
#include "mlk_imgbutton.h"
#include "mlk_sliderbar.h"
#include "mlk_event.h"

#include "def_global.h"
#include "def_style.h"
#include "func_widget.h"
#include "layout.h"


//-------------------

struct _Toolbar
{
	MLK_CONTAINER_DEF

	mSliderBar *slider;
};

enum
{
	WID_END = 100,
	WID_NEXT,
	WID_PREV,
	WID_TOP,
	WID_SLIDER
};

//-------------------

static const uint8_t g_img_end[]={
0x00,0x00,0x06,0x00,0x06,0x01,0x86,0x01,0xc6,0x01,0xe6,0x01,0xf6,0x3f,0xfe,0x3f,
0xf6,0x3f,0xe6,0x01,0xc6,0x01,0x86,0x01,0x06,0x01,0x06,0x00,0x00,0x00 },
	g_img_next[]={
0x00,0x00,0x80,0x00,0xc0,0x00,0xe0,0x00,0xf0,0x00,0xf8,0x3f,0xfc,0x3f,0xfe,0x3f,
0xfc,0x3f,0xf8,0x3f,0xf0,0x00,0xe0,0x00,0xc0,0x00,0x80,0x00,0x00,0x00 },
	g_img_prev[]={
0x00,0x00,0x80,0x00,0x80,0x01,0x80,0x03,0x80,0x07,0xfe,0x0f,0xfe,0x1f,0xfe,0x3f,
0xfe,0x1f,0xfe,0x0f,0x80,0x07,0x80,0x03,0x80,0x01,0x80,0x00,0x00,0x00 },
	g_img_top[]={
0x00,0x00,0x00,0x30,0x40,0x30,0xc0,0x30,0xc0,0x31,0xc0,0x33,0xfe,0x37,0xfe,0x3f,
0xfe,0x37,0xc0,0x33,0xc0,0x31,0xc0,0x30,0x40,0x30,0x00,0x30,0x00,0x00 };

//-------------------


/* イベントハンドラ */

static int _event_handle(mWidget *wg,mEvent *ev)
{
	if(ev->type == MEVENT_NOTIFY)
	{
		MainWindow *mainwin = GDAT->mainwin;
	
		switch(ev->notify.id)
		{
			case WID_SLIDER:
				if(ev->notify.notify_type == MSLIDERBAR_N_ACTION
					&& (ev->notify.param2 & MSLIDERBAR_ACTION_F_CHANGE_POS))
				{
					MainWindow_movePage(mainwin,
						(((mSliderBar *)ev->notify.widget_from)->sl.max - ev->notify.param1)
							* GDAT->style->b.pages);
				}
				break;
			case WID_NEXT:
				MainWindow_movePage(mainwin, PAGENO_NEXT);
				break;
			case WID_PREV:
				MainWindow_movePage(mainwin, PAGENO_PREV);
				break;
			case WID_TOP:
				MainWindow_movePage(mainwin, PAGENO_HOME);
				break;
			case WID_END:
				MainWindow_movePage(mainwin, PAGENO_END);
				break;
		}
	}

	return FALSE;
}

/* ボタン作成 */

static void _create_button(Toolbar *p,int id,const uint8_t *img)
{
	mImgButton *btt;

	btt = mImgButtonCreate(MLK_WIDGET(p), id, 0, 0, 0);

	mImgButton_setBitImage(btt, MIMGBUTTON_TYPE_1BIT_TP_TEXT, img, 15, 15);
}

/** ツールバーウィジェット作成 */

Toolbar *createToolbarWidget(mWidget *parent)
{
	Toolbar *p;

	p = (Toolbar *)mContainerNew(parent, sizeof(Toolbar));

	mContainerSetType_horz(MLK_CONTAINER(p), 5);
	mContainerSetPadding_pack4(MLK_CONTAINER(p), MLK_MAKE32_4(4,3,4,3));

	p->wg.flayout = MLF_EXPAND_W;
	p->wg.notify_to = MWIDGET_NOTIFYTO_SELF;
	p->wg.draw_bkgnd = mWidgetDrawBkgndHandle_fillFace;
	p->wg.event = _event_handle;

	//初期非表示

	if(!(GDAT->viewflags & VIEWFLAGS_TOOLBAR))
		mWidgetShow(MLK_WIDGET(p), 0);

	//------

	//終端

	_create_button(p, WID_END, g_img_end);

	//次へ

	_create_button(p, WID_NEXT, g_img_next);
	
	//スライダー

	p->slider = mSliderBarCreate(MLK_WIDGET(p), WID_SLIDER, MLF_EXPAND_W | MLF_MIDDLE, 0, MSLIDERBAR_S_HORZ);

	//前へ

	_create_button(p, WID_PREV, g_img_prev);

	//先頭

	_create_button(p, WID_TOP, g_img_top);

	return p;
}

/** ページ数変更時 */

void Toolbar_changePageNum(Toolbar *p)
{
	int max;

	//最大数 (見開きの場合は 1/2)

	max = GDAT->layout->page_num;

	if(GDAT->style->b.pages == 2)
		max = (max + 1) / 2;

	if(max) max--;

	//

	mSliderBarSetRange(p->slider, 0, max);

	//1ページしかない場合は無効

	mWidgetEnable(MLK_WIDGET(p->slider), (max != 0));
}

/** ページ位置変更時 */

void Toolbar_changePagePos(Toolbar *p)
{
	int no;

	//スライダー位置

	no = LayoutGetPageNo(GDAT->curpage);

	if(GDAT->style->b.pages == 2) no /= 2;

	mSliderBarSetPos(p->slider, p->slider->sl.max - no);
}
