/*$
aobook
Copyright (c) 2014-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * 見出し一覧ダイアログ
 *****************************************/

#include "mlk_gui.h"
#include "mlk_widget_def.h"
#include "mlk_widget.h"
#include "mlk_window.h"
#include "mlk_listview.h"
#include "mlk_event.h"
#include "mlk_str.h"
#include "mlk_columnitem.h"

#include "def_global.h"
#include "trid.h"
#include "layout.h"


//-------------------

typedef struct
{
	MLK_DIALOG_DEF

	mListView *lv;
}_dialog;

#define WID_LIST 100

//-------------------


/* 見出しリストセット */

static void _set_list(_dialog *p)
{
	TitleItem *pi;
	mListView *list = p->lv;
	mStr str = MSTR_INIT;
	int toplevel,i;

	//大/中/小のうち、トップレベル

	for(toplevel = 0; toplevel < 3 && GDAT->layout->title_num[toplevel] == 0; toplevel++);

	//

	for(pi = (TitleItem *)GDAT->layout->list_title.top; pi; pi = (TitleItem *)pi->i.next)
	{
		mStrEmpty(&str);

		//見出しレベルに応じてインデント

		for(i = pi->type - toplevel; i > 0; i--)
			mStrAppendText(&str, "  ");

		mStrAppendText(&str, pi->text);
	
		mListViewAddItem(list, str.buf, -1, MCOLUMNITEM_F_COPYTEXT, pi->pageno);
	}

	mStrFree(&str);

	//最初の項目を選択

	mListViewSetAutoWidth(list, FALSE);
	mListViewSetFocusItem_index(list, 0);
}

/* イベント */

static int _event_handle(mWidget *wg,mEvent *ev)
{
	if(ev->type == MEVENT_NOTIFY
		&& ev->notify.id == WID_LIST
		&& ev->notify.notify_type == MLISTVIEW_N_ITEM_L_DBLCLK)
	{
		//ダブルクリック

		mDialogEnd(MLK_DIALOG(wg), TRUE);
		return 1;
	}
	else
		return mDialogEventDefault_okcancel(wg, ev);
}

/* ダイアログ作成 */

static _dialog *_create_dialog(mWindow *parent)
{
	_dialog *p;
	mListView *list;

	p = (_dialog *)mDialogNew(parent, sizeof(_dialog),
		MTOPLEVEL_S_DIALOG_NORMAL | MTOPLEVEL_S_NO_INPUT_METHOD);
	if(!p) return NULL;

	p->wg.event = _event_handle;
	p->ct.sep = 12;

	mContainerSetPadding_same(MLK_CONTAINER(p), 8);

	mToplevelSetTitle(MLK_TOPLEVEL(p), MLK_TR2(TRGROUP_ID_DIALOG, TRID_DIALOG_CAPTION));

	//リスト

	p->lv = list = mListViewCreate(MLK_WIDGET(p), WID_LIST, MLF_EXPAND_WH, 0,
		0, MSCROLLVIEW_S_HORZVERT_FRAME);

	mWidgetSetInitSize_fontHeightUnit(MLK_WIDGET(list), 18, 18);

	//OK/Cancel

	mContainerCreateButtons_okcancel(MLK_WIDGET(p), 0);

	//リストをセット

	if(GDAT->layout->list_title.num)
		_set_list(p);

	return p;
}


/** 見出しダイアログ
 *
 * return: ページ番号。-1 でキャンセル */

int CaptionDlg(mWindow *parent)
{
	_dialog *p;
	int page;

	p = _create_dialog(parent);
	if(!p) return -1;

	mWindowResizeShow_initSize(MLK_WINDOW(p));

	if(!mDialogRun(MLK_DIALOG(p), FALSE))
		page = -1;
	else
		page = mListViewGetItemParam(p->lv, -1);

	mWidgetDestroy(MLK_WIDGET(p));

	return page;
}
