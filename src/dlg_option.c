/*$
aobook
Copyright (c) 2014-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * 環境設定ダイアログ
 *****************************************/

#include <string.h>

#include "mlk_gui.h"
#include "mlk_widget_def.h"
#include "mlk_widget.h"
#include "mlk_window.h"
#include "mlk_event.h"
#include "mlk_str.h"
#include "mlk_columnitem.h"

#include "mlk_listview.h"
#include "mlk_pager.h"

#include "def_global.h"
#include "trid.h"
#include "func_widget.h"

#include "pv_envdlg.h"


//--------------------

typedef struct
{
	MLK_DIALOG_DEF

	mListView *lv;
	mPager *pager;

	EnvOptData dat;
}_dialog;

enum
{
	WID_LIST = 100
};

enum
{
	TRID_TITLE,
	TRID_PAGE_BUTTON,
	TRID_PAGE_TOOL,
	TRID_PAGE_SHORTCUTKEY
};

//--------------------

mlkbool EnvDlg_createPage_button(mPager *p,mPagerInfo *info);
mlkbool EnvDlg_createPage_tool(mPager *p,mPagerInfo *info);
mlkbool EnvDlg_createPage_sckey(mPager *p,mPagerInfo *info);

//--------------------



//===================


/* ページ作成 */

static void _create_page(_dialog *p,int no)
{
	mFuncPagerCreate func[] = {
		EnvDlg_createPage_button, EnvDlg_createPage_tool, EnvDlg_createPage_sckey
	};

	mPagerSetPage(p->pager, func[no]);

	mWidgetReLayout(MLK_WIDGET(p->pager));
}

/* イベントハンドラ */

static int _event_handle(mWidget *wg,mEvent *ev)
{
	if(ev->type == MEVENT_NOTIFY
		&& ev->notify.id == WID_LIST
		&& ev->notify.notify_type == MLISTVIEW_N_CHANGE_FOCUS)
	{
		//ページ変更

		_create_page((_dialog *)wg, MLK_COLUMNITEM(ev->notify.param1)->param);
		
		return TRUE;
	}

	return mDialogEventDefault_okcancel(wg, ev);
}


//=======================
// 作成
//=======================


/* 編集用データにコピー */

static void _copy_data(EnvOptData *pd,GlobalData *ps)
{
	memcpy(pd->bttact, ps->bttact, BUTTONACT_BTT_NUM);

	mStrArrayCopy(pd->strtool, ps->strTool, TOOLITEM_NUM);

	//ショートカットキー
	
	pd->sckey = (ShortcutKey *)mMalloc(sizeof(ShortcutKey) * GDAT->sckey_num);

	memcpy(pd->sckey, ps->sckey, sizeof(ShortcutKey) * GDAT->sckey_num);
}

/* 終了時、値を適用 */

static int _apply_value(_dialog *p)
{
	GlobalData *pd = GDAT;
	EnvOptData *ps = &p->dat;
	ShortcutKey *psc1,*psc2;
	int ret = 0;

	memcpy(pd->bttact, ps->bttact, BUTTONACT_BTT_NUM);

	//ツール

	mStrArrayCopy(pd->strTool, ps->strtool, TOOLITEM_NUM);

	if(ps->fchange_tool)
		ret |= ENVDLG_F_UPDATE_TOOL;

	//ショートカットキー

	psc1 = pd->sckey;
	psc2 = ps->sckey;

	for(; psc1->id; psc1++, psc2++)
	{
		if(psc1->key != psc2->key)
		{
			ret |= ENVDLG_F_UPDATE_KEY;
			break;
		}
	}

	memcpy(pd->sckey, ps->sckey, sizeof(ShortcutKey) * GDAT->sckey_num);

	return ret;
}

/* ウィジェット作成 */

static void _create_widget(_dialog *p)
{
	mWidget *ct;
	mListView *lv;
	int i,fonth;

	fonth = mWidgetGetFontHeight(MLK_WIDGET(p));

	ct = mContainerCreateHorz(MLK_WIDGET(p), 10, MLF_EXPAND_WH, 0);

	//リスト

	p->lv = lv = mListViewCreate(ct, WID_LIST, MLF_EXPAND_H, 0,
		0, MSCROLLVIEW_S_VERT | MSCROLLVIEW_S_FRAME);

	lv->wg.initH = fonth * 18;

	for(i = 0; i < 3; i++)
		mListViewAddItem_text_static_param(lv, MLK_TR(TRID_PAGE_BUTTON + i), i);

	mListViewSetItemHeight_min(lv, fonth + 2);
	mListViewSetAutoWidth(lv, TRUE);

	mListViewSetFocusItem_index(lv, 0);

	//mPager

	p->pager = mPagerCreate(ct, MLF_EXPAND_WH, 0);

	mPagerSetDataPtr(p->pager, &p->dat);
}

/* ダイアログ作成 */

static _dialog *_create_dialog(mWindow *parent)
{
	_dialog *p;

	p = (_dialog *)mDialogNew(parent, sizeof(_dialog), MTOPLEVEL_S_DIALOG_NORMAL);
	if(!p) return NULL;

	p->wg.event = _event_handle;

	//データコピー

	_copy_data(&p->dat, GDAT);

	//

	MLK_TRGROUP(TRGROUP_ID_DLG_ENV);

	mToplevelSetTitle(MLK_TOPLEVEL(p), MLK_TR(TRID_TITLE));

	mContainerSetPadding_same(MLK_CONTAINER(p), 8);

	//ウィジェット

	_create_widget(p);

	mContainerCreateButtons_okcancel(MLK_WIDGET(p), MLK_MAKE32_4(0,10,0,0));

	//初期ページ

	mPagerSetPage(p->pager, EnvDlg_createPage_button);

	return p;
}

/** 環境設定ダイアログ実行
 *
 * return: フラグ */

int EnvOptDialog(mWindow *parent)
{
	_dialog *p;
	int ret;

	p = _create_dialog(parent);
	if(!p) return 0;

	mWindowResizeShow_initSize(MLK_WINDOW(p));

	ret = mDialogRun(MLK_DIALOG(p), FALSE);
	
	//適用

	if(ret)
	{
		//現在の値取得

		mPagerGetPageData(p->pager);

		//

		ret = _apply_value(p);
	}

	//削除

	mStrArrayFree(p->dat.strtool, TOOLITEM_NUM);
	mFree(p->dat.sckey);

	mWidgetDestroy(MLK_WIDGET(p));

	return ret;
}

