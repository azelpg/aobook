/*$
aobook
Copyright (c) 2014-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/********************************
 * 翻訳文字列ID
 ********************************/

/* グループID */

enum TRGROUP_ID
{
	TRGROUP_ID_MESSAGE = 0,
	TRGROUP_ID_FILEDIALOG,
	TRGROUP_ID_DIALOG,
	TRGROUP_ID_BOOKMARK,
	TRGROUP_ID_DLG_STYLE,
	TRGROUP_ID_DLG_ENV,
	TRGROUP_ID_MENU = 1000
};

/* メッセージ */

enum
{
	TRID_MES_ERR_LOADFILE = 1,
	TRID_MES_ERR_CONVCODE,
};

/* 他ダイアログ */

enum
{
	TRID_DIALOG_PAGENO = 0,
	TRID_DIALOG_LINENO,
	TRID_DIALOG_CAPTION
};

/* メニュー */

enum
{
	TRID_MENU_SCKEY_TOP = 1000,
	TRID_MENU_SCKEY_END = 1500,

	//トップ
	TRID_MENU_TOP_FILE = 1,
	TRID_MENU_TOP_PAGE,
	TRID_MENU_TOP_TOOL,
	TRID_MENU_TOP_VIEW,
	TRID_MENU_TOP_OPTION,

	//ファイル
	TRID_MENU_FILE_OPEN = 1000,
	TRID_MENU_FILE_NEXTFILE,
	TRID_MENU_FILE_PREVFILE,
	TRID_MENU_FILE_RELOAD,
	TRID_MENU_FILE_RECENTFILE,
	TRID_MENU_FILE_EXIT,

	//ページ
	TRID_MENU_PAGE_NEXT = 1100,
	TRID_MENU_PAGE_PREV,
	TRID_MENU_PAGE_TOP,
	TRID_MENU_PAGE_BOTTOM,
	TRID_MENU_PAGE_PAGENO,
	TRID_MENU_PAGE_LINENO,
	TRID_MENU_PAGE_CAPTION,
	TRID_MENU_PAGE_ADD_BOOKMARK_GLOBAL,
	TRID_MENU_PAGE_ADD_BOOKMARK_LOCAL,

	//表示
	TRID_MENU_VIEW_STYLE = 1200,
	TRID_MENU_VIEW_BOOKMARK,
	TRID_MENU_VIEW_TOOLBAR,
	TRID_MENU_VIEW_PAGENO,

	//設定ほか
	TRID_MENU_OPT_STYLE = 1300,
	TRID_MENU_OPT_ENV,
	TRID_MENU_OPT_ABOUT
};
