/*$
aobook
Copyright (c) 2014-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * しおりデータ
 *****************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mlk_gui.h"
#include "mlk_list.h"
#include "mlk_str.h"
#include "mlk_stdio.h"
#include "mlk_file.h"

#include "def_global.h"
#include "bookmark.h"
#include "layout.h"


//----------------

#define _BKM_GLOBAL_FILENAME "bkm_global.txt"
#define _BKM_MAX_NUM  200

//----------------



/* アイテム破棄ハンドラ */

static void _local_destroy(mList *list,mListItem *pi)
{
	mFree(BKM_LOCAL(pi)->comment);
}

/** しおりデータ初期化 */

void Bookmark_init(void)
{
	GDAT->list_bkm_local.item_destroy = _local_destroy;
}


//========================
// グローバル
//========================


/** 現在のページをグローバルに追加 */

BkmGlobal *BkmGlobal_add(void)
{
	BkmGlobal *pi;
	int len;

	if(!GDAT->curpage) return NULL;

	//最大数

	if(GDAT->list_bkm_global.num == _BKM_MAX_NUM)
		return NULL;

	//追加

	len = GDAT->strFileName.len;

	pi = (BkmGlobal *)mListAppendNew(&GDAT->list_bkm_global, sizeof(BkmGlobal) + len);
	if(!pi) return NULL;

	pi->lineno = LayoutGetPageLineNo(GDAT->curpage);

	memcpy(pi->fname, GDAT->strFileName.buf, len);

	//

	GDAT->fchange_bkm_global = TRUE;

	return pi;
}

/** リストの表示用文字列取得 */

void BkmGlobal_getListStr(mStr *str,BkmGlobal *pi)
{
	mStr dir = MSTR_INIT,fname = MSTR_INIT;

	mStrPathGetDir(&dir, pi->fname);
	mStrPathGetBasename(&fname, pi->fname);

	mStrSetFormat(str, "[L%d] %t | %t", pi->lineno + 1, &fname, &dir);

	mStrFree(&dir);
	mStrFree(&fname);
}

/** ファイルに保存 */

void BkmGlobal_saveFile(void)
{
	FILE *fp;
	BkmGlobal *pi;
	mStr str = MSTR_INIT;

	//更新された時のみ保存

	if(!GDAT->fchange_bkm_global) return;

	//開く

	mGuiGetPath_config(&str, _BKM_GLOBAL_FILENAME);

	fp = mFILEopen(str.buf, "wt");

	mStrFree(&str);

	if(!fp) return;

	//

	for(pi = BKM_GLOBAL(GDAT->list_bkm_global.top); pi; pi = BKM_GLOBAL(pi->i.next))
		fprintf(fp, "%d %s\n", pi->lineno + 1, pi->fname);

	fclose(fp);
}

/** ファイルから読み込み */

void BkmGlobal_loadFile(void)
{
	mFileText *ft;
	BkmGlobal *item;
	char *pc,*split;
	mStr str = MSTR_INIT;
	int len;

	mGuiGetPath_config(&str, _BKM_GLOBAL_FILENAME);

	mFileText_readFile(&ft, str.buf);

	mStrFree(&str);

	if(!ft) return;

	//

	while(1)
	{
		pc = mFileText_nextLine_skip(ft);
		if(!pc) break;

		//空白で区切る

		split = strchr(pc, ' ');
		if(!split) continue;

		*(split++) = 0;

		len = strlen(split);

		//追加

		item = (BkmGlobal *)mListAppendNew(&GDAT->list_bkm_global, sizeof(BkmGlobal) + len);
		if(item)
		{
			item->lineno = atoi(pc) - 1;

			memcpy(item->fname, split, len);
		}
	}

	mFileText_end(ft);
}


//========================
// local
//========================


/* ソート関数 */

static int _sortfunc_local(mListItem *p1,mListItem *p2,void *param)
{
	int n1,n2;

	n1 = BKM_LOCAL(p1)->lineno;
	n2 = BKM_LOCAL(p2)->lineno;

	if(n1 == n2)
		return 0;
	else if(n1 < n2)
		return -1;
	else
		return 1;
}

/** ローカルに追加 */

BkmLocal *BkmLocal_add(void)
{
	BkmLocal *pi;
	int lineno;

	if(!GDAT->curpage) return NULL;

	//最大数

	if(GDAT->list_bkm_local.num == _BKM_MAX_NUM)
		return NULL;

	//同じ行番号があるか

	lineno = LayoutGetPageLineNo(GDAT->curpage);

	MLK_LIST_FOR(GDAT->list_bkm_local, pi, BkmLocal)
	{
		if(pi->lineno == lineno)
			return NULL;
	}

	//追加

	pi = (BkmLocal *)mListAppendNew(&GDAT->list_bkm_local, sizeof(BkmLocal));
	if(!pi) return NULL;

	pi->lineno = lineno;

	//行番号順にソート

	mListSort(&GDAT->list_bkm_local, _sortfunc_local, 0);

	return pi;
}

/** リスト表示用文字列取得 */

void BkmLocal_getListStr(mStr *str,BkmLocal *pi)
{
	if(pi->comment)
		mStrSetFormat(str, "L%d: %s", pi->lineno + 1, pi->comment);
	else
		mStrSetFormat(str, "L%d", pi->lineno + 1);
}

/** ファイルに保存 */

void BkmLocal_saveFile(const char *fname)
{
	FILE *fp;
	BkmLocal *pi;

	fp = mFILEopen(fname, "wt");
	if(!fp) return;

	MLK_LIST_FOR(GDAT->list_bkm_local, pi, BkmLocal)
	{
		if(pi->comment)
			fprintf(fp, "%d %s\n", pi->lineno + 1, pi->comment);
		else
			fprintf(fp, "%d\n", pi->lineno + 1);
	}

	fclose(fp);
}

/** ファイルから読み込み */

void BkmLocal_loadFile(const char *fname)
{
	mFileText *ft;
	BkmLocal *item;
	char *pc,*split;

	mListDeleteAll(&GDAT->list_bkm_local);

	//

	mFileText_readFile(&ft, fname);
	if(!ft) return;

	while(1)
	{
		pc = mFileText_nextLine_skip(ft);
		if(!pc) break;

		//空白で区切る (なければコメントなし)

		split = strchr(pc, ' ');

		if(split)
			*(split++) = 0;

		//追加

		item = (BkmLocal *)mListAppendNew(&GDAT->list_bkm_local, sizeof(BkmLocal));
		if(item)
		{
			item->lineno = atoi(pc) - 1;
			item->comment = (split && *split)? mStrdup(split): NULL;
		}
	}

	mFileText_end(ft);
}

