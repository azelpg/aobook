/*$
aobook
Copyright (c) 2014-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/********************************
 * しおりデータ
 ********************************/

#define BKM_GLOBAL(p) ((BkmGlobal *)(p))
#define BKM_LOCAL(p)  ((BkmLocal *)(p))

typedef struct
{
	mListItem i;
	int lineno;
	char fname[1];	//ファイルパス
}BkmGlobal;

typedef struct
{
	mListItem i;
	int lineno;
	char *comment; //NULL でなし
}BkmLocal;


void Bookmark_init(void);

BkmGlobal *BkmGlobal_add(void);
void BkmGlobal_getListStr(mStr *str,BkmGlobal *pi);
void BkmGlobal_saveFile(void);
void BkmGlobal_loadFile(void);

BkmLocal *BkmLocal_add(void);
void BkmLocal_getListStr(mStr *str,BkmLocal *pi);
void BkmLocal_saveFile(const char *fname);
void BkmLocal_loadFile(const char *fname);

