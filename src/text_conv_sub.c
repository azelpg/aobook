/*$
aobook
Copyright (c) 2014-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*******************************************
 * 内部データ変換処理のサブ関数
 *******************************************/

#include "mlk.h"
#include "mlk_buf.h"
#include "mlk_list.h"
#include "mlk_charset.h"
#include "mlk_unicode.h"

#include "textconv.h"
#include "def_textdata.h"


//--------------

#define _ITEM_TYPE_IS_CMD(p)     ((p)->type != DATATYPE_CHAR)
#define _ITEM_TYPE_IS_CHAR(p)    ((p)->type == DATATYPE_CHAR)
#define _ITEM_TYPE_ISNOT_CHAR(p) ((p)->type != DATATYPE_CHAR)

//--------------


//=============================
// UTF-16BE から1行取得
//=============================


/** UTF-16BE から1文字取得
 *
 * tonext: 取得後、次の位置へ進める
 * return: UTF-32 文字。0 で終端。 */

static uint32_t _get_src_char(ConvertData *p,mlkbool tonext)
{
	uint8_t *ps = p->pcur;
	uint32_t c,c2;

	if(p->remain == 0) return 0;

	//2byte 取得

	c = (ps[0] << 8) | ps[1];
	ps += 2;

	//4byte 文字

	if((c & 0xfc00) == 0xd800)
	{
		c2 = (ps[0] << 8) | ps[1];
		ps += 2;

		c = (((c & 0x03c0) + 0x40) << 10) | ((c & 0x3f) << 10) | (c2 & 0x3ff);
	}

	//ポインタを進める

	if(tonext)
	{
		p->remain -= ps - p->pcur;
		p->pcur = ps;
	}

	return c;
}

/** UTF-16BE から UTF-32 文字列1行分取得
 *
 * - linebuf に uint32 の文字列がセットされる。
 * - 終端には 0 が追加される。
 * - BOM/改行文字は除外される。
 *
 * return: TRUE で成功。FALSE で終端。 */

mlkbool convert_getLine(ConvertData *p)
{
	mBuf *buf = &p->linebuf;
	uint32_t c;
	mlkbool end = FALSE;

	mBufReset(buf);

	//1文字ずつ取得し、改行で終了

	while(1)
	{
		c = _get_src_char(p, TRUE);

		if(c == 0)
		{
			//終端
			end = TRUE;
			break;
		}
		else if(c == '\n')
			break;
		else if(c == '\r')
		{
			if(_get_src_char(p, FALSE) == '\n')
				_get_src_char(p, TRUE);

			break;
		}
		else if(c == 0xfeff)
			//BOM はスキップ
			continue;

		mBufAppend(buf, &c, 4);
	}

	//

	if(end && buf->cursize == 0)
		//終端 (文字が一つもない時)
		return FALSE;
	else
	{
		//0 を追加

		c = 0;
		mBufAppend(buf, &c, 4);

		return TRUE;
	}
}


//=============================
// UTF-32 文字列関連
//=============================


/* ISO-2022-JP-3 コードから Unicode 文字に変換 */

static uint32_t _get_utf32_from_iso(const uint8_t *src,int size)
{
	uint8_t *buf;
	uint32_t c;

	buf = (uint8_t *)mConvertCharset(src, size, "ISO-2022-JP-3", "UTF-32BE", NULL, 0);
	if(!buf) return 0;

	c = ((uint32_t)buf[0] << 24) | (buf[1] << 16) | (buf[2] << 8) | buf[3];

	mFree(buf);

	return c;
}


/** UTF-32 文字列から '］' を見つけて、0 に置き換え、*pptop にその次の位置をセット
 *
 * return: TRUE で見つかった。FALSE で見つからなかった。 */

mlkbool convert_text_kagiClose(uint32_t **pptext)
{
	uint32_t *p;

	for(p = *pptext; *p && *p != U'］'; p++);

	if(*p == 0)
		return FALSE;
	else
	{
		*p = 0;
		*pptext = p + 1;

		return TRUE;
	}
}

/** UTF-32 文字列から16進数の数値取得 */

uint32_t convert_text_to_hex(uint32_t *text)
{
	uint32_t v = 0,c;

	while(*text)
	{
		c = *(text++);

		if(c >= '0' && c <= '9')
			c -= '0';
		else if(c >= 'a' && c <= 'f')
			c = c - 'a' + 10;
		else if(c >= 'A' && c <= 'F')
			c = c - 'A' + 10;
		else
			break;

		v <<= 4;
		v |= c;
	}

	return v;
}

/** JIS 面区点 (面-区-点) 文字列から Unicode 文字取得
 *
 * return: 0 でエラー */

uint32_t convert_text_to_jis(uint32_t *text)
{
	int no[3] = {0,0,0},num = 0;
	uint32_t c;
	uint8_t iso[6] = {0x1b,'$','(',0,0,0};

	//"面-区-点" の各番号を no に取得

	while(*text && num < 3)
	{
		c = *(text++);

		if(c == '-')
			num++;
		else if(c >= '0' && c <= '9')
		{
			no[num] *= 10;
			no[num] += c - '0';
		}
		else
			break;
	}

	if(num < 2) return 0;

	//面は 1 or 2
	
	if(no[0] != 1 && no[0] != 2) return 0;

	//区・点は 0-95
	
	if(no[1] > 96 || no[2] > 96) return 0;

	//ISO-2022-JP-3 コードに変換

	iso[3] = (no[0] == 1)? 'O': 'P';
	iso[4] = 0x20 + no[1];
	iso[5] = 0x20 + no[2];

	return _get_utf32_from_iso(iso, 6);
}

/** Unicode 文字が漢字かどうか */

mlkbool convert_is_kanji(uint32_t c)
{
	return ((c >= 0x2E80 && c <= 0x2EF3)
		|| (c >= 0x2F00 && c <= 0x2FD5)
		|| (c >= 0x3400 && c <= 0x4DB5)
		|| (c >= 0x4E00 && c <= 0x9FFF)
		|| c == U'々' || c == U'〆' || c == U'〇' || c == U'ヶ' || c == U'〻'
		|| (c >= 0xF900 && c <= 0xFAFF)
		|| (c >= 0x20000 && c <= 0x2A6D6)
		|| (c >= 0x2F800 && c <= 0x2FA1D));

	/* 仝 = U+4EDD */
}

/** UTF-32 と UTF-16 (常に2byte) 文字列比較 */

mlkbool convert_compareText16(uint32_t *text,const uint16_t *cmp)
{
	for(; *cmp && *text == *cmp; text++, cmp++);

	return (*cmp == 0);
}


//=============================
// ［＃...］ マッチ
//=============================

/* [!] 終端の '］' は 0 に置き換えられている */


/** 配列での定義から、注記判定
 *
 * '*' は、全角数字としてマッチさせ、val1,2 にセット。
 * (最大2箇所。数値は 0-99 まで)
 *
 * return: TRUE でマッチした */

mlkbool convert_matchCmd_array(uint32_t *ptext,const CmdMatchDef *array,CmdMatchData *dst)
{
	uint32_t *pt;
	const uint16_t *pcmp;
	int val[2],valno;

	for(; array->str; array++)
	{
		//'*' の数値初期化
		
		valno = 0;
		val[0] = -1;
		val[1] = -1;

		//文字列比較

		pcmp = array->str;
	
		for(pt = ptext; *pt && *pcmp; pt++)
		{
			//'*' は全角数字にマッチ
		
			if(*pcmp == '*')
			{
				if(*pt < U'０' || *pt > U'９')
				{
					//全角数字以外 -> マッチ解除
					pcmp++;
					valno++;
				}
				else
				{
					//数字マッチ中
					
					if(val[valno] == -1)
						val[valno] = 0;
				
					val[valno] *= 10;
					val[valno] += *pt - U'０';
					continue;
				}
			}

			//通常文字比較
			
			if(*pt != *pcmp) break;

			pcmp++;
		}

		//すべてマッチした

		if(*pcmp == 0 && *pt == 0)
		{
			dst->type = array->cmdtype;
			dst->flags = array->flags;

			if(array->flags & CMDMATCHDEF_F_HAVE_VAL1)
				//定義データから
				dst->val1 = array->val1;
			else
			{
				//数字のマッチから
				
				if(val[0] > 99) val[0] = 99;
				if(val[1] > 99) val[1] = 99;

				dst->val1 = (uint8_t)val[0]; //値がない (-1) の場合、255 になる
				dst->val2 = (uint8_t)val[1];
			}

			return TRUE;
		}
	}

	return FALSE;
}

/** 注記 "「*」は|に*" 判定
 *
 * 「」内の文字列は、効果がかかる直前の文字列。dst->text,textlen にセット。
 * 「」以降の文字列は、定義配列から判定。 */

mlkbool convert_matchCmd_kagi(uint32_t *ptext,const CmdMatchDef *array,CmdMatchData *dst)
{
	uint32_t *pt,*ptop;
	const uint16_t *pcmp;

	//"「」" 内の文字列取得

	for(pt = ptext; *pt && *pt != U'」'; pt++);

	if(!(*pt)) return FALSE;

	dst->text = ptext;
	dst->textlen = pt - ptext;

	//'」'以降を比較

	ptop = pt + 1;

	for(; array->str; array++)
	{
		//文字列比較
		
		for(pt = ptop, pcmp = array->str; *pcmp && *pt == *pcmp; pt++, pcmp++);

		if(*pcmp == 0 && *pt == 0)
		{
			dst->type = array->cmdtype;
			dst->flags = array->flags;

			if(array->flags & CMDMATCHDEF_F_HAVE_VAL1)
				dst->val1 = array->val1;

			return TRUE;
		}
	}

	return FALSE;
}

/** 挿絵注記の判定
 *
 * "［＃caption（filename[、...]）入る］" */

mlkbool convert_matchCmd_picture(uint32_t *ptext,CmdMatchData *dst)
{
	uint32_t c,*pfile = NULL;
	int len = -1;

	for(; *ptext; ptext++)
	{
		c = *ptext;
	
		if(c == U'（')
			//ファイル名部分の開始
			pfile = ptext + 1;
		else if(c == U'、' || c == U'）')
		{
			//ファイル名部分の終了
			
			if(pfile && len == -1)
				len = ptext - pfile;
		}
		else if(c == U'入' && ptext[1] == U'る' && ptext[2] == 0)
		{
			//最後が "入る"

			if(!pfile || len <= 0) return FALSE;

			dst->type = COMMAND_PICTURE;
			dst->text = pfile;
			dst->textlen = len;
			dst->flags = CMDMATCHDEF_F_NO_ENTER;
			return TRUE;
		}
	}

	return FALSE;
}


//=============================
// 中間データ追加
//=============================


/** 中間データに通常１文字追加 */

void convert_addItem_normal(ConvertData *p,uint32_t c)
{
	ItemChar *pi;

	pi = (ItemChar *)mListAppendNew(&p->list, sizeof(ItemChar));
	if(pi)
	{
		pi->code = c;
		pi->type = DATATYPE_CHAR;
	}
}

/** 中間データにコマンドを追加
 *
 * insert != NULL の場合、その位置に挿入 */

void convert_addItem_command(ConvertData *p,CmdMatchData *dat)
{
	ItemChar *pi;

	//追加/挿入

	if(dat->insert)
		pi = (ItemChar *)mListInsertNew(&p->list, MLISTITEM(dat->insert), sizeof(ItemChar));
	else
		pi = (ItemChar *)mListAppendNew(&p->list, sizeof(ItemChar));

	if(!pi) return;

	//

	if(dat->type == COMMAND_PICTURE)
	{
		//挿絵
		pi->type = DATATYPE_PICTURE;
		pi->text = dat->text;
		pi->textlen = dat->textlen;
	}
	else
	{
		pi->type = (dat->val1 == COMMAND_VALUE_NONE)? DATATYPE_COMMAND: DATATYPE_COMMAND_VAL;
		pi->cmdno   = dat->type;
		pi->cmdval1 = dat->val1;
		pi->cmdval2 = dat->val2;
	}

	//"「*」は|に*" の場合、END のコマンドを追加

	if(dat->insert)
	{
		pi = (ItemChar *)mListAppendNew(&p->list, sizeof(ItemChar));
		if(pi)
		{
			pi->type = DATATYPE_COMMAND;
			pi->cmdno = dat->type + 1;
			pi->cmdval1 = dat->val1;
			pi->cmdval2 = dat->val2;
		}
	}
}


//=============================
// 中間データサブ処理
//=============================


/* 中間データの一番最後の通常文字の位置を取得 */

static ItemChar *_get_list_bottom_char(ConvertData *p)
{
	ItemChar *pi;

	for(pi = (ItemChar *)p->list.bottom;
		pi && _ITEM_TYPE_ISNOT_CHAR(pi); pi = (ItemChar *)pi->i.prev);

	return pi;
}

/** ルビの親文字列すべてにルビの情報をセット
 *
 * pitop: 親文字列の先頭の一つ前の位置 (NULL で行頭) */

void convert_setRubyInfo(ConvertData *p,ItemChar *pitop,uint32_t *rubytop,uint16_t rubylen)
{
	ItemChar *pi;

	//先頭位置

	if(pitop)
		pi = (ItemChar *)pitop->i.next;
	else
		pi = (ItemChar *)p->list.top;

	//先頭からリスト終端の CHAR タイプを親文字列とし、情報セット

	for(; pi; pi = (ItemChar *)pi->i.next)
	{
		if(_ITEM_TYPE_IS_CHAR(pi))
		{
			pi->text = rubytop;
			pi->textlen = rubylen;
		}
	}
}

/** ルビの親文字列を自動判定し、ルビ情報セット
 *
 * rubytop: ルビ文字列の先頭
 * rubylen: ルビ文字列の長さ */

void convert_setRubyInfo_auto(ConvertData *p,uint32_t *rubytop,uint16_t rubylen)
{
	ItemChar *pi;
	uint32_t c;
	mlkbool is_alphabet;

	//中間データの一番最後の通常文字の位置

	pi = _get_list_bottom_char(p);
	if(!pi) return;

	//最後の文字が半角英字か

	c = pi->code;

	is_alphabet = ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z'));

	//最後の文字から前方向に判定しつつ、ルビ情報セット

	for(; pi; pi = (ItemChar *)pi->i.prev)
	{
		if(_ITEM_TYPE_ISNOT_CHAR(pi)) continue;

		//すでにルビがセットされている文字なら終了
		
		if(pi->text) break;

		//文字判定
	
		c = pi->code;

		if(is_alphabet)
		{
			//[半角英字で判定] スペースまたは半角英字以外の文字で終了
			
			if(c == ' ') break;
			
			if(!((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')))
				break;
		}
		else
		{
			//漢字以外の文字で終了
			
			if(!convert_is_kanji(c)) break;
		}

		//セット

		pi->text = rubytop;
		pi->textlen = rubylen;
	}
}

/** "「*」は|に*" 時、「」内の効果をかける文字列を、中間データの終端から検索し、
 *  一致した場合、その文字列の先頭位置を dat->insert にセット。 */

mlkbool convert_compareCmdTextLast(ConvertData *p,CmdMatchData *dat)
{
	ItemChar *pi;
	uint32_t *pt;
	int len;

	len = dat->textlen;
	if(len == 0) return FALSE;

	//一番最後の通常文字位置

	pi = _get_list_bottom_char(p);
	if(!pi) return FALSE;

	//文字列を終端から比較

	pt = dat->text + len - 1;

	for(; pi; pi = (ItemChar *)pi->i.prev)
	{
		if(_ITEM_TYPE_ISNOT_CHAR(pi)) continue;

		if(pi->code != *pt) break;

		pt--;
		len--;

		if(len == 0) break;
	}

	//len == 0 の時、一致

	if(len)
		return FALSE;
	else
	{
		dat->insert = pi;
		return TRUE;
	}
}


//==================================
// 中間データ => 出力データ変換
//==================================


/* 32bit 文字列追加 */

static void _output_add_text32(mBuf *buf,ItemChar *pi,uint16_t len)
{
	//長さ

	mBufAppend(buf, &len, 2);

	//UTF-32 文字列

	for(; len > 0; len--, pi = (ItemChar *)pi->i.next)
		mBufAppend(buf, &pi->code, 4);
}

/* ルビなし文字列追加 */

static void _output_add_normal_text(mBuf *buf,ItemChar *pitop,uint16_t len)
{
	ItemChar *pi;
	int i,is_32bit;
	uint16_t n16;

	//32bit の値が含まれるか

	is_32bit = FALSE;

	for(i = 0, pi = pitop; i < len; i++, pi = (ItemChar *)pi->i.next)
	{
		if(pi->code > 0xffff)
		{
			is_32bit = TRUE;
			break;
		}
	}

	//データタイプ

	mBufAppendByte(buf, (is_32bit)? DATATYPE_NORMAL_TEXT_32: DATATYPE_NORMAL_TEXT_16);

	//長さ

	mBufAppend(buf, &len, 2);

	//文字列

	for(i = 0, pi = pitop; i < len; i++, pi = (ItemChar *)pi->i.next)
	{
		if(is_32bit)
			mBufAppend(buf, &pi->code, 4);
		else
		{
			n16 = pi->code;
			mBufAppend(buf, &n16, 2);
		}
	}
}

/* 挿絵コマンド追加 */

static void _output_add_picture(mBuf *buf,ItemChar *pi)
{
	char *utf8;
	int len;
	uint16_t lenw;

	utf8 = mUTF32toUTF8_alloc(pi->text, pi->textlen, &len);
	if(!utf8) return;

	if(len > 0)
	{
		lenw = len + 1;

		mBufAppendByte(buf, pi->type);
		mBufAppend(buf, &lenw, 2);
		mBufAppend(buf, utf8, lenw);
	}

	mFree(utf8);
}

/* コマンドデータ追加 */

static void _output_add_command(mBuf *buf,ItemChar *pi)
{
	uint8_t b[4];
	int size;

	if(pi->type == DATATYPE_PICTURE)
		//挿絵
		_output_add_picture(buf, pi);
	else
	{
		//他コマンド

		b[0] = pi->type;
		b[1] = pi->cmdno;
		size = 2;

		if(pi->type == DATATYPE_COMMAND_VAL)
		{
			b[2] = pi->cmdval1;
			b[3] = pi->cmdval2;
			size += 2;
		}

		mBufAppend(buf, b, size);
	}
}

/** 1行分の中間データを出力データに変換 */

void convert_output_line(ConvertData *p)
{
	ItemChar *pi,*piend,*pinext;
	mBuf *buf;
	int cnt;

	buf = &p->dstbuf;

	//行番号

	mBufAppendByte(buf, DATATYPE_LINEINFO);
	mBufAppend(buf, &p->curline, 4);

	//

	for(pi = (ItemChar *)p->list.top; pi; pi = pinext)
	{
		pinext = (ItemChar *)pi->i.next;
	
		//------- コマンド

		if(_ITEM_TYPE_IS_CMD(pi))
		{
			_output_add_command(buf, pi);
			continue;
		}

		//------ 通常文字列 or ルビ付き

		//通常文字またはルビ付き文字の連続範囲

		for(piend = pinext, cnt = 1; piend; cnt++, piend = (ItemChar *)piend->i.next)
		{
			if(_ITEM_TYPE_ISNOT_CHAR(piend)
				|| pi->text != piend->text)
				break;
		}

		//追加

		if(pi->text)
		{
			//ルビ付き文字列

			mBufAppendByte(buf, DATATYPE_RUBY_TEXT);
			_output_add_text32(buf, pi, cnt);

			mBufAppend(buf, &pi->textlen, 2);
			mBufAppend(buf, pi->text, pi->textlen * 4);
		}
		else
		{
			//通常文字列
		
			_output_add_normal_text(buf, pi, cnt);
		}

		//

		pinext = piend;
	}

	//中間データ削除

	mListDeleteAll(&p->list);
}

