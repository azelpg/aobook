/*$
aobook
Copyright (c) 2014-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/***********************************
 * レイアウト:メイン処理 (作業用)
 ***********************************/

#include "mlk_gui.h"
#include "mlk_list.h"

#include "def_global.h"
#include "def_textdata.h"
#include "def_style.h"
#include "pv_layout.h"


//==============================
// 本文文字位置セット
//==============================


/** 現在の字下げ/地上げの Y 位置計算
 *
 * [LayoutWork]
 * jisage_y      : 先頭行の字下げ
 * jisage_wrap_y : 折り返し後の字下げ
 * jiage_bottom  : 字上げの下端 */

static void _set_jisage_jiage_pos(LayoutWork *p)
{
	StyleDef *st;
	int top,wrap,bottom,charh,max;

	st = p->stdef;

	//字下げ文字数 (行単位を優先)

	if(p->linestate.jisage)
		top = wrap = p->linestate.jisage;
	else
	{
		top = p->blockstate.jisage;
		wrap = p->blockstate.jisage_wrap;
	}

	//字上げ文字数

	if(p->linestate.jiage || p->blockstate.jiage)
		bottom = ((p->blockstate.jiage)? p->blockstate.jiage: p->linestate.jiage) - 1;
	else
		bottom = 0;

	//文字数調整

	max = st->chars - 1;

	if(top > max) top = max;
	if(wrap > max) wrap = max;
	if(bottom > max) bottom = max;

	if(top + bottom > st->chars) top = 0;
	if(wrap + bottom > st->chars) wrap = 0;

	//セット

	charh = p->fontmain_h + st->char_space;

	if(bottom)
		bottom = bottom * p->fontmain_h + (bottom - 1) * st->char_space;

	p->jisage_y = top * charh;
	p->jisage_wrap_y = wrap * charh;
	p->jiage_bottom = p->pageH - bottom;
}

/** 1行分の本文文字の描画位置をセット
 *
 * [!] 地付き/地上げでは、Y 位置は上端からの位置のままになっているので、描画時に処理する。
 *
 * ptcur: 現在の行の px 位置。次の行の位置が入る。
 * prev_wrapnum: 前ページの前行からの折り返しで、前ページの分の行数。
 * center_diffx: ページ左右中央の場合の X 位置差分 (描画時)
 * return: 現在の行が次ページに渡って折り返す場合、先頭から何行ずらすか */

static int _set_line_char_pos(LayoutWork *p,mPoint *ptcur,int prev_wrapnum,int center_diffx)
{
	StyleDef *st;
	CharItem *pi,*pinext,*pinext2;
	int x,y,charh,nexth,charsp,wrap,bottomY[2];
	int wrapcnt = 0,	//折り返し行数
		next_wrapnum = 0;	//次ページで折り返しをスキップする行数
	uint8_t fnext_hanging = FALSE, //次の文字がぶら下げか
		bottom_type = 0;

	//字下げ/字上げの Y 位置

	_set_jisage_jiage_pos(p);

	//下端位置 ([0] 通常時 [1] 地付き/字上げ時)

	bottomY[0] = p->pageH;
	bottomY[1] = p->jiage_bottom;

	//------------

	x = ptcur->x + p->line_width * prev_wrapnum; //前ページの折り返し分をずらす
	y = p->jisage_y;

	st = p->stdef;
	charsp = st->char_space;

	//

	for(pi = (CharItem *)p->list_char.top; pi; pi = pinext)
	{
		pinext = (CharItem *)pi->i.next;
	
		charh = pi->height;
		nexth = (pinext)? pinext->height: 0;

		//地付き/地上げ開始

		if(!bottom_type && (pi->flags & CHARITEM_F_JIAGE))
			bottom_type = 1;

		//折り返し/ぶら下げ判定
		// wrap: [0] 通常 [1] 現在文字を次行に折り返し [2] 次の文字をぶら下げ
		//       [3] (ぶら下げ処理) そのまま位置のセット後、次行に折り返し

		wrap = 0;

		if(fnext_hanging)
			//前の文字で、次がぶら下げとして指定されている場合
			wrap = 3;
		else if(y + charh > bottomY[bottom_type])
			//現在の文字が下端を超える => 折り返し
			wrap = 1;
		else if(y + charh + charsp + nexth > bottomY[bottom_type])
		{
			//次の文字が下端を超える (現在の文字が行末になる)
		
			if(layout_is_nobottom(p, pi))
				//現在の文字が行末に置いてはいけない文字の場合、現在文字を次行へ
				wrap = 1;
			else if(pinext && layout_find_utf32(pinext->code, &p->u32_nohead))
			{
				//次の文字が行頭禁則の対象

				if((st->flags & STYLE_F_HANGING)
					&& layout_find_utf32(pinext->code, &p->u32_hanging))
					//ぶら下げ対象で、ぶら下げが有効
					wrap = 2;
				else
					//現在文字から折り返し
					wrap = 1;
			}
		}
		else if(pinext && pinext->i.next)
		{
			//次の次の文字が下端を超えて、かつ行頭禁則対象 (ぶら下げ対象外) で、
			//かつ現在文字が行末に来てはならない文字の場合、現在文字を折り返し
			//[例] "……」" で、行頭禁則によって "…」" が次行に折り返すが、"…" は分割禁止のため、
			//     最初の "…" で折り返す時。

			pinext2 = (CharItem *)pinext->i.next;

			if(y + charh + charsp + nexth + charsp + pinext2->height > bottomY[bottom_type]
				&& layout_find_utf32(pinext2->code, &p->u32_nohead)
				&& !((st->flags & STYLE_F_HANGING) && layout_find_utf32(pinext2->code, &p->u32_hanging))
				&& layout_is_nobottom(p, pi))
				wrap = 1;
		}

		if(wrap == 2)
			//次の文字をぶら下げとして予約
			fnext_hanging = TRUE;
		else
		{
			fnext_hanging = FALSE;

			//現在文字から折り返し

			if(wrap == 1)
			{
				x -= p->line_width;
				y = p->jisage_wrap_y;
				wrapcnt++;
			}
		}

		//文字描画位置セット

		pi->x = x - center_diffx;
		pi->y = y;

		//折り返しの先頭文字の場合、フラグを付加

		if(y == p->jisage_wrap_y && pi->i.prev)
			pi->flags |= CHARITEM_F_WRAP_TOP;

		//次の文字のY位置

		y += charh + charsp;

		//現在 X 位置がページ範囲外の場合
		// (折り返しで次ページにまたがる時)
		// :最初に範囲外になった時に、現在の折り返し数を記憶

		if(x < 0 && !next_wrapnum)
			next_wrapnum = wrapcnt;

		//ぶら下げによる折り返し (次の文字がある場合)

		if(wrap == 3 && pinext)
		{
			x -= p->line_width;
			y = p->jisage_wrap_y;
			wrapcnt++;
		}
	}

	//次行の位置

	ptcur->x = x - p->line_width;
	ptcur->y = 0;

	return next_wrapnum;
}


//==============================
// 描画時用
//==============================


/** [描画時] 1行分の地付き/地からｎ字上げ処理
 *
 * Y 位置を実際の位置に調整する。 */

static void _proc_draw_line_jiage(LayoutWork *p)
{
	CharItem *pinext,*pitop,*piend,*pistart,*pi;
	int charspace,texth,toph,upper_h,jisage_h,n;
	mlkbool flag;

	charspace = p->stdef->char_space;

	//字上げ文字数 (0 で地付き)

	n = (p->blockstate.jiage)? p->blockstate.jiage: p->linestate.jiage;
	n--;
	if(n < 0) return;

	//上げる高さ (px)

	if(n == 0)
		upper_h = 0;
	else
		upper_h = n * p->fontmain_h + (n - 1) * charspace;

	//各文字の Y 位置をずらす

	jisage_h = p->jisage_y;
	flag = FALSE;

	for(pitop = (CharItem *)p->list_char.top; pitop; pitop = (CharItem *)piend->i.next)
	{
		//--- 描画上での1行分の地上げの範囲と情報取得
		// pistart,piend: 地上げの範囲
		// toph: 地上げより前のテキストの高さ
		// texth: 地上げテキストの高さ

		pistart = pitop;
		texth = toph = 0;

		for(piend = pitop; 1; piend = pinext)
		{
			pinext = (CharItem *)piend->i.next;

			//地上げ開始

			if(!flag && (piend->flags & CHARITEM_F_JIAGE))
			{
				pistart = piend;
				toph = texth;
				texth = 0;
				flag = TRUE;
			}

			texth += piend->height;

			if(!pinext) break;
			if(pinext->flags & CHARITEM_F_WRAP_TOP) break;

			texth += charspace;
		}

		//地上げありの場合、Y 位置をずらす
		//(上端からの位置でセットされているため、余白分を追加)

		if(flag)
		{
			//加算幅
			
			n = p->pageH - toph - texth - upper_h - jisage_h;
			if(n < 0) n = 0;

			//適用

			for(pi = pistart; 1; pi = (CharItem *)pi->i.next)
			{
				pi->y += n;
			
				if(pi == piend) break;
			}
		}

		//1行目以降は、折り返し字下げ

		jisage_h = p->jisage_wrap_y;
	}
}

/** [描画時] 1行分のルビの描画位置セット */

static void _set_draw_line_ruby_pos(LayoutWork *p)
{
	RubyItem *pi,*pinext,*piprev;
	int x,y,prev_bottom,wrap;

	piprev = NULL;

	for(pi = (RubyItem *)p->list_ruby.top; pi; piprev = pi, pi = pinext)
	{
		pinext = (RubyItem *)pi->i.next;

		//親文字列の情報
		// wrap : [0bit] 親文字列が途中で折り返し [1bit] 親文字列の先頭が折り返しの先頭

		wrap = layout_getrubyinfo(p, pi);

		//親文字列の先頭位置

		x = pi->char_top->x;
		y = pi->char_top->y;

		//前のルビの下端 Y 位置
		//(-1 でルビがない、または行が違う)

		prev_bottom = (piprev && piprev->x == x)? piprev->y + piprev->ruby_h: -1;

		//
		
		if(prev_bottom != -1 && y < prev_bottom)
		{
			//前のルビが同じ行にあり、かつ現在の親文字列の位置に重なる場合は、
			//前回ルビの終端位置から開始
			 
			y = prev_bottom;

			pi->char_h = RUBYITEM_CHARH_NO_PADDING;
		}
		else if(pi->ruby_h >= pi->char_h)
		{
			//ルビ > 親文字列の場合は、親文字列に対して中央揃え (親文字列からはみ出す)
			// :親文字列間に余白を付ける場合も含む。
			// :ただし、親文字列が折り返しあり or 折り返しの先頭 or 前後どちらかにルビが続く場合は、
			// :親文字列の先頭から

			if(!wrap && !layout_is_ruby_connect(piprev, pi) && !layout_is_ruby_connect(pi, pinext))
			{
				//中央揃え位置

				y -= (pi->ruby_h - pi->char_h) / 2;

				//前のルビと重なる場合

				if(prev_bottom != -1 && y < prev_bottom)
					y = prev_bottom;
			}

			pi->char_h = RUBYITEM_CHARH_NO_PADDING;
		}

		//親文字列 > ルビの場合、char_h を維持して、ルビに余白を付ける
		
		//セット

		pi->x = x;
		pi->y = y;
	}
}


//==============================
// sub
//==============================


/** ページ関連のコマンド処理
 *
 * pageno: 現在のページ位置
 * return: [0] 改ページ [1] 次ページを空白&改ページ [2] コマンドを無視 */

static int _proc_command_page(LayoutWork *p,int cmd,mPoint *ptcur,int pageno)
{
	int ret = -1,is_topline;

	//先頭行か

	is_topline = (ptcur->x == p->text_right_x);

	//見開きの場合 (単一ページなら常に改ページ)

	if(p->stdef->pages == 2)
	{
		switch(cmd)
		{
			//改丁
			// :右側のページなら、改ページ。
			// :左側のページなら、次ページを空白にしてその次から開始
			case COMMAND_KAITYO:
				ret = (pageno & 1)? 1: 0;
				break;
			//改見開き
			// :右側のページなら、改ページ。
			// :左側のページなら、次ページを空白にしてその次から。
			// :(ただし、先頭ページで、かつ先頭行の場合は無視する)
			case COMMAND_KAIMIHIRAKI:
				if(pageno & 1)
					ret = 0;
				else
					//先頭ページの先頭行の場合、無視
					ret = (pageno == 0 && is_topline)? 2: 1;
				break;
		}
	}

	//

	if(ret == -1)
		//通常改ページ (ページの先頭行なら無視)
		return (is_topline)? 2: 0;
	else
		return ret;
}


//=============================
// ページレイアウト/描画
//=============================


/** 1ページのレイアウト
 *
 * lf->curpage に現在のページ情報をセットし、
 * lf->nextpage に次のページの情報をセット。
 *
 * return: FALSE でデータ終了 */

mlkbool layout_page(LayoutWork *p,LayoutFirst *lf)
{
	mPoint ptcur;
	BlockState top_blockstate;
	uint8_t *top_text;
	int wrapnum,cmd,ret;

	mMemset0(&p->pagestate, sizeof(PageState));

	p->text = lf->curpage.src;
	p->blockstate = lf->curpage.blockstate;
	p->curlineno = lf->curpage.lineno;

	wrapnum = lf->curpage.wrap_num;

	lf->curpage.diffx = 0;
	lf->nextpage.flags = 0;

	//データの終端なら終了

	if(*(p->text) == DATATYPE_END) return FALSE;

	//空白ページの場合は、次ページの処理へ

	if(lf->curpage.flags & PAGEINFO_F_BLANK) goto NEXT;

	//----- 現在のページを、各行ごとに処理

	ptcur.x = p->text_right_x;
	ptcur.y = 0;

	while(ptcur.x >= 0)
	{
		//次ページに折り返す時用に、行開始時点の情報を保存
	
		top_text = p->text;
		top_blockstate = p->blockstate;
	
		//1行分の文字データと状態を取得
		// cmd:ページ関連のコマンドなら、コマンド番号。なければ -1。
	
		if(!layout_getline(p, &cmd)) break;

		//挿絵

		if(p->pagestate.picture
			&& (p->stdef->flags & STYLE_F_ENABLE_PICTURE))
			break;

		//ページ関連のコマンド処理

		if(cmd != -1)
		{
			ret = _proc_command_page(p, cmd, &ptcur, lf->pagenum);

			if(ret == 1)
				//次ページを空白に
				lf->nextpage.flags |= PAGEINFO_F_BLANK;
			else if(ret == 2)
				//コマンドを無視
				continue;

			break;
		}
		 
		//本文文字の描画位置セット

		wrapnum = _set_line_char_pos(p, &ptcur, wrapnum, 0);

		//リスト解放

		mListDeleteAll(&p->list_char);
		mListDeleteAll(&p->list_ruby);
	}

	//ページの左右中央位置
	//(次ページに折り返しが続く場合は除く)

	if(p->pagestate.fcenter && !wrapnum)
		lf->curpage.diffx = (ptcur.x + p->line_width) / 2;

	//------ 次のページ情報
	
NEXT:
	//次のページの行番号
	
	lf->nextpage.lineno = p->curlineno;

	//現在のページで挿絵があり、先頭行でない場合は、
	//次ページの先頭は挿絵データ

	if((p->stdef->flags & STYLE_F_ENABLE_PICTURE)
		&& p->pagestate.picture
		&& ptcur.x != p->text_right_x)
		p->text = p->pagestate.picture - 1;

	//次ページの情報

	lf->nextpage.wrap_num = wrapnum;

	if(wrapnum)
	{
		//折り返しがある場合、その行の先頭から再処理する
		lf->nextpage.src = top_text;
		lf->nextpage.blockstate = top_blockstate;
	}
	else
	{
		//折り返しがない場合、現在の状態をセット
		lf->nextpage.src = p->text;
		lf->nextpage.blockstate = p->blockstate;
		lf->nextpage.lineno++;
	}

	return TRUE;
}

/** 1ページの描画
 *
 * page: 描画するページ情報
 * pagepos: 単ページの場合 -1、見開きの場合はページ位置 (0:右 1:左) */

void layout_drawpage(LayoutWork *p,PageInfo *page,int pagepos)
{
	mPoint ptcur;
	int wrapnum,cmd,ret;

	//ページ情報描画

	if(GDAT->viewflags & VIEWFLAGS_PAGENO)
		layout_draw_pageinfo(p, page->pageno, pagepos);

	//空白ページの場合、何も描画しない

	if(page->flags & PAGEINFO_F_BLANK) return;

	//

	mMemset0(&p->pagestate, sizeof(PageState));

	p->text = page->src;
	p->blockstate = page->blockstate;

	ptcur.x = p->text_right_x;
	ptcur.y = 0;

	wrapnum = page->wrap_num;

	//

	while(ptcur.x >= 0)
	{
		//1行分取得
	
		if(!layout_getline(p, &cmd)) break;

		//挿絵

		if(p->pagestate.picture
			&& (p->stdef->flags & STYLE_F_ENABLE_PICTURE))
		{
			//ページ先頭でない場合、次ページへ

			if(ptcur.x == p->text_right_x)
				layout_draw_picture(p, pagepos);

			break;
		}

		//ページ単位のコマンド

		if(cmd != -1)
		{
			ret = _proc_command_page(p, cmd, &ptcur, page->pageno);

			if(ret == 2)
				//コマンド無視
				continue;
			else
				break;
		}
		 
		//描画位置セット

		_set_line_char_pos(p, &ptcur, wrapnum, page->diffx);

		_proc_draw_line_jiage(p);
		
		_set_draw_line_ruby_pos(p);

		wrapnum = 0;

		//描画

		layout_draw_line(p, pagepos);
		
		//リスト解放

		mListDeleteAll(&p->list_char);
		mListDeleteAll(&p->list_ruby);
	}
}

