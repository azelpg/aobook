/*$
aobook
Copyright (c) 2014-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * Font : フォント
 * (縦書きやグリフ置換を行わない場合は、mFont を使う)
 *****************************************/

#include <string.h>

#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_GLYPH_H
#include FT_BITMAP_H
#include FT_OUTLINE_H

#define MLK_FONT_FREETYPE_DEFINE

#include "mlk.h"
#include "mlk_font.h"
#include "mlk_fontinfo.h"
#include "mlk_font_freetype.h"
#include "mlk_opentype.h"
#include "mlk_pixbuf.h"

#include "font.h"


//----------------------

static mFontSystem *g_fontsys = NULL;

#define _RUBY_HEIGHTFLAG_SIZE 24	//U+3040-U+30FF の範囲

//フォントのサブデータ

struct _mFTSubData
{
	mOT_VORG vorg;		//縦書き原点位置
	uint8_t *gsub_vert,	//縦書き置き換え
		*gsub_ruby,		//ルビ文字置き換え
		*gsub_hwid,		//プロポーショナル -> 等幅
		*gsub_twid,		//1/3幅
		*gsub_nlck,		//印刷標準字体
		ruby_hflags[_RUBY_HEIGHTFLAG_SIZE];	//ルビ時、Unicode ひらカナ範囲の高さのフラグ (ON でフォント高さと同じ)
	int is_gsub_vrt2;	//TRUE=vrt2, FALSE=vert
};

#define _DRAWF_VRT2_REPLACED  (1<<16)
#define _DRAWF_SRC_BITMAP     (1<<17)

//----------------------



//==============================
// 初期化
//==============================


/** 初期化 */

mlkbool FontInit(void)
{
	g_fontsys = mFontSystem_init();

	return (g_fontsys != 0);
}

/** 終了処理 */

void FontEnd(void)
{
	mFontSystem_finish(g_fontsys);
}

/** フォントシステム取得 */

mFontSystem *FontGetSystem(void)
{
	return g_fontsys;
}


//==============================
// OpenType 各データ読み込み
//==============================


/* VORG 読み込み */

static void _read_VORG(mFont *p)
{
	void *buf;
	uint32_t size;

	if(mFontFT_loadTable(p, MLK_MAKE32_4('V','O','R','G'), &buf, &size) == MLKERR_OK)
	{
		mOpenType_readVORG(buf, size, &p->sub->vorg);

		mFree(buf);
	}
}

/* GSUB/GPOS: Feature 取得 */

static mlkerr _get_feature(mOTLayout *p,uint32_t feature_tag,mOT_TABLE *dst)
{
	uint32_t script_tag,
	 tags[] = {
		MLK_MAKE32_4('h','a','n','i'), MLK_MAKE32_4('k','a','n','a'), MLK_MAKE32_4('h','a','n','g'), 0
	};

	mOTLayout_searchScriptList(p, tags, &script_tag);

	return mOTLayout_getFeature2(p, script_tag, 0, feature_tag, dst);
}

/* vert/vrt2 読み込み */

static void _read_GSUB_vert(mFont *p,mOTLayout *ot)
{
	mOT_TABLE tbl;
	uint8_t *vert = NULL, *vrt2 = NULL;

	//vert

	if(_get_feature(ot, MOPENTYPE_FEATURE_VERT, &tbl) == MLKERR_OK)
		mOTLayout_createGSUB_single(ot, &tbl, &vert);

	//vrt2

	if(_get_feature(ot, MOPENTYPE_FEATURE_VRT2, &tbl) == MLKERR_OK)
	{
		if(mOTLayout_createGSUB_single(ot, &tbl, &vrt2) == MLKERR_OK)
			p->sub->is_gsub_vrt2 = TRUE;
	}

	//結合

	p->sub->gsub_vert = mOpenType_combineData_GSUB_single(vert, vrt2);
}

/* GSUB 読み込み */

static void _read_GSUB(mFont *p,uint32_t flags)
{
	mOTLayout *ot;
	void *buf;
	uint32_t size;
	mOT_TABLE tbl;

	if(mFontFT_loadTable(p, MLK_MAKE32_4('G','S','U','B'), &buf, &size))
		return;

	if(mOTLayout_new(&ot, buf, size, FALSE) == MLKERR_OK)
	{
		_read_GSUB_vert(p, ot);

		//
	
		if(flags & FONT_CREATE_F_RUBY)
		{
			//ruby

			if(_get_feature(ot, MOPENTYPE_FEATURE_RUBY, &tbl) == MLKERR_OK)
				mOTLayout_createGSUB_single(ot, &tbl, &p->sub->gsub_ruby);
		}
		else
		{
			//--- 本文用
			
			//hwid

			if(_get_feature(ot, MLK_MAKE32_4('h','w','i','d'), &tbl) == MLKERR_OK)
				mOTLayout_createGSUB_single(ot, &tbl, &p->sub->gsub_hwid);

			//twid

			if(_get_feature(ot, MLK_MAKE32_4('t','w','i','d'), &tbl) == MLKERR_OK)
				mOTLayout_createGSUB_single(ot, &tbl, &p->sub->gsub_twid);

			//nlck

			if(_get_feature(ot, MLK_MAKE32_4('n','l','c','k'), &tbl) == MLKERR_OK)
				mOTLayout_createGSUB_single(ot, &tbl, &p->sub->gsub_nlck);
		}

		//
	
		mOTLayout_free(ot);
	}

	mFree(buf);
}

/* GPOS 読み込み */

/*
static void _read_GPOS(mFont *p)
{
	mOTLayout *ot;
	void *buf;
	uint32_t size;
	mOT_TABLE tbl;

	if(mFontFT_loadTable(p, MLK_MAKE32_4('G','P','O','S'), &buf, &size))
		return;

	if(mOTLayout_new(&ot, buf, size, TRUE) == MLKERR_OK)
	{
		if(_get_feature(ot, MOPENTYPE_FEATURE_VERT, &tbl) == MLKERR_OK)
			mOTLayout_createGPOS_single(ot, &tbl, &p->sub->gpos_vert);
	
		mOTLayout_free(ot);
	}

	mFree(buf);
}
*/


//==============================
// main
//==============================


/* フォント解放時ハンドラ */

static void _free_handle(mFont *font)
{
	mFTSubData *p = font->sub;

	//サブデータ

	if(p)
	{
		mFree(p->vorg.buf);

		mFree(p->gsub_vert);
		mFree(p->gsub_ruby);
		mFree(p->gsub_hwid);
		mFree(p->gsub_twid);
		mFree(p->gsub_nlck);
		
		mFree(p);
	}
}

/** フォント作成 */

mFont *FontCreate(const char *text,uint32_t flags)
{
	mFont *p;
	mFontInfo info;

	mFontInfoInit(&info);
	mFontInfoSetFromText(&info, text);

	mFontInfoSetDefault_embeddedBitmap(&info, FALSE); //埋め込みビットマップはデフォルトで無効

	//作成
	
	p = mFontCreate(g_fontsys, &info);

	mFontInfoFree(&info);
	
	if(!p) return NULL;

	//サブデータ

	p->sub = (mFTSubData *)mMalloc0(sizeof(mFTSubData));
	if(!p->sub)
	{
		mFontFree(p);
		return NULL;
	}

	p->free_font = _free_handle;

	//

	_read_VORG(p);
	_read_GSUB(p, flags);
	//_read_GPOS(p);
	
	return p;
}


//=================================
// 縦書き用
//=================================


/* [縦書き] 文字コードから GID 取得 */

static uint32_t _code_to_gid_vert(mFont *p,uint32_t code,uint32_t *pflags)
{
	uint32_t code_src,flags = *pflags;

	code = FT_Get_Char_Index(p->face, code);

	//縦書き置換

	code_src = code;

	code = mOpenType_getGSUB_replaceSingle(p->sub->gsub_vert, code);

	//回転ありで、vrt2 によって回転文字に置き換わった時

	if((flags & FONT_DRAW_F_ROTATE) && p->sub->is_gsub_vrt2 && code != code_src)
		*pflags |= _DRAWF_VRT2_REPLACED;

	//印刷標準字体

	if(flags & FONT_DRAW_F_PRINT)
		code = mOpenType_getGSUB_replaceSingle(p->sub->gsub_nlck, code);

	//ルビ文字置換

	if(flags & FONT_DRAW_F_RUBY)
		code = mOpenType_getGSUB_replaceSingle(p->sub->gsub_ruby, code);

	return code;
}

/* [縦書き] GID から、ビットマップグリフをロード
 *
 * 原点は、左上となる。
 *
 * pos: フォントデザイン単位。
 *  x,y : [IN] アウトラインフォント時、座標調整値 (負の値で下方向)。
 *        [OUT] 描画原点からのビットマップ左上位置 (px)。
 *  advance : [IN] advance の調整値 [OUT] 描画原点から次のグリフへの距離 (px) */

static mlkbool _loadglyph_vert_gid(mFont *p,uint32_t gid,uint32_t *pflags,mFTPos *pos)
{
	FT_Face face = p->face;
	uint32_t f,flags;
	int origin,is_rotate;
	FT_Matrix matrix;

	//グリフスロットにロード

	if(FT_Load_Glyph(face, gid, p->gdraw.fload_glyph | FT_LOAD_VERTICAL_LAYOUT))
		return FALSE;

	//

	flags = *pflags;
	f = p->gdraw.flags;
	is_rotate = 0;

	if(face->glyph->format == FT_GLYPH_FORMAT_OUTLINE)
	{
		//------ アウトライン

		//Y 原点
		// VORG テーブルがあればそこから取得。なければ ascender。

		if(!mOpenType_getVORG_originY(&p->sub->vorg, gid, &origin))
			origin = face->ascender;

		//太字化

		if(f & MFTGLYPHDRAW_F_EMBOLDEN)
			FT_Outline_Embolden(&face->glyph->outline, 1<<5);

		//斜体

		if(p->gdraw.flags & MFTGLYPHDRAW_F_SLANT_MATRIX)
			FT_Outline_Transform(&face->glyph->outline, &p->gdraw.slant_matrix);

		//

		if((flags & FONT_DRAW_F_ROTATE) && !(flags & _DRAWF_VRT2_REPLACED))
		{
			//90度回転
			// GSUB:vrt2 ですでに回転している場合は除く。
			// 回転後は左上が原点になるため、移動は行わない。

			matrix.xx = matrix.yy = 0;
			matrix.xy = 1<<16, matrix.yx = ((FT_ULong)-1) << 16;

			FT_Outline_Transform(&face->glyph->outline, &matrix);

			is_rotate = TRUE;
		}
		else
		{
			//原点をグリフ左上へ移動
			
			FT_Outline_Translate(&face->glyph->outline,
				FT_MulFix(pos->x, face->size->metrics.x_scale),
				FT_MulFix(pos->y - origin, face->size->metrics.y_scale));
		}

		//ビットマップに変換

		if(FT_Render_Glyph(face->glyph, p->gdraw.render_mode))
			return FALSE;

		//

		pos->x = face->glyph->bitmap_left;
		pos->y = -(face->glyph->bitmap_top);
	}
	else if(face->glyph->format == FT_GLYPH_FORMAT_BITMAP)
	{
		//------ ビットマップ

		//太字化

		if(f & MFTGLYPHDRAW_F_EMBOLDEN)
		{
			FT_GlyphSlot_Own_Bitmap(face->glyph);
			FT_Bitmap_Embolden(p->sys->lib, &face->glyph->bitmap, 1<<6, 0);
		}

		//[!] グリフ中央・上が原点になっている

		if(flags & FONT_DRAW_F_ROTATE)
		{
			//90度回転

			pos->x = face->glyph->bitmap_top;
			pos->y = face->glyph->bitmap_left + p->mt.pixel_per_em / 2;

			is_rotate = TRUE;
		}
		else
		{
			pos->x = face->glyph->bitmap_left + p->mt.pixel_per_em / 2;
			pos->y = face->glyph->bitmap_top;
		}

		*pflags |= _DRAWF_SRC_BITMAP;
	}

	//送り幅

	if(is_rotate)
	{
		//90度回転

		pos->advance = mFontFT_round_fix6(face->glyph->metrics.horiAdvance);
	}
	else
	{
		pos->advance = mFontFT_round_fix6(face->glyph->advance.y
			+ FT_MulFix(pos->advance, face->size->metrics.y_scale));
	}
	
	return TRUE;
}

/* グリフ描画 (1bit, 90度回転) */

static void _draw_glyph_1bit_rotate(mFont *p,int x,int y,mFontDrawInfo *drawinfo,void *param)
{
	mFuncFontSetPixelMono setpix;
	FT_Bitmap *bm;
	uint8_t *pd,*pdY,f;
	int ix,iy,w,h,pitch;
	
	setpix = drawinfo->setpix_mono;

	bm = &p->face->glyph->bitmap;
	
	w = bm->width;
	h = bm->rows;
	pdY = bm->buffer;
	pitch = bm->pitch;
	
	if(pitch < 0) pdY += -pitch * (h - 1);
		
	for(ix = h - 1; ix >= 0; ix--, pdY += pitch)
	{
		pd = pdY;
		f = 0x80;
		
		for(iy = 0; iy < w; iy++)
		{
			if(*pd & f)
				(setpix)(x + ix, y + iy, param);
			
			f >>= 1;
			if(!f) { f = 0x80; pd++; }
		}
	}
}

/* [縦書き] グリフロード (文字コード) */

static mlkbool _loadglyph_vert_code(mFont *p,uint32_t code,uint32_t *pflags,mFTPos *dst)
{
	//mOT_POS otpos;

	code = _code_to_gid_vert(p, code, pflags);

/*
	//GPOS "vert" の調整位置 (フォントデザイン単位)
	//[!] GPOS:vert が存在するフォントがほぼない

	if(mOpenType_getGPOS_single(p->sub->gpos_vert, code, &otpos))
	{
		pos.x = otpos.x;
		pos.y = otpos.y;
		pos.advance = otpos.advy;
	}
	else
*/
	dst->x = dst->y = dst->advance = 0; 

	//グリフ読み込み

	return _loadglyph_vert_gid(p, code, pflags, dst);
}

/* [縦書き] グリフ描画 (文字コード)
 *
 * return: 次の y 位置 */

static int _drawglyph_vert_code(mFont *p,int x,int y,uint32_t code,uint32_t flags,
	mFontDrawInfo *info,void *param)
{
	mFTPos pos;

	if(_loadglyph_vert_code(p, code, &flags, &pos))
	{
		if((flags & FONT_DRAW_F_ROTATE) && (flags & _DRAWF_SRC_BITMAP))
			//回転 & ソースのグリフがビットマップの場合
			_draw_glyph_1bit_rotate(p, x + pos.x, y + pos.y, info, param);
		else
		{
			//濁点調整
			if(flags & FONT_DRAW_F_DAKUTEN_VERT)
				pos.y += p->face->glyph->bitmap.rows;
			
			mFontFT_drawGlyph(p, x + pos.x, y + pos.y, info, param);
		}

		y += pos.advance;
	}

	return y;
}


//=================================
// 横書き用
//=================================


/* [横書き] 文字コードから GID に変換 */

static uint32_t _code_to_gid_horz(mFont *p,uint32_t code,uint32_t flags)
{
	uint32_t src;

	code = FT_Get_Char_Index(p->face, code);

	//---- GSUB 置換

	src = code;

	//1/3幅 (なかった場合、半角幅を検索)

	if(flags & FONT_DRAW_F_WIDTH_1_3)
	{
		code = mOpenType_getGSUB_replaceSingle(p->sub->gsub_twid, code);

		if(code != src) return code;
	}

	//プロポーショナル -> 半角

	if(flags & FONT_DRAW_F_WIDTH_1_2)
		code = mOpenType_getGSUB_replaceSingle(p->sub->gsub_hwid, code);

	return code;
}

/* [横書き] グリフ描画 (文字コード)
 *
 * return: 次の x 位置 */

static int _drawglyph_horz_code(mFont *p,int x,int y,uint32_t code,uint32_t flags,
	mFontDrawInfo *info,void *param)
{
	mFTPos pos;

	code = _code_to_gid_horz(p, code, flags);

	//縦中横のため、縦書きのレイアウトで取得
	//(通常の横書きで問題ないフォントもあるが、源ノフォントではY位置がずれる)

	pos.x = pos.y = pos.advance = 0;

	if(_loadglyph_vert_gid(p, code, &flags, &pos))
	{
		mFontFT_drawGlyph(p, x + pos.x, y + pos.y, info, param);

		x += mFontFT_round_fix6(p->face->glyph->metrics.horiAdvance);
	}

	return x;
}


//=================================
//
//=================================


/** 横書き用、テキストの幅取得 (縦中横用、ASCII 文字)
 *
 * 文字数によって、自動で文字幅置き換えあり */

int FontGetTextWidth_horz(mFont *p,const char *text,int len,uint32_t flags)
{
	mFTPos pos;
	uint32_t code;
	int w = 0;

	if(len == 2)
		flags |= FONT_DRAW_F_WIDTH_1_2;
	else if(len == 3)
		flags |= FONT_DRAW_F_WIDTH_1_3;

	//

	mFontFT_setLCDFilter(p);

	for(; len > 0; text++, len--)
	{
		pos.x = pos.y = pos.advance = 0;

		code = _code_to_gid_horz(p, *text, flags);

		if(mFontFT_loadGlyphH_gid(p, code, &pos))
			w += pos.advance;
	}

	return w;
}

/** 横書き描画 (ASCII文字列。縦中横用)
 *
 * 文字数によって、自動で文字幅置き換えあり */

void FontDrawText_horz(mFont *p,mPixbuf *img,int x,int y,const char *text,int len,mRgbCol col,uint32_t flags)
{
	mFontDrawInfo *info;
	mFontDrawParam_pixbuf param;

	if(!p || !img) return;

	//文字幅置き換え
	
	if(len == 2)
		flags |= FONT_DRAW_F_WIDTH_1_2;
	else if(len == 3)
		flags |= FONT_DRAW_F_WIDTH_1_3 | FONT_DRAW_F_WIDTH_1_2;

	//mPixbuf 描画用情報

	info = mFontGetDrawInfo_pixbuf();

	mFontSetDrawParam_pixbuf(&param, img, col);

	//

	mFontFT_setLCDFilter(p);

	for(; len > 0; text++, len--)
		x = _drawglyph_horz_code(p, x, y, *text, flags, info, &param);
}

/** 縦書き用、テキストの幅(高さ)取得 */

int FontGetTextWidth_vert(mFont *p,const uint32_t *text,int len,uint32_t flags)
{
	mFTPos pos;
	uint8_t *hbuf;
	uint32_t f,c;
	int w = 0,hf,ppem;

	mFontFT_setLCDFilter(p);

	if(flags & FONT_DRAW_F_RUBY)
	{
		//ルビ時

		ppem = p->mt.pixel_per_em;

		for(; len > 0; text++, len--)
		{
			c = *text;

			//ひらカナの範囲

			if(c >= 0x3040 && c < 0x3100)
			{
				hbuf = p->sub->ruby_hflags + ((c - 0x3040) >> 3);
				hf = 1 << ((c - 0x3040) & 7);

				if(*hbuf & hf)
				{
					w += ppem;
					continue;
				}
			}
			else
				hbuf = NULL;

			//計算
			
			f = flags;
			
			if(_loadglyph_vert_code(p, *text, &f, &pos))
				w += pos.advance;

			//フラグ

			if(hbuf && pos.advance == ppem)
				*hbuf |= hf;
		}
	}
	else
	{
		//通常
	
		for(; len > 0; text++, len--)
		{
			f = flags;
			
			if(_loadglyph_vert_code(p, *text, &f, &pos))
				w += pos.advance;
		}
	}

	return w;
}

/** 縦書きテキスト描画
 *
 * x,y は文字の左上位置
 *
 * return: テキスト幅(高さ) */

int FontDrawText_vert(mFont *p,mPixbuf *img,int x,int y,
	const uint32_t *text,int len,mRgbCol col,uint32_t flags)
{
	mFontDrawInfo *info;
	mFontDrawParam_pixbuf param;
	int topy;

	if(!p || !img) return 0;
	
	//mPixbuf 描画用情報

	info = mFontGetDrawInfo_pixbuf();

	mFontSetDrawParam_pixbuf(&param, img, col);

	//

	topy = y;

	mFontFT_setLCDFilter(p);

	for(; len > 0; text++, len--)
		y = _drawglyph_vert_code(p, x, y, *text, flags, info, &param);

	return y - topy;
}

