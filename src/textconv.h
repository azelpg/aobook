/*$
aobook
Copyright (c) 2014-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/********************************
 * 内部データ変換用定義
 ********************************/

//[*] の値は型を変更しないこと


/** 変換作業用データ */

typedef struct
{
	void *txtbuf;		//変換元テキストの先頭バッファ (解放時用)
	uint8_t *pcur;		//現在の変換元テキスト位置
	uint32_t remain,	//変換元の残りバイト数
		curline;		//現在のテキスト行位置 (0〜) [*]

	mBuf dstbuf,  	//変換後データの出力バッファ
		linebuf;  	//1行分の UTF-32 文字列データ
	mList list;		//1行分の中間データ (ItemChar)
}ConvertData;

/** 中間データのアイテム (1文字/1コマンド分) */

typedef struct
{
	mListItem i;

	uint8_t type,		//データタイプ
		cmdno,cmdval1,cmdval2;	//コマンドのタイプと値
	uint32_t code;		//CHAR 時、UTF-32 文字
	uint16_t textlen;	//text の長さ [*]
	uint32_t *text;
		//[CHAR] ルビ文字列の先頭。親文字列すべてにセットされる。
		//[PICTURE] 挿絵ファイル名
}ItemChar;

/** 注記マッチ用の定義データ */

typedef struct
{
	const uint16_t *str;
	uint8_t cmdtype,	//コマンドタイプ
		flags,
		val1;
}CmdMatchDef;

/** 注記マッチの結果データ */

typedef struct
{
	uint8_t type,	//コマンドタイプ
		val1,		//値1 (255 で値なし)
		val2,
		flags;		//CmdMatchDef::flags の値が入る
	int textlen;		//text の文字数
	uint32_t *text;		//注記内の必要文字列の先頭位置
		//[PICTURE] ファイル名の位置
		//"「*」*" の場合、「」内の文字列
	ItemChar *insert;	//"「*」*" で効果をかける文字列の先頭位置
}CmdMatchData;

//----------

//CmdMatchDef::flags
enum
{
	CMDMATCHDEF_F_NO_ENTER = 1<<0,	//この注記が行末の場合、文章としての改行を追加しない
	CMDMATCHDEF_F_HAVE_VAL1 = 1<<1	//val1 データあり
};

#define COMMAND_VALUE_NONE  255	//コマンドの値なし時

//----------

mlkbool convert_getLine(ConvertData *p);

mlkbool convert_text_kagiClose(uint32_t **pptext);
uint32_t convert_text_to_hex(uint32_t *text);
uint32_t convert_text_to_jis(uint32_t *text);
mlkbool convert_is_kanji(uint32_t c);
mlkbool convert_compareText16(uint32_t *text,const uint16_t *cmp);

mlkbool convert_matchCmd_array(uint32_t *ptext,const CmdMatchDef *array,CmdMatchData *dst);
mlkbool convert_matchCmd_kagi(uint32_t *ptext,const CmdMatchDef *array,CmdMatchData *dst);
mlkbool convert_matchCmd_picture(uint32_t *ptext,CmdMatchData *dst);

void convert_setRubyInfo(ConvertData *p,ItemChar *pitop,uint32_t *rubytop,uint16_t rubylen);
void convert_setRubyInfo_auto(ConvertData *p,uint32_t *rubytop,uint16_t rubylen);
mlkbool convert_compareCmdTextLast(ConvertData *p,CmdMatchData *dat);

void convert_addItem_normal(ConvertData *p,uint32_t c);
void convert_addItem_command(ConvertData *p,CmdMatchData *dat);

void convert_output_line(ConvertData *p);
