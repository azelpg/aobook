/*$
aobook
Copyright (c) 2014-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * スタイル設定ダイアログ
 *****************************************/

#include <stdio.h>

#include "mlk_gui.h"
#include "mlk_widget_def.h"
#include "mlk_widget.h"
#include "mlk_window.h"
#include "mlk_sysdlg.h"
#include "mlk_event.h"
#include "mlk_columnitem.h"
#include "mlk_menu.h"

#include "mlk_listview.h"
#include "mlk_iconbar.h"
#include "mlk_tab.h"
#include "mlk_pager.h"

#include "mlk_str.h"
#include "mlk_list.h"
#include "mlk_imagelist.h"
#include "mlk_iniread.h"

#include "def_global.h"
#include "def_style.h"
#include "def_widget.h"
#include "style.h"
#include "trid.h"
#include "func_widget.h"


//--------------------

//スタイルリストアイテム

typedef struct
{
	mColumnItem i;
	StyleDef def;
}StyleItem;

//ダイアログ

typedef struct
{
	MLK_DIALOG_DEF

	mListView *lvstyle;
	mTab *tab;
	mPager *pager;

	StyleItem *picur;
	int ret_flags;
}_dialog;

//--------------------

enum
{
	TRID_TITLE,
	TRID_MES_SAME_STYLENAME,

	TRID_BTT_ADD = 20,
	TRID_BTT_DEL,
	TRID_BTT_COPY,
	TRID_BTT_RENAME,
	TRID_BTT_DOWN,
	TRID_BTT_UP,

	TRID_TAB_BASIC = 100,
	TRID_TAB_SCREEN,
	TRID_TAB_FONT,
	TRID_TAB_CHAR,

	TRID_ADDSTYLE = 200,
	TRID_STYLENAME,
};

enum
{
	WID_LIST = 10000,
	WID_LISTBTT,
	WID_TAB
};

//--------------------

static const uint8_t g_img_icon[169] = {
0x89,0x50,0x4e,0x47,0x0d,0x0a,0x1a,0x0a,0x00,0x00,0x00,0x0d,0x49,0x48,0x44,0x52,
0x00,0x00,0x00,0x60,0x00,0x00,0x00,0x10,0x01,0x00,0x00,0x00,0x00,0xc1,0xc1,0x47,
0x54,0x00,0x00,0x00,0x70,0x49,0x44,0x41,0x54,0x78,0x5e,0x4d,0xce,0xbd,0x0d,0x80,
0x20,0x10,0x86,0x61,0x2e,0x16,0x94,0xd4,0x56,0x8c,0x72,0xa3,0x71,0x8e,0x61,0xc5,
0x28,0xd8,0xb0,0x87,0x0b,0x98,0x50,0x42,0x72,0x78,0xfe,0xa1,0xf2,0x76,0x4f,0xf3,
0xe5,0x53,0xd2,0xa5,0x18,0x45,0xc8,0x8a,0x30,0xee,0xae,0x81,0x80,0x91,0xf1,0x86,
0xf7,0xa1,0x32,0x66,0xfb,0x60,0x0a,0x91,0x31,0x99,0x17,0x1b,0xe3,0xaa,0x15,0x01,
0x81,0x9f,0x2f,0x2c,0x43,0x07,0x82,0x0f,0xd7,0xde,0x87,0x65,0x68,0xd3,0x34,0x86,
0x6d,0xd5,0x3f,0x62,0x32,0x0d,0xa5,0x84,0x9a,0x6d,0x43,0x86,0xee,0x4e,0x06,0x91,
0xdd,0x9d,0x90,0xae,0x03,0xb6,0x08,0x7f,0x8f,0x8b,0xee,0xf9,0x7a,0x00,0x00,0x00,
0x00,0x49,0x45,0x4e,0x44,0xae,0x42,0x60,0x82 };

//--------------------

mlkbool StyleDlg_createPage_basic(mPager *p,mPagerInfo *info);
mlkbool StyleDlg_createPage_screen(mPager *p,mPagerInfo *info);
mlkbool StyleDlg_createPage_font(mPager *p,mPagerInfo *info);
mlkbool StyleDlg_createPage_char(mPager *p,mPagerInfo *info);

//--------------------


//=====================
// sub
//=====================


/* アイテムの破棄ハンドラ */

static void _item_destroy(mList *list,mListItem *item)
{
	StyleDef_free(&((StyleItem *)item)->def);

	mColumnItemHandle_destroyItem(list, item);
}

/* スタイルを読み込み & 初期選択アイテムセット*/

static void _read_style(_dialog *p)
{
	mIniRead *ini;
	StyleItem *pi,*pisel = NULL;
	mStr *pstrname;
	const char *name;
	int i,num;

	pstrname = &GDAT->style->b.str_stylename;

	//設定ファイルから読み込み & リストアイテム追加

	num = StyleConf_openRead(&ini);

	for(i = 0; i < num; i++)
	{
		if(!mIniRead_setGroupNo(ini, i)) break;

		name = mIniRead_getText(ini, "name", NULL);
		if(!name) break;

		pi = (StyleItem *)mListViewAddItem_size(p->lvstyle, sizeof(StyleItem),
			name, 0, MCOLUMNITEM_F_COPYTEXT, 0);

		if(pi)
		{
			StyleConf_readDefine(ini, &pi->def);

			//現在のスタイル名と同じなら選択

			if(!pisel && mStrCompareEq(pstrname, name))
				pisel = pi;
		}
	}

	mIniRead_end(ini);

	//スタイルが一つもなければ、デフォルトを追加

	if(mListViewGetItemNum(p->lvstyle) == 0)
	{
		pisel = pi = (StyleItem *)mListViewAddItem_size(p->lvstyle, sizeof(StyleItem),
			"default", 0, 0, 0);

		if(pi)
			StyleDef_setDefault(&pi->def, "default");
	}

	//選択がなければ、先頭

	if(!pisel)
		pisel = (StyleItem *)mListViewGetTopItem(p->lvstyle);

	//選択

	p->picur = pisel;

	mListViewSetFocusItem_scroll(p->lvstyle, MLK_COLUMNITEM(pisel));
}

/* ページ作成 */

static void _create_page(_dialog *p,int no)
{
	mFuncPagerCreate func[] = {
		StyleDlg_createPage_basic, StyleDlg_createPage_screen,
		StyleDlg_createPage_font, StyleDlg_createPage_char
	};

	mPagerSetPage(p->pager, func[no]);

	//ページの推奨サイズが大きい場合は、ウィンドウのサイズ変更。
	//現在のサイズに収まれば、mPager のみ再レイアウト。

	if(!mWidgetResize_calchint_larger(MLK_WIDGET(p->wg.toplevel)))
		mWidgetLayout_redraw(MLK_WIDGET(p->pager));
}

/* スタイルの選択を変更
 *
 * is_getdata: TRUE で、変更前に現在のスタイルのデータを取得 (スタイル削除/複製時は FALSE) */

static void _change_style(_dialog *p,StyleItem *pi,mlkbool is_getdata)
{
	//現在のページデータを取得

	if(is_getdata)
		mPagerGetPageData(p->pager);

	//変更

	p->picur = pi;

	mPagerSetDataPtr(p->pager, (pi)? &pi->def: NULL);
	mPagerSetPageData(p->pager);
}


//=====================
// command
//=====================


/* スタイル追加 */

static void _cmd_style_add(_dialog *p)
{
	mStr str = MSTR_INIT;
	StyleItem *pi;

	if(mListViewGetItemNum(p->lvstyle) >= STYLE_MAXNUM)
		return;

	//名前入力

	if(!mSysDlg_inputText(MLK_WINDOW(p),
		MLK_TR(TRID_ADDSTYLE), MLK_TR(TRID_STYLENAME),
		MSYSDLG_INPUTTEXT_F_NOT_EMPTY, &str))
		return;

	//同名が存在するか

	if(mListViewGetItem_fromText(p->lvstyle, str.buf))
	{
		mMessageBoxErr(MLK_WINDOW(p), MLK_TR(TRID_MES_SAME_STYLENAME));
		mStrFree(&str);
		return;
	}

	//追加

	pi = (StyleItem *)mListViewAddItem_size(p->lvstyle, sizeof(StyleItem),
		str.buf, 0, MCOLUMNITEM_F_COPYTEXT, 0);

	if(pi)
	{
		StyleDef_setDefault(&pi->def, str.buf);
	
		mListViewSetFocusItem_scroll(p->lvstyle, MLK_COLUMNITEM(pi));
	
		_change_style(p, pi, TRUE);

		p->ret_flags |= STYLEDLG_F_UPDATE_LIST;
	}

	mStrFree(&str);
}

/* スタイル削除 */

static void _cmd_style_delete(_dialog *p)
{
	mColumnItem *item;

	//必ず一つは残す

	if(!p->picur
		|| mListViewGetItemNum(p->lvstyle) == 1)
		return;

	//リスト削除

	item = mListViewDeleteItem_focus(p->lvstyle);

	//

	_change_style(p, (StyleItem *)item, FALSE);

	p->ret_flags |= STYLEDLG_F_UPDATE_LIST;
}

/* スタイル複製 */

static void _cmd_style_copy(_dialog *p)
{
	StyleItem *pisrc,*pi;
	mStr str = MSTR_INIT;

	if(mListViewGetItemNum(p->lvstyle) >= STYLE_MAXNUM)
		return;

	//複製元

	pisrc = p->picur;
	if(!pisrc) return;

	//スタイル名

	mStrSetFormat(&str, "%t-copy", &pisrc->def.str_stylename);

	//追加

	pi = (StyleItem *)mListViewAddItem_size(p->lvstyle, sizeof(StyleItem),
		str.buf, 0, MCOLUMNITEM_F_COPYTEXT, 0);

	if(pi)
	{
		//複製元の現在値取得

		mPagerGetPageData(p->pager);
	
		//コピー
		
		StyleDef_copy(&pi->def, &pisrc->def);

		mStrCopy(&pi->def.str_stylename, &str);

		//

		mListViewSetFocusItem_scroll(p->lvstyle, MLK_COLUMNITEM(pi));

		_change_style(p, pi, FALSE);

		p->ret_flags |= STYLEDLG_F_UPDATE_LIST;
	}

	mStrFree(&str);
}

/* 名前の変更 */

static void _cmd_style_rename(_dialog *p)
{
	StyleItem *pi;
	mStr str = MSTR_INIT;

	pi = p->picur;
	if(!pi) return;

	//名前入力

	mStrCopy(&str, &pi->def.str_stylename);

	if(!mSysDlg_inputText(MLK_WINDOW(p),
		MLK_TR(TRID_BTT_RENAME), MLK_TR(TRID_STYLENAME),
		MSYSDLG_INPUTTEXT_F_NOT_EMPTY | MSYSDLG_INPUTTEXT_F_SET_DEFAULT,
		&str))
	{
		mStrFree(&str);
		return;
	}

	//同じ名前ならそのまま

	if(mStrCompareEq(&pi->def.str_stylename, str.buf))
	{
		mStrFree(&str);
		return;
	}

	//同名確認

	if(mListViewGetItem_fromText(p->lvstyle, str.buf))
	{
		mMessageBoxErr(MLK_WINDOW(p), MLK_TR(TRID_MES_SAME_STYLENAME));
		mStrFree(&str);
		return;
	}

	//セット

	mStrCopy(&pi->def.str_stylename, &str);

	mListViewSetItemText_copy(p->lvstyle, MLK_COLUMNITEM(pi), str.buf);

	p->ret_flags |= STYLEDLG_F_UPDATE_LIST;

	mStrFree(&str);
}


//=====================
//
//=====================


/* 通知イベント */

static int _event_notify(_dialog *p,mEventNotify *ev)
{
	switch(ev->id)
	{
		//スタイルリスト
		case WID_LIST:
			if(ev->notify_type == MLISTVIEW_N_CHANGE_FOCUS)
				_change_style(p, (StyleItem *)ev->param1, TRUE);
			return TRUE;

		//タブ
		case WID_TAB:
			if(ev->notify_type == MTAB_N_CHANGE_SEL)
				_create_page(p, mTabGetItemParam_atIndex(p->tab, -1));
			return TRUE;
	}

	return FALSE;
}

/* イベントハンドラ */

static int _event_handle(mWidget *wg,mEvent *ev)
{
	_dialog *p = (_dialog *)wg;

	switch(ev->type)
	{
		case MEVENT_NOTIFY:
			if(_event_notify(p, (mEventNotify *)ev))
				return TRUE;
			break;

		case MEVENT_COMMAND:
			switch(ev->cmd.id)
			{
				//追加
				case TRID_BTT_ADD:
					_cmd_style_add(p);
					break;
				//削除
				case TRID_BTT_DEL:
					_cmd_style_delete(p);
					break;
				//複製
				case TRID_BTT_COPY:
					_cmd_style_copy(p);
					break;
				//名前変更
				case TRID_BTT_RENAME:
					_cmd_style_rename(p);
					break;
				//下へ
				case TRID_BTT_DOWN:
					mListViewMoveItem_updown(p->lvstyle, NULL, TRUE);
					p->ret_flags |= STYLEDLG_F_UPDATE_LIST;
					break;
				//上へ
				case TRID_BTT_UP:
					mListViewMoveItem_updown(p->lvstyle, NULL, FALSE);
					p->ret_flags |= STYLEDLG_F_UPDATE_LIST;
					break;
			}
			return TRUE;
	}

	return mDialogEventDefault_okcancel(wg, ev);
}

/* ウィジェット作成 */

static void _create_widget(_dialog *p)
{
	mWidget *cttop,*ct;
	mListView *lv;
	mIconBar *ib;
	mTab *tab;
	mImageList *imglist;
	int i,fonth;

	fonth = mWidgetGetFontHeight(MLK_WIDGET(p));

	cttop = mContainerCreateHorz(MLK_WIDGET(p), 10, MLF_EXPAND_WH, 0);

	//---- 左

	ct = mContainerCreateVert(cttop, 5, MLF_EXPAND_H, 0);

	//リスト

	p->lvstyle = lv = mListViewCreate(ct, WID_LIST, MLF_EXPAND_H, 0,
		MLISTVIEW_S_AUTO_WIDTH, MSCROLLVIEW_S_HORZVERT_FRAME);

	lv->wg.hintW = fonth * 11;
	lv->wg.initH = fonth * 17;

	mListViewSetItemDestroyHandle(lv, _item_destroy);

	//ボタン用イメージ

	imglist = mImageListLoadPNG_buf(g_img_icon, sizeof(g_img_icon), 16);

	mImageListReplaceTextColor_mono(imglist);

	//ボタン

	ib = mIconBarCreate(ct, WID_LISTBTT, 0, 0,
		MICONBAR_S_TOOLTIP | MICONBAR_S_BUTTON_FRAME | MICONBAR_S_DESTROY_IMAGELIST);

	mIconBarSetTooltipTrGroup(ib, TRGROUP_ID_DLG_STYLE);
	mIconBarSetImageList(ib, imglist);

	for(i = 0; i < 6; i++)
		mIconBarAdd(ib, TRID_BTT_ADD + i, i, TRID_BTT_ADD + i, 0);

	//----- 右

	ct = mContainerCreateVert(cttop, 15, MLF_EXPAND_WH, 0);

	//タブ

	p->tab = tab = mTabCreate(ct, WID_TAB, MLF_EXPAND_W, 0, MTAB_S_TOP | MTAB_S_HAVE_SEP);

	for(i = 0; i < 4; i++)
		mTabAddItem(tab, MLK_TR(TRID_TAB_BASIC + i), -1, 0, i);

	mTabSetSel_atIndex(tab, 0);

	//mPager

	p->pager = mPagerCreate(ct, MLF_EXPAND_WH, 0);
}

/* ダイアログ作成 */

static _dialog *_create_dialog(mWindow *parent)
{
	_dialog *p;

	p = (_dialog *)mDialogNew(parent, sizeof(_dialog), MTOPLEVEL_S_DIALOG_NORMAL);
	if(!p) return NULL;

	p->wg.event = _event_handle;

	//

	MLK_TRGROUP(TRGROUP_ID_DLG_STYLE);

	mToplevelSetTitle(MLK_TOPLEVEL(p), MLK_TR(TRID_TITLE));

	mContainerSetPadding_same(MLK_CONTAINER(p), 8);

	//ウィジェット

	_create_widget(p);

	mContainerCreateButtons_okcancel(MLK_WIDGET(p), MLK_MAKE32_4(0,10,0,0));

	//スタイル読み込み

	_read_style(p);

	//初期ページ

	mPagerSetPage(p->pager, StyleDlg_createPage_basic);

	//初期選択

	_change_style(p, p->picur, FALSE);

	return p;
}


//===========================


/* スタイルメニューを再セット */

static void _update_style_menu(_dialog *p)
{
	mMenu *menu;
	mStr *pstrname;
	StyleItem *pi;
	int i,flags;

	menu = GDAT->mainwin->menu_style;

	pstrname = &GDAT->style->b.str_stylename;

	mMenuDeleteAll(menu);

	//リストから項目追加
	//(現在のスタイル名と一致するものがなければ、選択無しでOK)

	pi = (StyleItem *)mListViewGetTopItem(p->lvstyle);

	for(i = 0; pi; pi = (StyleItem *)(MLISTITEM(pi)->next), i++)
	{
		flags = MMENUITEM_F_COPYTEXT | MMENUITEM_F_RADIO_TYPE;

		if(mStrCompareEq(pstrname, pi->def.str_stylename.buf))
			flags |= MMENUITEM_F_CHECKED;
	
		mMenuAppend(menu, MAINWIN_CMDID_STYLE + i, pi->def.str_stylename.buf, 0, flags);
	}
}

/* 設定ファイルを書き込み */

static void _write_config(_dialog *p)
{
	void *fp;
	StyleItem *pi;
	int i;

	fp = StyleConf_openWrite(mListViewGetItemNum(p->lvstyle));
	if(!fp) return;

	pi = (StyleItem *)mListViewGetTopItem(p->lvstyle);

	for(i = 0; pi; pi = (StyleItem *)(MLISTITEM(pi)->next), i++)
		StyleConf_writeDefine(fp, i, &pi->def);

	fclose((FILE *)fp);
}

/** スタイル設定ダイアログ実行
 *
 * return: フラグ */

int StyleOptDialog(mWindow *parent)
{
	_dialog *p;
	int ret,n;

	p = _create_dialog(parent);
	if(!p) return 0;

	mWindowResizeShow_initSize(MLK_WINDOW(p));

	ret = mDialogRun(MLK_DIALOG(p), FALSE);
	
	//適用

	if(ret)
	{
		ret = p->ret_flags;

		//現在の値取得

		mPagerGetPageData(p->pager);

		//ファイルに保存

		_write_config(p);

		//現在のスタイルに値を適用
		//(選択アイテムを現在のスタイルにする)

		if(p->picur)
		{
			n = StyleWork_apply(GDAT->style, &p->picur->def);

			if(n == 1)
				ret |= STYLEDLG_F_UPDATE;
			else if(n == 2)
				ret |= STYLEDLG_F_LAYOUT | STYLEDLG_F_UPDATE;
		}

		//スタイルメニューを再セット

		_update_style_menu(p);
	}

	//削除

	mWidgetDestroy(MLK_WIDGET(p));

	return ret;
}
