/*$
aobook
Copyright (c) 2014-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/**********************************
 * テキスト内部データに関する定義
 **********************************/


//データタイプ (先頭の1byte値)

enum
{
	DATATYPE_END,			//終端
	DATATYPE_ENTER,			//文章としての改行
	DATATYPE_LINEINFO,		//ソースの行番号値 [(uint32)行番号]
	DATATYPE_NORMAL_TEXT_16,	//通常文字列(16bit) [(uint16)文字数, 16bit文字列:ヌル含まない]
	DATATYPE_NORMAL_TEXT_32,	//通常文字列(32bit) [(uint16)文字数, 32bit文字列]
	DATATYPE_RUBY_TEXT,		//ルビ付き文字列 [(uint16)親文字列の文字数, UTF-32親文字列, (uint16)ルビ文字数, UTF-32ルビ文字列]
	DATATYPE_COMMAND,		//注記コマンド [(uint8)コマンドタイプ]
	DATATYPE_COMMAND_VAL,	//注記コマンド [(uint8)コマンドタイプ, (uint8x2)値]
	DATATYPE_PICTURE,		//挿絵 [(uint16)文字列バイト数:ヌル含む, UTF-8ファイル名文字列:ヌル含む]

	DATATYPE_CHAR = 255		//変換作業時用
};

//注記のコマンド
// START+1 = END にすること

enum
{
	COMMAND_NONE,
	COMMAND_PICTURE,		//挿絵 (変換中のみ)

	COMMAND_KAITYO,			//改丁
	COMMAND_KAIPAGE,		//改ページ
	COMMAND_KAIMIHIRAKI,	//改見開き
	COMMAND_PAGE_CENTER,	//左右中央

	COMMAND_TITLE_START,	//見出し [val1=type]
	COMMAND_TITLE_END,

	COMMAND_JISAGE_LINE, 		//n字下げ (1行) [val1=字下げ数, val2=折り返し下げ数]
	COMMAND_JISAGE_START,		//n字下げ
	COMMAND_JISAGE_END,			//字下げ終了(共通)
	COMMAND_JISAGE_WRAP_START,	//n字下げ、折り返してn字下げ
	COMMAND_JISAGE_TEN_WRAP_START,	//改行天付き、折り返してn字下げ

	COMMAND_JIAGE_LINE, 	//地付き/地からn字上げ (1行) [val1=地上げ数 (1=地付き、2〜=n字上げ)]
	COMMAND_JIAGE_START,	//開始
	COMMAND_JIAGE_END,		//終了

	COMMAND_BOLD_START,			//太字
	COMMAND_BOLD_END,
	COMMAND_TATETYUYOKO_START,	//縦中横
	COMMAND_TATETYUYOKO_END,
	COMMAND_YOKOGUMI_START,		//横組み
	COMMAND_YOKOGUMI_END,

	COMMAND_BOUTEN_START,	//傍点 [val1=type]
	COMMAND_BOUTEN_END,
	COMMAND_BOUSEN_START,	//傍線 [val1=type]
	COMMAND_BOUSEN_END
};

//見出しタイプ

enum TITLE_TYPE
{
	TITLE_TYPE_DAI = 1,	//大
	TITLE_TYPE_TYUU,	//中
	TITLE_TYPE_SYOU		//小
};

//傍点タイプ

enum BOUTEN_TYPE
{
	BOUTEN_TYPE_GOMA = 1,	//ごま
	BOUTEN_TYPE_SIROGOMA,	//白ごま
	BOUTEN_TYPE_JYANOME,	//蛇の目
	BOUTEN_TYPE_MARU,		//丸
	BOUTEN_TYPE_SIROMARU,	//白丸
	BOUTEN_TYPE_KUROSANKAKU,	//黒三角
	BOUTEN_TYPE_SIROSANKAKU,	//白三角
	BOUTEN_TYPE_NIJUUMARU,		//二重丸
	BOUTEN_TYPE_BATU,			//バツ
};

//傍線タイプ

enum BOUSEN_TYPE
{
	BOUSEN_TYPE_NORMAL = 1,	//直線
	BOUSEN_TYPE_DOUBLE,		//二重線
	BOUSEN_TYPE_KUSARI,		//鎖線
	BOUSEN_TYPE_HASEN,		//破線
	BOUSEN_TYPE_NAMISEN,	//波線
};

