/*$
aobook
Copyright (c) 2014-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * スタイル設定のページ
 *****************************************/

#include "mlk_gui.h"
#include "mlk_widget_def.h"
#include "mlk_widget.h"
#include "mlk_event.h"
#include "mlk_str.h"
#include "mlk_fontinfo.h"

#include "mlk_label.h"
#include "mlk_button.h"
#include "mlk_checkbutton.h"
#include "mlk_lineedit.h"
#include "mlk_groupbox.h"
#include "mlk_colorbutton.h"
#include "mlk_pager.h"
#include "mlk_fileinput.h"
#include "mlk_fontbutton.h"
#include "mlk_combobox.h"

#include "def_style.h"
#include "style.h"


//-----------------

//文字列ID
enum
{
	TRID_SINGLE_PAGE = 1000,
	TRID_DOUBLE_PAGE,
	TRID_CHARS,
	TRID_CHAR_SPACE,
	TRID_LINES,
	TRID_LINE_SPACE,
	TRID_FLAG_HANGING,
	TRID_FLAG_PICTURE,
	TRID_FLAG_DASH_TO_LINE,

	TRID_MARGIN = 1100,
	TRID_LEFT,
	TRID_TOP,
	TRID_RIGHT,
	TRID_BOTTOM,
	TRID_PAGE_MARGIN,
	TRID_BKGND,
	TRID_BKGND_COL,
	TRID_BKGND_IMAGE,
	TRID_BKGND_SCALE,
	TRID_BKGND_TILE,

	TRID_HONBUN = 1200,
	TRID_RUBY,
	TRID_HONBUN_BOLD,
	TRID_PAGEINO,
	TRID_DAKUTEN,
	TRID_DAKUTEN_ITEM,
	TRID_REPLACE_PRINT,
	TRID_SAME_FONT,

	TRID_NOHEAD = 1300,
	TRID_NOBOTTOM,
	TRID_HANGING,
	TRID_NOSEP,
	TRID_REPLACE_CHAR,
	TRID_CHAR_HELP
};

//ウィジェットID
enum
{
	WID_FONT_SAME = 100,
	WID_CHAR_CHECK = 100
};

//-----------------

//基本
typedef struct
{
	mCheckButton *ckpage[2],
		*ckflags[3];
	mLineEdit *edit[4];
}_pagedata_basic;

//画面
typedef struct
{
	mLineEdit *edit[5];
	mFileInput *fi_bkgnd;
	mColorButton *colbtt_bkgnd;
	mCheckButton *ck_bkgndtype[2];
}_pagedata_screen;

//フォント
typedef struct
{
	mFontButton *fbtt[4];
	mColorButton *colbtt[3];
	mComboBox *cb_dakuten;
	mCheckButton *ck_flag;
}_pagedata_font;

//文字
typedef struct
{
	mLineEdit *edit[5];
	mCheckButton *check[4];
}_pagedata_char;

//--------------------


//**********************************
// 基本
//**********************************


/* データ取得 */

static mlkbool _basic_getdata(mPager *p,void *pagedat,void *dst)
{
	_pagedata_basic *pd = (_pagedata_basic *)pagedat;
	StyleDef *ps = (StyleDef *)dst;

	if(!dst) return FALSE;

	//ページ数

	ps->pages = (mCheckButtonIsChecked(pd->ckpage[1]))? 2: 1;

	//文字数など

	ps->chars = mLineEditGetNum(pd->edit[0]);
	ps->char_space = mLineEditGetNum(pd->edit[1]);
	ps->lines = mLineEditGetNum(pd->edit[2]);
	ps->line_space = mLineEditGetNum(pd->edit[3]);

	//フラグ

	ps->flags &= ~(STYLE_F_HANGING | STYLE_F_ENABLE_PICTURE | STYLE_F_DASH_TO_LINE);

	if(mCheckButtonIsChecked(pd->ckflags[0]))
		ps->flags |= STYLE_F_HANGING;

	if(mCheckButtonIsChecked(pd->ckflags[1]))
		ps->flags |= STYLE_F_ENABLE_PICTURE;

	if(mCheckButtonIsChecked(pd->ckflags[2]))
		ps->flags |= STYLE_F_DASH_TO_LINE;

	return TRUE;
}

/* データセット */

static mlkbool _basic_setdata(mPager *p,void *pagedat,void *src)
{
	_pagedata_basic *pd = (_pagedata_basic *)pagedat;
	StyleDef *ps = (StyleDef *)src;

	if(!src) return FALSE;

	//ページ数

	mCheckButtonSetState(pd->ckpage[(ps->pages == 2)], 1);

	//文字数など

	mLineEditSetNum(pd->edit[0], ps->chars);
	mLineEditSetNum(pd->edit[1], ps->char_space);
	mLineEditSetNum(pd->edit[2], ps->lines);
	mLineEditSetNum(pd->edit[3], ps->line_space);

	//フラグ

	mCheckButtonSetState(pd->ckflags[0], (ps->flags & STYLE_F_HANGING));
	mCheckButtonSetState(pd->ckflags[1], (ps->flags & STYLE_F_ENABLE_PICTURE));
	mCheckButtonSetState(pd->ckflags[2], (ps->flags & STYLE_F_DASH_TO_LINE));

	return TRUE;
}

/** "基本" ページ作成 */

mlkbool StyleDlg_createPage_basic(mPager *p,mPagerInfo *info)
{
	_pagedata_basic *pd;
	mWidget *ct;
	mLineEdit *le;
	int i,min,max;

	pd = (_pagedata_basic *)mMalloc0(sizeof(_pagedata_basic));
	if(!pd) return FALSE;

	info->pagedat = pd;
	info->setdata = _basic_setdata;
	info->getdata = _basic_getdata;

	//ページ数

	ct = mContainerCreateHorz(MLK_WIDGET(p), 6, 0, MLK_MAKE32_4(0,0,0,12));

	for(i = 0; i < 2; i++)
	{
		pd->ckpage[i] = mCheckButtonCreate(ct, 0, 0, 0,
			MCHECKBUTTON_S_RADIO, MLK_TR(TRID_SINGLE_PAGE + i), FALSE);
	}

	//文字数など

	ct = mContainerCreateGrid(MLK_WIDGET(p), 4, 5, 7, 0, MLK_MAKE32_4(0,0,0,14));
	
	for(i = 0; i < 4; i++)
	{
		//mLabel

		mLabelCreate(ct, MLF_MIDDLE, (i & 1)? MLK_MAKE32_4(5,0,0,0): 0, 0, MLK_TR(TRID_CHARS + i));

		//mLineEdit
		
		pd->edit[i] = le = mLineEditCreate(ct, 0, 0, 0, MLINEEDIT_S_SPIN);

		mLineEditSetWidth_textlen(le, (i & 1)? 8: 5);

		if(!(i & 1))
			//字数/行数
			min = 3, max = 200;
		else if(i == 1)
		{
			//字間
			min = -200, max = 200;
			mLineEditSetLabelText(le, "px", FALSE);
		}
		else
		{
			//行間
			min = 0, max = 1000;
			mLineEditSetLabelText(le, "%", FALSE);
		}
		
		mLineEditSetNumStatus(le, min, max, 0);
	}

	//フラグ

	ct = mContainerCreateVert(MLK_WIDGET(p), 4, 0, 0);

	for(i = 0; i < 3; i++)
		pd->ckflags[i] = mCheckButtonCreate(ct, 0, 0, 0, 0, MLK_TR(TRID_FLAG_HANGING + i), FALSE);

	return TRUE;
}


//**********************************
// 画面
//**********************************


/* データ取得 */

static mlkbool _screen_getdata(mPager *p,void *pagedat,void *dst)
{
	_pagedata_screen *pd = (_pagedata_screen *)pagedat;
	StyleDef *ps = (StyleDef *)dst;

	if(!dst) return FALSE;

	//余白

	ps->margin.x1 = mLineEditGetNum(pd->edit[0]);
	ps->margin.y1 = mLineEditGetNum(pd->edit[1]);
	ps->margin.x2 = mLineEditGetNum(pd->edit[2]);
	ps->margin.y2 = mLineEditGetNum(pd->edit[3]);

	ps->page_space = mLineEditGetNum(pd->edit[4]);

	//背景

	ps->col_bkgnd = mColorButtonGetColor(pd->colbtt_bkgnd);

	mFileInputGetPath(pd->fi_bkgnd, &ps->str_bkgndimg);

	if(mCheckButtonIsChecked(pd->ck_bkgndtype[1]))
		ps->flags |= STYLE_F_BKGND_TILE;
	else
		ps->flags &= ~STYLE_F_BKGND_TILE;

	return TRUE;
}

/* データセット */

static mlkbool _screen_setdata(mPager *p,void *pagedat,void *src)
{
	_pagedata_screen *pd = (_pagedata_screen *)pagedat;
	StyleDef *ps = (StyleDef *)src;

	if(!src) return FALSE;

	//余白

	mLineEditSetNum(pd->edit[0], ps->margin.x1);
	mLineEditSetNum(pd->edit[1], ps->margin.y1);
	mLineEditSetNum(pd->edit[2], ps->margin.x2);
	mLineEditSetNum(pd->edit[3], ps->margin.y2);
	mLineEditSetNum(pd->edit[4], ps->page_space);

	//背景

	mColorButtonSetColor(pd->colbtt_bkgnd, ps->col_bkgnd);

	mFileInputSetPath(pd->fi_bkgnd, ps->str_bkgndimg.buf);

	mCheckButtonSetState(pd->ck_bkgndtype[((ps->flags & STYLE_F_BKGND_TILE) != 0)], 1);

	return TRUE;
}

/** "画面" ページ作成 */

mlkbool StyleDlg_createPage_screen(mPager *p,mPagerInfo *info)
{
	_pagedata_screen *pd;
	mWidget *ct;
	mGroupBox *gb;
	mLineEdit *le;
	int i;

	pd = (_pagedata_screen *)mMalloc0(sizeof(_pagedata_screen));
	if(!pd) return FALSE;

	info->pagedat = pd;
	info->setdata = _screen_setdata;
	info->getdata = _screen_getdata;

	//---- 余白

	gb = mGroupBoxCreate(MLK_WIDGET(p), MLF_EXPAND_W, MLK_MAKE32_4(0,0,0,15), 0, MLK_TR(TRID_MARGIN));

	mContainerSetType_grid(MLK_CONTAINER(gb), 4, 5, 7);

	for(i = 0; i < 5; i++)
	{
		mLabelCreate(MLK_WIDGET(gb), MLF_MIDDLE,
			(i & 1)? MLK_MAKE32_4(5,0,0,0): 0, 0, MLK_TR(TRID_LEFT + i));

		//mLineEdit

		pd->edit[i] = le = mLineEditCreate(MLK_WIDGET(gb), 0, 0, 0, MLINEEDIT_S_SPIN);

		mLineEditSetWidth_textlen(le, 6);
		mLineEditSetNumStatus(le, 0, 1000, 0);
	}

	//---- 背景

	gb = mGroupBoxCreate(MLK_WIDGET(p), MLF_EXPAND_W, 0, 0, MLK_TR(TRID_BKGND));

	mContainerSetType_grid(MLK_CONTAINER(gb), 2, 7, 7);

	//背景色

	mLabelCreate(MLK_WIDGET(gb), MLF_MIDDLE, 0, 0, MLK_TR(TRID_BKGND_COL));

	pd->colbtt_bkgnd = mColorButtonCreate(MLK_WIDGET(gb), 0, 0, 0, MCOLORBUTTON_S_DIALOG, 0);

	//[背景画像]

	mLabelCreate(MLK_WIDGET(gb), MLF_MIDDLE, 0, 0, MLK_TR(TRID_BKGND_IMAGE));

	//ファイル名

	pd->fi_bkgnd = mFileInputCreate_file(MLK_WIDGET(gb), 0, MLF_EXPAND_W, 0, 0,
		"Image files (BMP/PNG/JPEG)\t*.bmp;*.png;*.jpg;*.jpeg\tAll files\t*", 0, NULL);

	//背景タイプ

	mWidgetNew(MLK_WIDGET(gb), 0);

	ct = mContainerCreateVert(MLK_WIDGET(gb), 4, 0, 0);

	for(i = 0; i < 2; i++)
		pd->ck_bkgndtype[i] = mCheckButtonCreate(ct, 0, 0, 0, MCHECKBUTTON_S_RADIO, MLK_TR(TRID_BKGND_SCALE + i), 0);

	return TRUE;
}


//**********************************
// フォント
//**********************************


/* データ取得 */

static mlkbool _font_getdata(mPager *p,void *pagedat,void *dst)
{
	_pagedata_font *pd = (_pagedata_font *)pagedat;
	StyleDef *ps = (StyleDef *)dst;

	if(!dst) return FALSE;

	//フォント

	mFontButtonGetInfo_text(pd->fbtt[0], &ps->str_fontmain);
	mFontButtonGetInfo_text(pd->fbtt[1], &ps->str_fontruby);
	mFontButtonGetInfo_text(pd->fbtt[2], &ps->str_fontbold);
	mFontButtonGetInfo_text(pd->fbtt[3], &ps->str_fontinfo);

	//色

	ps->col_text = mColorButtonGetColor(pd->colbtt[0]);
	ps->col_ruby = mColorButtonGetColor(pd->colbtt[1]);
	ps->col_info = mColorButtonGetColor(pd->colbtt[2]);

	//濁点

	ps->dakuten_type = mComboBoxGetItemParam(pd->cb_dakuten, -1);

	//フラグ

	if(mCheckButtonIsChecked(pd->ck_flag))
		ps->flags |= STYLE_F_REPLACE_PRINT;
	else
		ps->flags &= ~STYLE_F_REPLACE_PRINT;

	return TRUE;
}

/* データセット */

static mlkbool _font_setdata(mPager *p,void *pagedat,void *src)
{
	_pagedata_font *pd = (_pagedata_font *)pagedat;
	StyleDef *ps = (StyleDef *)src;

	if(!src) return FALSE;

	//フォント

	mFontButtonSetInfo_text(pd->fbtt[0], ps->str_fontmain.buf);
	mFontButtonSetInfo_text(pd->fbtt[1], ps->str_fontruby.buf);
	mFontButtonSetInfo_text(pd->fbtt[2], ps->str_fontbold.buf);
	mFontButtonSetInfo_text(pd->fbtt[3], ps->str_fontinfo.buf);

	//色

	mColorButtonSetColor(pd->colbtt[0], ps->col_text);
	mColorButtonSetColor(pd->colbtt[1], ps->col_ruby);
	mColorButtonSetColor(pd->colbtt[2], ps->col_info);

	//濁点

	mComboBoxSetSelItem_atIndex(pd->cb_dakuten, ps->dakuten_type);

	//フラグ

	mCheckButtonSetState(pd->ck_flag, ps->flags & STYLE_F_REPLACE_PRINT);

	return TRUE;
}

/* フォンを統一 */

static void _font_same_main(_pagedata_font *p)
{
	mFontInfo infos,infod;
	int i;

	mFontInfoInit(&infos);
	mFontInfoInit(&infod);

	//本文フォント取得

	mFontButtonGetInfo(p->fbtt[0], &infos);

	//フォント名を同じに

	for(i = 1; i < 4; i++)
	{
		mFontButtonGetInfo(p->fbtt[i], &infod);

		mFontInfoCopyName(&infod, &infos);

		mFontButtonSetInfo(p->fbtt[i], &infod);
	}

	mFontInfoFree(&infos);
	mFontInfoFree(&infod);
}

/* イベント */

static int _font_event(mPager *p,mEvent *ev,void *pagedat)
{
	if(ev->type == MEVENT_NOTIFY
		&& ev->notify.id == WID_FONT_SAME)
	{
		//フォントを統一

		_font_same_main((_pagedata_font *)pagedat);
	
		return 1;
	}

	return 0;
}

/** "フォント" ページ作成 */

mlkbool StyleDlg_createPage_font(mPager *p,mPagerInfo *info)
{
	_pagedata_font *pd;
	mWidget *ct;
	mComboBox *cb;
	int i,cno;

	pd = (_pagedata_font *)mMalloc0(sizeof(_pagedata_font));
	if(!pd) return FALSE;

	info->pagedat = pd;
	info->setdata = _font_setdata;
	info->getdata = _font_getdata;
	info->event = _font_event;

	//---- フォント・色

	ct = mContainerCreateGrid(MLK_WIDGET(p), 3, 7, 7, MLF_EXPAND_W, MLK_MAKE32_4(0,0,0,10));

	cno = 0;

	for(i = 0; i < 4; i++)
	{
		mLabelCreate(ct, MLF_MIDDLE, 0, 0, MLK_TR(TRID_HONBUN + i));

		//mFontButton

		pd->fbtt[i] = mFontButtonCreate(ct, 0, MLF_EXPAND_W | MLF_MIDDLE, 0, 0);

		mFontButtonSetFlags(pd->fbtt[i], MSYSDLG_FONT_F_ALL);
		
		//mColorButton

		if(i == 2)
			mWidgetNew(ct, 0); //本文太字
		else
		{
			pd->colbtt[cno] = mColorButtonCreate(ct, 0, MLF_MIDDLE, 0, MCOLORBUTTON_S_DIALOG, 0);
			cno++;
		}
	}

	//統一

	mButtonCreate(MLK_WIDGET(p), WID_FONT_SAME, MLF_RIGHT, MLK_MAKE32_4(0,0,0,18), 0, MLK_TR(TRID_SAME_FONT));

	//---- 濁点

	ct = mContainerCreateHorz(MLK_WIDGET(p), 6, 0, MLK_MAKE32_4(0,0,0,15));

	mLabelCreate(ct, MLF_MIDDLE, 0, 0, MLK_TR(TRID_DAKUTEN));

	//

	pd->cb_dakuten = cb = mComboBoxCreate(ct, 0, 0, 0, 0);

	mComboBoxAddItems_sepnull(cb, MLK_TR(TRID_DAKUTEN_ITEM), 0);
	mComboBoxSetAutoWidth(cb);

	//--- フラグ

	pd->ck_flag = mCheckButtonCreate(MLK_WIDGET(p), 0, 0, 0, 0, MLK_TR(TRID_REPLACE_PRINT), FALSE);

	return TRUE;
}


//**********************************
// 文字
//**********************************


/* データ取得 */

static mlkbool _char_getdata(mPager *p,void *pagedat,void *dst)
{
	_pagedata_char *pd = (_pagedata_char *)pagedat;
	StyleDef *ps = (StyleDef *)dst;
	uint32_t **ptr[5],**pp;
	mStr str = MSTR_INIT;
	int i;

	if(!dst) return FALSE;

	StyleDef_getCharsArray(ps, ptr);

	for(i = 0; i < 5; i++)
	{
		pp = ptr[i];

		if(i != 4 && !mCheckButtonIsChecked(pd->check[i]))
			//デフォルト
			StyleDef_setCharsText(pp, (const char *)1);
		else
		{
			mLineEditGetTextStr(pd->edit[i], &str);
			
			StyleDef_setCharsText(pp, str.buf);
		}
	}

	mStrFree(&str);
	
	return TRUE;
}

/* データセット */

static mlkbool _char_setdata(mPager *p,void *pagedat,void *src)
{
	_pagedata_char *pd = (_pagedata_char *)pagedat;
	StyleDef *ps = (StyleDef *)src;
	uint32_t **ptr[5],*pu;
	int i,fdefault;

	if(!src) return FALSE;

	StyleDef_getCharsArray(ps, ptr);

	for(i = 0; i < 5; i++)
	{
		pu = *(ptr[i]);

		fdefault = (pu == STYLE_CHARS_DEFAULT);

		//テキスト

		if(fdefault)
			mLineEditSetText_utf32(pd->edit[i], StyleDef_getCharsDefault(i));
		else
			mLineEditSetText_utf32(pd->edit[i], pu);

		//チェック

		if(i != 4)
		{
			mCheckButtonSetState(pd->check[i], !fdefault);

			mLineEditSetReadOnly(pd->edit[i], fdefault);
		}
	}

	return TRUE;
}

/* イベント */

static int _char_event(mPager *p,mEvent *ev,void *pagedat)
{
	if(ev->type == MEVENT_NOTIFY
		&& ev->notify.id >= WID_CHAR_CHECK && ev->notify.id < WID_CHAR_CHECK + 4)
	{
		//---- チェック

		_pagedata_char *pd = (_pagedata_char *)pagedat;
		int no = ev->notify.id - WID_CHAR_CHECK;

		//OFF なら READONLY

		mLineEditSetReadOnly(pd->edit[no], !ev->notify.param1);

		//デフォルトの文字列

		if(!ev->notify.param1)
			mLineEditSetText_utf32(pd->edit[no], StyleDef_getCharsDefault(no));
	
		return 1;
	}

	return 0;
}

/** "文字" ページ作成 */

mlkbool StyleDlg_createPage_char(mPager *p,mPagerInfo *info)
{
	_pagedata_char *pd;
	mWidget *ct;
	int i;

	pd = (_pagedata_char *)mMalloc0(sizeof(_pagedata_char));
	if(!pd) return FALSE;

	info->pagedat = pd;
	info->setdata = _char_setdata;
	info->getdata = _char_getdata;
	info->event = _char_event;

	//----

	ct = mContainerCreateGrid(MLK_WIDGET(p), 2, 5, 8, MLF_EXPAND_W, 0);

	for(i = 0; i < 5; i++)
	{
		if(i == 4)
			mLabelCreate(ct, MLF_MIDDLE, 0, 0, MLK_TR(TRID_REPLACE_CHAR));
		else
			pd->check[i] = mCheckButtonCreate(ct, WID_CHAR_CHECK + i, MLF_MIDDLE, 0, 0, MLK_TR(TRID_NOHEAD + i), FALSE);

		//

		pd->edit[i] = mLineEditCreate(ct, 0, MLF_EXPAND_W, 0, 0);
	}

	//説明

	mLabelCreate(MLK_WIDGET(p), MLF_EXPAND_W, MLK_MAKE32_4(0,15,0,0), MLABEL_S_BORDER, MLK_TR(TRID_CHAR_HELP));

	return TRUE;
}

