/*$
aobook
Copyright (c) 2014-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * ファイル開くダイアログ (テキスト用)
 *****************************************/

#include "mlk_gui.h"
#include "mlk_widget_def.h"
#include "mlk_widget.h"
#include "mlk_window.h"
#include "mlk_filedialog.h"
#include "mlk_label.h"
#include "mlk_combobox.h"

#include "text.h"
#include "trid.h"


//---------------

typedef struct
{
	MLK_FILEDIALOG_DEF

	mComboBox *cb_charcode;
}_dialog;

enum
{
	TRID_CHARCODE = 0,
	TRID_AUTO
};

#define WID_CB_CHARCODE  100

//---------------


/** 拡張ウィジェット作成 */

static void _create_widget(_dialog *p)
{
	mWidget *ct;
	mComboBox *cb;
	int i;

	MLK_TRGROUP(TRGROUP_ID_FILEDIALOG);

	//水平コンテナ

	ct = mContainerCreateHorz(MLK_WIDGET(p), 5, 0, MLK_MAKE32_4(0,10,0,0));

	//ラベル

	mLabelCreate(ct, MLF_MIDDLE, 0, 0, MLK_TR(TRID_CHARCODE));

	//コンボボックス

	p->cb_charcode = cb = mComboBoxCreate(ct, WID_CB_CHARCODE, MLF_MIDDLE, 0, 0);

	mComboBoxAddItem_static(cb, MLK_TR(TRID_AUTO), -1);

	for(i = 0; i < TEXT_CODE_NUM; i++)
		mComboBoxAddItem_static(cb, TextGetCodeName(i), i);

	mComboBoxSetAutoWidth(cb);
	mComboBoxSetSelItem_atIndex(cb, 0);
}

/** ファイルを開くダイアログ */

mlkbool OpenDialog_textFile(mWindow *parent,const char *initdir,mStr *strdst,int *retcode)
{
	_dialog *p;
	mlkbool ret;

	p = (_dialog *)mFileDialogNew(parent, sizeof(_dialog), 0, MFILEDIALOG_TYPE_OPENFILE);
	if(!p) return FALSE;

	mFileDialogInit(MLK_FILEDIALOG(p),
		"all files\t*\ttext file (*.txt)\t*.txt\tzip file (*.zip)\t*.zip", 0,
		initdir, strdst);

	_create_widget(p);

	mFileDialogShow(MLK_FILEDIALOG(p));

	//

	ret = mDialogRun(MLK_DIALOG(p), FALSE);

	if(ret)
		*retcode = mComboBoxGetItemParam(p->cb_charcode, -1);

	mWidgetDestroy(MLK_WIDGET(p));

	return ret;
}

