/*$
aobook
Copyright (c) 2014-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * MainWindow - command
 *****************************************/

#include "mlk_gui.h"
#include "mlk_widget_def.h"
#include "mlk_widget.h"
#include "mlk_window.h"
#include "mlk_sysdlg.h"
#include "mlk_progressbar.h"
#include "mlk_popup_progress.h"
#include "mlk_thread.h"
#include "mlk_file.h"
#include "mlk_str.h"
#include "mlk_list.h"
#include "mlk_filelist.h"
#include "mlk_util.h"

#include "def_global.h"
#include "def_widget.h"
#include "func_widget.h"
#include "text.h"
#include "style.h"
#include "layout.h"
#include "trid.h"


//--------------

/* unzip.c */
mlkbool extractZipFile(const char *zipname,const char *filename,mBufSize *dst);

//--------------


//========================
// 最初のレイアウト
//========================


/* レイアウトのスレッド処理 */

static void _thread_layout(mThread *th)
{
	mPopupProgress *prog = (mPopupProgress *)th->param;

	LayoutRunFirst(GDAT->layout, prog);

	mPopupProgressThreadEnd(prog);
}

/* テキストのレイアウト実行 (読み込み/スタイル変更時) */

static void _run_layout(MainWindow *p)
{
	mPopupProgress *prog;
	mBox box;
	int w;

	GDAT->curpage = NULL;

	//

	w = p->wg.w;
	if(w > 200) w = 200;

	mWidgetGetBox_rel(MLK_WIDGET(p), &box);

	//スレッド

	prog = mPopupProgressNew(0, MPROGRESSBAR_S_FRAME);
	if(!prog) return;

	GDAT->is_in_thread = TRUE;

	mPopupProgressRun(prog, MLK_WIDGET(p),
		0, 0, &box,
		MPOPUP_F_LEFT | MPOPUP_F_BOTTOM | MPOPUP_F_GRAVITY_RIGHT | MPOPUP_F_GRAVITY_TOP,
		w, _thread_layout);

	GDAT->is_in_thread = FALSE;

	mWidgetDestroy(MLK_WIDGET(prog));

	//

	Toolbar_changePageNum(p->toolbar);
}


//========================
// テキスト読み込み
//========================


/* ZIP ファイル判定 */

static mlkbool _is_zipfile(const char *filename)
{
	uint8_t b[4];

	//先頭 4byte 読み込み

	if(mReadFileHead(filename, b, 4)) return FALSE;

	//ローカルファイルヘッダ識別子

	return (b[0] == 0x50 && b[1] == 0x4b && b[2] == 0x03 && b[3] == 0x04);
}

/* テキストファイル読み込み
 *
 * pcode: [IN] 文字コード [OUT] 自動判定時、実際の文字コード
 * return: 0 で成功、それ以外でエラーの TRID */

static int _load_textfile(const char *filename,int *pcode)
{
	mBufSize txtbuf,unibuf,pvbuf;
	uint8_t *buf;
	int rescode;
	int32_t size;

	//-------- データ変換

	//ソーステキスト読み込み

	if(_is_zipfile(filename))
	{
		//ZIP 先頭ファイル
		
		if(!extractZipFile(filename, NULL, &txtbuf))
			return TRID_MES_ERR_LOADFILE;

		GDAT->is_file_zip = TRUE;
	}
	else
	{
		//通常テキスト
		
		if(mReadFileFull_alloc(filename, 0, &buf, &size))
			return TRID_MES_ERR_LOADFILE;
		else
		{
			txtbuf.buf = (void *)buf;
			txtbuf.size = size;
		}

		GDAT->is_file_zip = FALSE;
	}

	//UTF-16BE に変換

	rescode = TextConvert_to_utf16be(&txtbuf, &unibuf, *pcode);

	mFree(txtbuf.buf);
	
	if(rescode < 0)
		return TRID_MES_ERR_CONVCODE;

	*pcode = rescode;

	//内部データに変換 (unibuf は自動で解放される)

	if(!TextConvert_to_internal(&unibuf, &pvbuf))
		return TRID_MES_ERR_LOADFILE;

	//--------- データセット

	GlobalSetTextData(&pvbuf);

	return 0;
}

/** テキストファイル読み込み
 *
 * code: テキストの文字コード。負の値で自動。
 * no: reload = TRUE の場合は、ページ位置。FALSE で行位置。
 * reload: TRUE で再読込。 */

void MainWindow_loadText(MainWindow *p,
	const char *filename,int code,int no,mlkbool reload)
{
	int ret;
	PageInfoItem *page;

	//テキスト読み込み

	ret = _load_textfile(filename, &code);

	if(ret)
	{
		//------ エラー

		mStr str = MSTR_INIT;

		mStrPathGetBasename(&str, filename);
		mStrAppendText(&str, "\n\n");
		mStrAppendText(&str, MLK_TR2(TRGROUP_ID_MESSAGE, ret));

		mMessageBoxErr(MLK_WINDOW(p), str.buf);

		mStrFree(&str);
	}
	else
	{
		//-------- OK

		//ファイル情報セット

		if(!reload)
		{
			mStrSetText(&GDAT->strFileName, filename);

			GDAT->text_charcode = code;

			//履歴

			GlobalAddRecentFile(filename, code, no);

			MainWindow_setRecentFileMenu(p);
		}

		//レイアウト

		_run_layout(p);

		//ページ位置

		if(reload)
			page = LayoutGetPage_pageno(GDAT->layout, no);
		else
			page = LayoutGetPage_lineno(GDAT->layout, no, FALSE);

		//更新

		MainWindow_setTitle(p);
		MainWindow_changePage(p, page);
	}
}


//=======================
// ほか
//=======================


/** 次/前のファイル読み込み */

void MainWindow_loadText_nextprev(MainWindow *p,mlkbool next)
{
	mStr dir = MSTR_INIT,fname = MSTR_INIT;
	mList list = MLIST_INIT;
	mFileListItem *pi;

	if(GLOBAL_IS_EMPTY_TEXT) return;

	//ディレクトリとファイル名

	mStrPathGetDir(&dir, GDAT->strFileName.buf);
	mStrPathGetBasename(&fname, GDAT->strFileName.buf);

	//ファイルリスト作成

	mFileList_create(&list, dir.buf, mFileList_func_excludeDir, 0);

	mFileList_sort_name(&list);

	//現在のファイルを検索し、前後のファイル名を取得

	pi = mFileList_find_name(&list, fname.buf);

	if(pi)
	{
		pi = (mFileListItem *)((next)? pi->i.next: pi->i.prev);

		if(pi) mStrPathJoin(&dir, pi->name);
	}

	//

	mListDeleteAll(&list);
	mStrFree(&fname);

	//読み込み

	if(pi)
		MainWindow_loadText(p, dir.buf, -1, 0, FALSE);

	mStrFree(&dir);
}

/** ファイル履歴から読み込み */

void MainWindow_loadText_recent(MainWindow *p,int no)
{
	mStr str = MSTR_INIT;
	int code,line;

	GlobalGetRecentFileInfo(no, &str, &code, &line);

	MainWindow_loadText(p, str.buf, code, line, FALSE);

	mStrFree(&str);
}

/** ファイル再読み込み */

void MainWindow_reloadFile(MainWindow *p)
{
	if(GLOBAL_IS_EMPTY_TEXT) return;

	MainWindow_loadText(p, GDAT->strFileName.buf,
		GDAT->text_charcode, LayoutGetPageNo(GDAT->curpage), TRUE);
}

/** テキストを再レイアウト */

void MainWindow_text_layout(MainWindow *p)
{
	int line;

	if(GLOBAL_IS_EMPTY_TEXT) return;

	line = LayoutGetPageLineNo(GDAT->curpage);

	//再レイアウト

	_run_layout(p);

	//更新

	MainWindow_changePage(p, LayoutGetPage_lineno(GDAT->layout, line, FALSE));
}

/** ウィンドウサイズをレイアウトのサイズに変更 */

void MainWindow_setWindowSize(MainWindow *p)
{
	mWidget *wg;
	mSize size;

	wg = p->wg_canvas;

	StyleWork_getScreenSize(GDAT->style, &size);

	if(wg->w != size.w || wg->h != size.h)
	{
		//キャンバスのサイズをセットして計算
		
		wg->hintW = size.w;
		wg->hintH = size.h;

		mWidgetSetRecalcHint(wg);
		mGuiCalcHintSize();

		mWidgetResize(MLK_WIDGET(p), p->wg.hintW, p->wg.hintH);

		//キャンバスの推奨サイズを戻す

		wg->hintW = wg->hintH = 1;
		mWidgetSetRecalcHint(wg);
		mGuiCalcHintSize();
	}
}

/** ページ位置の変更 */

void MainWindow_changePage(MainWindow *p,PageInfoItem *page)
{
	if(GDAT->curpage != page)
	{
		GDAT->curpage = page;

		Toolbar_changePagePos(p->toolbar);

		MainWindow_update(p);
	}
}

/** ページ移動 (ページ番号、またはコマンド)
 *
 * no: 負の値でコマンド、0 以上でページ番号 */

void MainWindow_movePage(MainWindow *p,int no)
{
	LayoutDat *layout = GDAT->layout;
	PageInfoItem *page;

	if(GLOBAL_IS_EMPTY_TEXT) return;

	if(no >= 0)
		page = LayoutGetPage_pageno(layout, no);
	else
	{
		switch(no)
		{
			case PAGENO_NEXT:
			case PAGENO_PREV:
				page = LayoutGetPage_move(layout, GDAT->curpage, (no == PAGENO_NEXT)? 1: -1);
				break;
			case PAGENO_HOME:
			case PAGENO_END:
				page = LayoutGetPage_homeEnd(layout, (no == PAGENO_END));
				break;
		}
	}

	MainWindow_changePage(p, page);
}

/** ページ移動 (行番号)
 *
 * wrap_top: TRUE = 指定行番号位置が折り返している場合、行頭が見えるページへ。
 *  FALSE の場合、折り返し後のページ。 */

void MainWindow_movePage_lineno(MainWindow *p,int no,mlkbool wrap_top)
{
	if(GLOBAL_IS_EMPTY_TEXT) return;

	MainWindow_changePage(p, LayoutGetPage_lineno(GDAT->layout, no, wrap_top));
}

/** ツールのコマンドを実行
 *
 * ZIP に対しても何かしらコマンドを実行する場合もあるため、テキスト/ZIP に関係なく実行する。 */

void MainWindow_execTool(MainWindow *p,int no)
{
	mStr str = MSTR_INIT,str2 = MSTR_INIT,strrep[3];

	if(GLOBAL_IS_EMPTY_TEXT) return;

	//コマンド文字列

	mStrGetSplitText(&str, GDAT->strTool[no].buf, '\t', 1);

	//パラメータ置き換え

	mStrArrayInit(strrep, 3);

	mStrAppendText_escapeForCmdline(&str2, GDAT->strFileName.buf);

	mStrSetFormat(strrep, "f%t", &str2);
	mStrSetFormat(strrep + 1, "l%d", LayoutGetPageLineNo(GDAT->curpage) + 1);
	mStrSetFormat(strrep + 2, "c%s", TextGetCodeName_def(GDAT->text_charcode));

	mStrReplaceParams(&str, '%', strrep, 3);

	mStrFree(&str2);
	mStrArrayFree(strrep, 3);

	//実行

	if(mStrIsnotEmpty(&str))
		mExec(str.buf);
	
	mStrFree(&str);
}

