/*$
aobook
Copyright (c) 2014-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/**********************************
 * レイアウト
 *
 * 内部データから1行分のレイアウト用データ取得
 **********************************/

#include "mlk.h"
#include "mlk_list.h"
#include "mlk_buf.h"

#include "font.h"
#include "pv_layout.h"
#include "def_textdata.h"
#include "def_style.h"


//-----------------

#define _BST_FLAG_ON(a)  bst->flags |= BLOCKSTATE_F_ ## a
#define _BST_FLAG_OFF(a) bst->flags &= ~(BLOCKSTATE_F_ ## a)

//-----------------


//=================================
// コマンド処理
//=================================


/** 値なしのコマンド処理
 *
 * cmd: コマンド番号
 * return: ページ関連のコマンドなら、コマンド番号。ほかは -1。 */

static int _proc_command_noval(LayoutWork *p,int cmd)
{
	LineState *lst = &p->linestate;
	BlockState *bst = &p->blockstate;

	switch(cmd)
	{
		//改ページ/改丁/改見開き
		case COMMAND_KAIPAGE:
		case COMMAND_KAITYO:
		case COMMAND_KAIMIHIRAKI:
			return cmd;
		//字下げ終わり [block]
		case COMMAND_JISAGE_END:
			bst->jisage = bst->jisage_wrap = 0;
			break;
		//縦中横 [line]
		case COMMAND_TATETYUYOKO_START:
			lst->tatetyuyoko_num = 1;
			break;
		case COMMAND_TATETYUYOKO_END:
			lst->tatetyuyoko_num = 0;
			break;
		//横組み
		case COMMAND_YOKOGUMI_START:
			_BST_FLAG_ON(YOKOGUMI);
			break;
		case COMMAND_YOKOGUMI_END:
			_BST_FLAG_OFF(YOKOGUMI);
			break;
		//太字
		case COMMAND_BOLD_START:
			_BST_FLAG_ON(BOLD);
			break;
		case COMMAND_BOLD_END:
			_BST_FLAG_OFF(BOLD);
			break;
		//傍点終わり
		case COMMAND_BOUTEN_END:
			lst->bouten = 0;
			break;
		//傍線終わり
		case COMMAND_BOUSEN_END:
			lst->bousen = 0;
			break;
		//地付き/地上げ終わり
		case COMMAND_JIAGE_END:
			bst->jiage = 0;
			break;
		//見出し終わり
		case COMMAND_TITLE_END:
			bst->title = 0;

			if(p->plist_title)
				layout_append_titlelist(p);
			break;
		//ページの左右中央
		case COMMAND_PAGE_CENTER:
			p->pagestate.fcenter = TRUE;
			break;
	}

	return -1;
}

/** 値付きのコマンド処理 */

static void _proc_command_val(LayoutWork *p,uint8_t **pptext)
{
	uint8_t *ps,cmd,val1,val2;
	LineState *lst = &p->linestate;
	BlockState *bst = &p->blockstate;

	ps = *pptext;

	cmd  = ps[0];
	val1 = ps[1];
	val2 = ps[2];

	*pptext = ps + 3;

	switch(cmd)
	{
		//字下げ [line]
		case COMMAND_JISAGE_LINE:
			lst->jisage = val1;
			break;
		//字下げ [block]
		case COMMAND_JISAGE_START:
			bst->jisage = val1;
			bst->jisage_wrap = val1;
			break;
		//字下げ、折り返し
		case COMMAND_JISAGE_WRAP_START:
			bst->jisage = val1;
			bst->jisage_wrap = val2;
			break;
		//字下げ、天付き・折り返し
		case COMMAND_JISAGE_TEN_WRAP_START:
			bst->jisage = 0;
			bst->jisage_wrap = val1;
			break;
		//傍点開始
		case COMMAND_BOUTEN_START:
			lst->bouten = val1;
			break;
		//傍線開始
		case COMMAND_BOUSEN_START:
			lst->bousen = val1;
			break;
		//地付き/地からn字上げ [line]
		case COMMAND_JIAGE_LINE:
			lst->jiage = val1;
			break;
		//地付き/地からn字上げ [block]
		case COMMAND_JIAGE_START:
			bst->jiage = val1;
			break;
		//見出し
		case COMMAND_TITLE_START:
			bst->title = val1;
			break;
	}
}

/** 挿絵処理
 * 
 * [uint16] 文字列長さ, UTF-8 ファイル名 (NULL 文字含む) */

static void _proc_picture(LayoutWork *p,uint8_t **pptext)
{
	uint8_t *ps;
	int len;

	ps = *pptext;

	p->pagestate.picture = ps;

	len = *((uint16_t *)ps);

	*pptext = ps + 2 + len;
}


//=================================
// ルビなし本文文字列追加
//=================================


/** [本文文字追加時] CharItem に、現在の注記状態を適用する
 *
 * [!] 縦中横の場合、2文字目以降は pi が削除される。 */

static void _set_char_state(LayoutWork *p,CharItem *pi)
{
	CharItem *pitop;
	BlockState *bst = &p->blockstate;
	LineState *lst = &p->linestate;
	int n;

	//[block/line] 地付き/地上げ

	if(lst->jiage || bst->jiage)
		pi->flags |= CHARITEM_F_JIAGE;

	//[block] 太字

	if(bst->flags & BLOCKSTATE_F_BOLD)
		pi->flags |= CHARITEM_F_BOLD;

	//[line] 傍点/傍線

	pi->bouten = lst->bouten;
	pi->bousen = lst->bousen;

	//見出し文字列
	// :見出しが終わるまで、buf_title に文字を追加していく
	// :※最初のレイアウト時のみ

	if(bst->title && p->plist_title)
	{
		//最初の文字の場合、1byte目に見出しタイプをセット
		
		if(p->buf_title.cursize == 0)
			mBufAppendByte(&p->buf_title, bst->title);

		//1文字追加

		if(p->buf_title.cursize <= 256 * 4)
			mBufAppend(&p->buf_title, &pi->code, 4);
	}

	//----- 文字の並べ方

	if(bst->flags & BLOCKSTATE_F_YOKOGUMI)
		//[block] 横組み
		pi->chartype = CHARITEM_TYPE_ROTATE;
	else if(lst->tatetyuyoko_num)
	{
		//[line] 縦中横

		n = lst->tatetyuyoko_num; //+1 されている
	
		if(n < 4 && pi->code < 127)
		{
			if(n == 1)
			{
				//最初の文字
			
				pi->chartype = CHARITEM_TYPE_HORZ;
				pi->horzcnt = 1;
				pi->horzchar[0] = (uint8_t)pi->code;

				lst->tatetyuyoko_top = pi;
			}
			else
			{
				//2〜3文字目は、先頭文字に結合して、CharItem 削除

				pitop = lst->tatetyuyoko_top;

				pitop->horzcnt++;
				pitop->horzchar[n - 1] = (uint8_t)pi->code;
			
				mListDelete(&p->list_char, MLISTITEM(pi));
			}

			lst->tatetyuyoko_num++;
		}
	}
}

/** [本文文字追加時] 文字列追加後の調整
 *
 * top: 追加された先頭位置
 * return: 実際に追加された文字数 (表示上での1文字) */

static int _text_adjust(LayoutWork *p,CharItem *top)
{
	CharItem *pi,*next,*prev,*third;
	uint32_t c;
	int len = 0;

	for(pi = top; pi; pi = next)
	{
		prev = (CharItem *)pi->i.prev;
		next = (CharItem *)pi->i.next;

		c = pi->code;
		len++;

		//文字置き換え

		if(layout_replace_char(p, &c))
			pi->code = c;

		//

		if(c == U'゛' || c == U'゜')
		{
			//濁点/半濁点の場合、1文字にまとめる

			if(prev && !(prev->flags & (CHARITEM_F_DAKUTEN | CHARITEM_F_HANDAKUTEN)))
			{
				prev->flags |= (c == U'゛')? CHARITEM_F_DAKUTEN: CHARITEM_F_HANDAKUTEN;

				mListDelete(&p->list_char, MLISTITEM(pi));
				len--;
			}
		}
		else if(c == U'／')
		{
			//くの字点を置換え

			if(next && next->code == U'＼')
			{
				//通常
				
				pi->code = 0x3033;
				next->code = 0x3035;
			}
			else if(next && next->code == U'″'
				&& next->i.next && ((CharItem *)next->i.next)->code == U'＼')
			{
				//濁点付き
			
				third = (CharItem *)next->i.next;

				mListDelete(&p->list_char, MLISTITEM(next));
				len--;
			
				pi->code = 0x3034;
				third->code = 0x3035;

				next = third;
			}
		}
	}

	return len;
}


//=================================
// 1行分取得
//=================================


/** ルビなしの本文文字列を追加
 *
 * [uint16] 文字数 [uint16 or uint32 x 文字数] 文字列
 *
 * is32bit: TRUE で 32bit、FALSE で 16bit 文字列
 * ppchar: NULL 以外の場合、追加された先頭の文字データを返す
 * return: 実際に追加された文字数 */

static int _append_text(LayoutWork *p,uint8_t **pptext,mlkbool is32bit,CharItem **ppchar)
{
	uint8_t *ps;
	int len,i,charsize;
	CharItem *pi,*pitop;
	mList *list = &p->list_char;
	mListItem *pibottom;

	ps = *pptext;

	charsize = (is32bit)? 4: 2;

	//文字数

	len = *((uint16_t *)ps);
	ps += 2;

	//次のソース位置

	*pptext = ps + charsize * len;

	//現在の終端位置

	pibottom = list->bottom;

	//リストに1文字ずつ追加
	//(縦中横の場合は一つのデータにまとめられる)

	for(i = len; i > 0; i--, ps += charsize)
	{
		pi = (CharItem *)mListAppendNew(list, sizeof(CharItem));
		if(pi)
		{
			if(is32bit)
				pi->code = *((uint32_t *)ps);
			else
				pi->code = *((uint16_t *)ps);

			//現在の注記状態を適用

			_set_char_state(p, pi);
		}
	}

	//追加された先頭位置

	pitop = (pibottom)? (CharItem *)pibottom->next: (CharItem *)list->top;

	//調整

	len = _text_adjust(p, pitop);

	//先頭位置

	if(ppchar) *ppchar = pitop;

	return len;
}

/** ルビ付き文字列を追加
 *
 * [uint16] 本文文字数, UTF-32文字列, [uint16] ルビ文字数, UTF-32文字列 */

static void _append_ruby(LayoutWork *p,uint8_t **pptext)
{
	uint8_t *ps;
	CharItem *pitop;
	RubyItem *pi;
	int clen,rlen;

	//本文文字列を追加

	clen = _append_text(p, pptext, TRUE, &pitop);

	//ルビアイテムを追加

	ps = *pptext;

	rlen = *((uint16_t *)ps);
	ps += 2;

	pi = (RubyItem *)mListAppendNew(&p->list_ruby, sizeof(RubyItem));
	if(pi)
	{
		pi->char_top = pitop;
		pi->rubytxt = (uint32_t *)ps;
		pi->rubylen = rlen;
		pi->charlen = clen;
		pi->ruby_h = FontGetTextWidth_vert(p->font_ruby, (uint32_t *)ps, rlen, FONT_DRAW_F_RUBY);
	}

	//次のデータ位置

	*pptext = ps + (rlen << 2);
}

/** 内部データから、1行分のデータを取得
 *
 * list_char/list_ruby のリストに文字データ、
 * 注記の情報は、行/ブロック状態にセット。 */

static void _get_line(LayoutWork *p,int *cmdret)
{
	uint8_t *ps = p->text;
	int floop = TRUE,type,cmd;

	*cmdret = -1;

	while(floop)
	{
		//データタイプ (1byte)
		type = *(ps++);

		switch(type)
		{
			//行番号の情報
			case DATATYPE_LINEINFO:
				p->curlineno = *((uint32_t *)ps);
				ps += 4;
				break;
			//通常文字列
			case DATATYPE_NORMAL_TEXT_16:
			case DATATYPE_NORMAL_TEXT_32:
				_append_text(p, &ps, (type == DATATYPE_NORMAL_TEXT_32), NULL);
				break;
			//ルビ付き文字列
			case DATATYPE_RUBY_TEXT:
				_append_ruby(p, &ps);
				break;
			//改行
			case DATATYPE_ENTER:
				floop = FALSE;
				break;
			//値なしコマンド
			case DATATYPE_COMMAND:
				cmd = _proc_command_noval(p, *(ps++));

				//ページが変わるコマンドの場合
				if(cmd != -1)
				{
					floop = FALSE;

					if(p->list_char.top)
					{
						//本文文字が残っている場合は先に処理させるため、
						//位置を戻す
						ps -= 2;
					}
					else
						*cmdret = cmd;
				}
				break;
			//値付きコマンド
			case DATATYPE_COMMAND_VAL:
				_proc_command_val(p, &ps);
				break;
			//挿絵
			case DATATYPE_PICTURE:
				floop = FALSE;

				if(p->list_char.top)
					//本文文字が残っている場合、先に処理
					ps--;
				else
					_proc_picture(p, &ps);
				break;
			//終了
			case DATATYPE_END:
				ps--;
				floop = FALSE;
				break;
		}
	}

	p->text = ps;
}


//=================================
// 1行分取得後の調整
//=================================


/** 半角文字の並び方を自動判定 */

static void _set_chartype_auto(LayoutWork *p)
{
	CharItem *pi,*next,*pitop,*pi2;
	int i,cnt;

	for(pi = (CharItem *)p->list_char.top; pi; pi = next)
	{
		next = (CharItem *)pi->i.next;
	
		//すでに文字種が指定されているか、半角文字以外なら、そのまま
		
		if(pi->chartype != CHARITEM_TYPE_NORMAL || pi->code >= 128)
			continue;
	
		//------- 半角文字の自動判定
	
		//縦中横の対象文字が連続している数

		for(cnt = 0, pi2 = pi;
			cnt < 4 && pi2 && layout_ischar_tatetyuyoko(pi2->code);
			cnt++, pi2 = (CharItem *)pi2->i.next);

		//

		if(cnt >= 1 && cnt <= 3)
		{
			//[縦中横]
			// 3文字以内の場合は自動で縦中横にする。
			// 最初の文字だけ残して1文字にまとめる。
		
			pitop = pi;

			pitop->chartype = CHARITEM_TYPE_HORZ;
			pitop->horzcnt = cnt;
			pitop->horzchar[0] = (uint8_t)pitop->code;

			for(pi = next, i = 1; pi && i < cnt; pi = pi2, i++)
			{
				pi2 = (CharItem *)pi->i.next;

				pitop->horzchar[i] = (uint8_t)pi->code;

				mListDelete(&p->list_char, MLISTITEM(pi));
			}
		}
		else
		{
			//[横組み]
			// 4文字以上連続しているなら横組み。
			// 半角文字以外が出るまで続ける。

			for(; pi && pi->code < 127; pi = (CharItem *)pi->i.next)
				pi->chartype = CHARITEM_TYPE_ROTATE;
		}

		next = pi;
	}
}

/** 本文文字の文字幅/高さをセット */

static void _set_char_size(LayoutWork *p)
{
	CharItem *pi;
	mFont *font[2];
	uint8_t *hbuf;
	int fonth[2],n,f,h;
	uint32_t code;

	font[0] = p->font_main;
	font[1] = p->font_bold;

	fonth[0] = p->fontmain_h;
	fonth[1] = p->fontbold_h;

	for(pi = (CharItem *)p->list_char.top; pi; pi = (CharItem *)pi->i.next)
	{
		code = pi->code;
		
		//[0] 通常 [1] 太字
		n = ((pi->flags & CHARITEM_F_BOLD) != 0);

		switch(pi->chartype)
		{
			//通常縦書き
			case CHARITEM_TYPE_NORMAL:
				if(n || code > 0xffff)
					//太字 or コードが16bitを超える場合は常に計算
					pi->height = FontGetTextWidth_vert(font[n], &code, 1, 0);
				else
				{
					//通常で16bit以内の場合、高速化のため、
					//全角高さと同じ場合はフラグを ON にする。
					//(ほとんどの文字は 16bit 範囲内にあるため)
					
					hbuf = p->buf_hflags + (code >> 3);
					f = 1 << (code & 7);

					if(*hbuf & f)
						//前回取得し、全角高さと同じ
						pi->height = fonth[0];
					else
					{
						h = FontGetTextWidth_vert(font[0], &code, 1, 0);

						if(h == fonth[0])
							*hbuf |= f;

						pi->height = h;
					}
				}
				break;
			//横組み (90度回転)
			case CHARITEM_TYPE_ROTATE:
				pi->height = FontGetTextWidth_vert(font[n], &code, 1, FONT_DRAW_F_ROTATE);
				break;
			//縦中横
			case CHARITEM_TYPE_HORZ:
				pi->width = FontGetTextWidth_horz(font[n], pi->horzchar, pi->horzcnt, 0);
				pi->height = fonth[n];
				break;
		}
	}
}

/* 親文字列高さ取得 (折り返しなしでの状態) */

static int _get_char_height(LayoutWork *p,CharItem *pi,int len)
{
	int h = 0,i;

	for(i = len; i; i--, pi = (CharItem *)pi->i.next)
		h += pi->height;

	return h + (len - 1) * p->stdef->char_space;
}

/** ルビと親文字列の調整
 *
 * 親文字列よりルビの方が大きい場合、ルビの親文字列に余白を追加 */

static void _set_ruby_parent(LayoutWork *p)
{
	RubyItem *pi;
	CharItem *pic;
	int charh,diff,diffmax,clen,sp1,sp2,i;

	diffmax = p->fontmain_h;

	for(pi = (RubyItem *)p->list_ruby.top; pi; pi = (RubyItem *)pi->i.next)
	{
		//親文字列の高さ

		charh = _get_char_height(p, pi->char_top, pi->charlen);

		//
		
		diff = pi->ruby_h - charh;
	
		if(diff <= diffmax)
		{
			pi->char_h = charh;
			continue;
		}

		//------- 

		pi->char_h = pi->ruby_h;

		//親文字列の高さに余白を追加

		clen = pi->charlen;

		if(clen == 1)
		{
			//親文字列が1文字の場合

			pic = pi->char_top;
			
			pic->height = pi->ruby_h;
			pic->padding = diff / 2;
		}
		else
		{
			//親文字列が2文字以上の場合
		
			sp1 = diff / (clen * 2);
			sp2 = diff / clen;

			for(pic = pi->char_top, i = 0; i < clen; i++, pic = (CharItem *)pic->i.next)
			{
				if(i == 0)
				{
					//先頭文字
					pic->height += sp1;
					pic->padding = sp1;
					diff -= sp1;
				}
				else if(i == clen - 1)
				{
					//終端文字
					pic->height += diff;
					pic->padding = sp2;
				}
				else
				{
					//間
					pic->height += sp2;
					pic->padding = sp2;
					diff -= sp2;
				}
			}
		}
	}
}


//=============================
// main
//=============================


/** 1行分の文字データ取得
 *
 * cmd: 改ページなどページ関連のコマンドが含まれる場合、そのコマンド番号。なければ -1。
 * return: FALSE でデータの終端 */

mlkbool layout_getline(LayoutWork *p,int *cmd)
{
	if(*(p->text) == DATATYPE_END) return FALSE;

	//行状態クリア

	mMemset0(&p->linestate, sizeof(LineState));

	//1行データ取得

	_get_line(p, cmd);

	//文字タイプ自動判定

	_set_chartype_auto(p);

	//本文pxサイズセット

	_set_char_size(p);

	//ルビと親文字列の調整

	_set_ruby_parent(p);

	return TRUE;
}

