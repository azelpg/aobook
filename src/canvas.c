/*$
aobook
Copyright (c) 2014-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * キャンバスウィジェット
 *****************************************/

#include "mlk_gui.h"
#include "mlk_widget_def.h"
#include "mlk_widget.h"
#include "mlk_event.h"
#include "mlk_pixbuf.h"

#include "def_global.h"
#include "def_style.h"
#include "func_widget.h"
#include "layout.h"


/* ボタン/ホイール操作 */

static void _event_pointer(mEvent *ev)
{
	int btt = -1;

	//ボタン番号 取得

	if(ev->type == MEVENT_SCROLL)
	{
		//ホイール
		
		if(ev->scroll.vert_step < 0)
			btt = BUTTONACT_BTT_SCROLL_UP;
		else if(ev->scroll.vert_step > 0)
			btt = BUTTONACT_BTT_SCROLL_DOWN;
		else if(ev->scroll.horz_step < 0)
			btt = BUTTONACT_BTT_SCROLL_LEFT;
		else if(ev->scroll.horz_step > 0)
			btt = BUTTONACT_BTT_SCROLL_RIGHT;
	}
	else if(ev->pt.act == MEVENT_POINTER_ACT_PRESS
		|| ev->pt.act == MEVENT_POINTER_ACT_DBLCLK)
	{
		//ポインタイベント
		
		if(ev->pt.btt == MLK_BTT_LEFT)
			btt = BUTTONACT_BTT_LEFT;
		else if(ev->pt.btt == MLK_BTT_RIGHT)
			btt = BUTTONACT_BTT_RIGHT;
	}

	if(btt == -1) return;

	//コマンド

	switch(GDAT->bttact[btt])
	{
		case BUTTONACT_ACT_PAGE_NEXT:
			MainWindow_movePage(GDAT->mainwin, PAGENO_NEXT);
			break;
		case BUTTONACT_ACT_PAGE_PREV:
			MainWindow_movePage(GDAT->mainwin, PAGENO_PREV);
			break;
	}
}

/* イベントハンドラ */

static int _event_handle(mWidget *wg,mEvent *ev)
{
	switch(ev->type)
	{
		case MEVENT_POINTER:
		case MEVENT_SCROLL:
			_event_pointer(ev);
			break;
	
		default:
			return FALSE;
	}

	return TRUE;
}

/* 描画ハンドラ */

static void _draw_handle(mWidget *wg,mPixbuf *pixbuf)
{
	GlobalData *g = GDAT;

	//レイアウトスレッド中にウィンドウサイズの変更などが行われたときは無効

	if(g->is_in_thread) return;

	//背景

	if(!g->style->img_bkgnd)
		mPixbufFillBox(pixbuf, 0, 0, wg->w, wg->h, mRGBtoPix(g->style->b.col_bkgnd));
	else
	{
		if(g->style->b.flags & STYLE_F_BKGND_TILE)
			mPixbufBltFill_imagebuf(pixbuf, 0, 0, wg->w, wg->h, g->style->img_bkgnd);
		else
			mPixbufBltScale_oversamp_imagebuf(pixbuf, 0, 0, wg->w, wg->h, g->style->img_bkgnd, 5);
	}

	//ページ描画

	LayoutDrawPage(g->layout, pixbuf, g->curpage);
}

/** キャンバスウィジェット作成 */

mWidget *createCanvasWidget(mWidget *parent)
{
	mWidget *wg;

	wg = mWidgetNew(parent, 0);

	wg->flayout = MLF_EXPAND_WH;
	wg->fevent |= MWIDGET_EVENT_POINTER | MWIDGET_EVENT_SCROLL;
	wg->event = _event_handle;
	wg->draw = _draw_handle;

	return wg;
}

