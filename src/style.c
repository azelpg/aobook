/*$
aobook
Copyright (c) 2014-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/********************************
 * スタイル関数
 ********************************/

#include <stdio.h>
#include <string.h>

#include "mlk_gui.h"
#include "mlk_str.h"
#include "mlk_unicode.h"
#include "mlk_font.h"
#include "mlk_imagebuf.h"
#include "mlk_loadimage.h"
#include "mlk_iniread.h"
#include "mlk_iniwrite.h"

#include "def_global.h"
#include "def_style.h"
#include "style.h"
#include "font.h"


//--------------------

#define STYLE_CONFIGNAME "styles.conf"

static const uint32_t *g_u32_nohead = U"）〕］｝〉》」』】〙〗、。，．・：；!?！？‼⁇⁈⁉ヽヾゝゞ々〻゜’”゛〟ーぁぃぅぇぉっゃゅょゎァィゥェォッャュョヮヵヶ",
	*g_u32_nobottom = U"［｛〔〈《「『【〘〖〝‘“",
	*g_u32_hanging = U"、。，．",
	*g_u32_nosep = U"…‥";

//--------------------



/* 画像読み込み */

static mImageBuf *_load_image(mStr *str)
{
	mLoadImageType tp;
	mLoadImageOpen open;
	mFuncLoadImageCheck funcs[] = {
		mLoadImage_checkPNG, mLoadImage_checkJPEG, mLoadImage_checkBMP, 0
	};

	if(mStrIsEmpty(str)) return NULL;

	open.type = MLOADIMAGE_OPEN_FILENAME;
	open.filename = str->buf;

	if(mLoadImage_checkFormat(&tp, &open, funcs, -1))
		return NULL;

	return mImageBuf_loadImage(&open, &tp, 24, -4);
}

/* 文字列挙: 変更されたか
 *
 * return: 0 で同じ */

static int _ischange_chars(uint32_t *p1,uint32_t *p2)
{
	if(p1 == STYLE_CHARS_DEFAULT || p2 == STYLE_CHARS_DEFAULT)
		return (p1 != p2);
	else
		return mUTF32Compare(p1, p2);
}

/* レイアウト時用の文字列挙を作成
 * (Unicode の小さい順に並べる) */

static uint32_t *_create_layout_chars(const uint32_t *src,const uint32_t *def,int *plen)
{
	uint32_t *buf,*pins,*ptmp,c;
	int len,dlen;

	if(!src) return NULL;

	if(src == (const uint32_t *)1)
		src = def;

	//

	len = mUTF32GetLen(src);

	buf = (uint32_t *)mMalloc((len + 1) * 4);
	if(!buf) return NULL;

	*buf = 0;
	dlen = 0;

	//

	while(1)
	{
		c = *(src++);
		if(!c) break;

		//挿入位置

		for(pins = buf; *pins && c > *pins; pins++);

		//挿入位置以降をずらす

		if(*pins)
		{
			for(ptmp = buf + dlen; ptmp > pins; ptmp--)
				*ptmp = ptmp[-1];
		}

		//セット

		*pins = c;
		
		dlen++;
		buf[dlen] = 0;
	}

	*plen = len;

	return buf;
}


//*********************************
// StyleDef
//*********************************


/** 文字列挙データを解放 */

void StyleDef_freeChars(StyleDef *p)
{
	uint32_t **ptr[5],**pp;
	int i;

	StyleDef_getCharsArray(p, ptr);

	for(i = 0; i < 5; i++)
	{
		pp = ptr[i];
	
		if(*pp != STYLE_CHARS_DEFAULT)
			mFree(*pp);

		*pp = NULL;
	}
}

/** 解放 */

void StyleDef_free(StyleDef *p)
{
	StyleDef_freeChars(p);

	mStrFree(&p->str_stylename);
	mStrFree(&p->str_fontmain);
	mStrFree(&p->str_fontruby);
	mStrFree(&p->str_fontbold);
	mStrFree(&p->str_fontinfo);
	mStrFree(&p->str_bkgndimg);
}

/** 文字列挙のポインタの配列をセット */

void StyleDef_getCharsArray(StyleDef *p,uint32_t ***dst)
{
	dst[0] = &p->u32_nohead;
	dst[1] = &p->u32_nobottom;
	dst[2] = &p->u32_hanging;
	dst[3] = &p->u32_nosep;
	dst[4] = &p->u32_replace;
}

/** デフォルト設定をセット
 *
 * name: スタイル名 */

void StyleDef_setDefault(StyleDef *p,const char *name)
{
	p->chars = 32;
	p->lines = 15;
	p->char_space = 0;
	p->line_space = 80;
	p->page_space = 30;
	p->flags = STYLE_F_HANGING | STYLE_F_ENABLE_PICTURE;

	p->pages = 1;
	p->dakuten_type = STYLE_DAKUTEN_COMBINE_HORZ;

	p->margin.x1 = p->margin.x2 = p->margin.y1 = p->margin.y2 = 30;

	p->col_text = 0;
	p->col_ruby = 0x300909;
	p->col_info = 0x244D26;
	p->col_bkgnd = 0xfaf2e3;

	p->u32_nohead = STYLE_CHARS_DEFAULT;
	p->u32_nobottom = STYLE_CHARS_DEFAULT;
	p->u32_hanging = STYLE_CHARS_DEFAULT;
	p->u32_nosep = STYLE_CHARS_DEFAULT;
	p->u32_replace = NULL;

	mStrSetText(&p->str_stylename, name);
	mStrSetText(&p->str_fontmain, "size=13");
	mStrSetText(&p->str_fontruby, "size=7");
	mStrSetText(&p->str_fontbold, "size=13");
	mStrSetText(&p->str_fontinfo, "size=9");
}

/** 設定をコピー */

void StyleDef_copy(StyleDef *dst,StyleDef *src)
{
	uint32_t **ptr_src[5],**ptr_dst[5],**ppsrc;
	int i;

	//値をコピー

	dst->chars = src->chars;
	dst->lines = src->lines;
	dst->char_space = src->char_space;
	dst->line_space = src->line_space;
	dst->page_space = src->page_space;
	dst->flags = src->flags;

	dst->pages = src->pages;
	dst->dakuten_type = src->dakuten_type;

	dst->margin = src->margin;

	dst->col_text = src->col_text;
	dst->col_ruby = src->col_ruby;
	dst->col_info = src->col_info;
	dst->col_bkgnd = src->col_bkgnd;

	//文字列挙

	StyleDef_freeChars(dst);

	StyleDef_getCharsArray(dst, ptr_dst);
	StyleDef_getCharsArray(src, ptr_src);

	for(i = 0; i < 5; i++)
	{
		ppsrc = ptr_src[i];

		if(*ppsrc && *ppsrc != STYLE_CHARS_DEFAULT)
			*(ptr_dst[i]) = mUTF32Dup(*ppsrc);
		else
			*(ptr_dst[i]) = *ppsrc;
	}

	//mStr

	mStrCopy(&dst->str_stylename, &src->str_stylename);
	mStrCopy(&dst->str_fontmain, &src->str_fontmain);
	mStrCopy(&dst->str_fontruby, &src->str_fontruby);
	mStrCopy(&dst->str_fontbold, &src->str_fontbold);
	mStrCopy(&dst->str_fontinfo, &src->str_fontinfo);
	mStrCopy(&dst->str_bkgndimg, &src->str_bkgndimg);
}

/** 文字列挙の文字列をセット (UTF-8 から)
 *
 * text: 1 でデフォルト */

void StyleDef_setCharsText(uint32_t **ppdst,const char *text)
{
	//解放
	
	if(*ppdst != STYLE_CHARS_DEFAULT)
		mFree(*ppdst);

	//セット

	if(text == (const char *)1)
		*ppdst = STYLE_CHARS_DEFAULT;
	else if(!text || !(*text))
		*ppdst = NULL;
	else
		*ppdst = mUTF8toUTF32_alloc(text, -1, NULL);
}

/** 文字列挙のデフォルト文字列を取得
 *
 * no: 0〜3 */

const uint32_t *StyleDef_getCharsDefault(int no)
{
	switch(no)
	{
		case 0: return g_u32_nohead;
		case 1: return g_u32_nobottom;
		case 2: return g_u32_hanging;
		default: return g_u32_nosep;
	}
}

/** レイアウト時用の文字列挙作成 */

uint32_t *StyleDef_createLayoutChars_nohead(StyleDef *p,int *plen)
{
	return _create_layout_chars(p->u32_nohead, g_u32_nohead, plen);
}

uint32_t *StyleDef_createLayoutChars_nobottom(StyleDef *p,int *plen)
{
	return _create_layout_chars(p->u32_nobottom, g_u32_nobottom, plen);
}

uint32_t *StyleDef_createLayoutChars_hanging(StyleDef *p,int *plen)
{
	return _create_layout_chars(p->u32_hanging, g_u32_hanging, plen);
}

uint32_t *StyleDef_createLayoutChars_nosep(StyleDef *p,int *plen)
{
	return _create_layout_chars(p->u32_nosep, g_u32_nosep, plen);
}

/** レイアウト時用の置き換え文字列作成
 *
 * 半角空白は除外。対になっていないものは除去。
 * 置き換え元の小さい順に並べる。
 *
 * len: 2文字1組での数 */

uint32_t *StyleDef_createLayoutChars_replace(StyleDef *p,int *plen)
{
	uint32_t *buf,*src,*pins,*ptmp,c,csrc,cdst;
	int len;

	if(!p->u32_replace) return NULL;

	src = p->u32_replace;

	len = mUTF32GetLen(src);

	buf = (uint32_t *)mMalloc((len + 1) * 4);
	if(!buf) return NULL;

	//

	*buf = 0;
	len = 0;

	while(1)
	{
		//置き換え元と置き換え先の文字
		
		csrc = cdst = 0;

		while(*src)
		{
			c = *(src++);
		
			if(c == ' ')
				continue;
			else if(!csrc)
				csrc = c;
			else if(!cdst)
			{
				cdst = c;
				break;
			}
		}

		if(!csrc || !cdst) break;

		//挿入位置

		for(pins = buf; *pins && csrc > *pins; pins += 2);

		//挿入位置以降をずらす

		if(*pins)
		{
			for(ptmp = buf + len * 2; ptmp > pins; ptmp -= 2)
			{
				ptmp[0] = ptmp[-2];
				ptmp[1] = ptmp[-1];
			}
		}

		//セット

		pins[0] = csrc;
		pins[1] = cdst;
		
		len++;
		buf[len * 2] = 0;
	}

	*plen = len;

	return buf;
}


//***********************************
// StyleWork (現在のスタイルデータ)
//***********************************


/** フォントの解放 */

void StyleWork_freeFont(StyleWork *p)
{
	mFontFree(p->font_main);
	mFontFree(p->font_ruby);
	mFontFree(p->font_bold);
	mFontFree(p->font_info);

	p->font_main = NULL;
	p->font_ruby = NULL;
	p->font_bold = NULL;
	p->font_info = NULL;
}

/** データをすべて解放 (p は残す) */

void StyleWork_freeFull(StyleWork *p)
{
	if(p)
	{
		StyleDef_free(&p->b);

		StyleWork_freeFont(p);

		mImageBuf_free(p->img_bkgnd);
		p->img_bkgnd = NULL;
	}
}

/** p 自体もすべて解放 (終了時) */

void StyleWork_free(StyleWork *p)
{
	StyleWork_freeFull(p);
	mFree(p);
}

/** フォントを作成 */

void StyleWork_createFont(StyleWork *p)
{
	StyleWork_freeFont(p);

	p->font_main = FontCreate(p->b.str_fontmain.buf, 0);
	p->font_ruby = FontCreate(p->b.str_fontruby.buf, FONT_CREATE_F_RUBY);
	p->font_bold = FontCreate(p->b.str_fontbold.buf, 0);

	//ページ情報用は、スレッド中に使わないため、GUI 用を使う
	p->font_info = mFontCreate_text(mGuiGetFontSystem(), p->b.str_fontinfo.buf);

	//圏点用フォントのサイズセット (ルビと同じ)

	mFontSetSize_fromText(GDAT->font_kenten, p->b.str_fontruby.buf);
}

/** 画面サイズ取得 */

void StyleWork_getScreenSize(StyleWork *p,mSize *dst)
{
	int fonth,linesp;

	fonth = mFontGetVertHeight(p->font_main);
	linesp = (int)(fonth * (p->b.line_space / 100.0) + 0.5);

	dst->w = (fonth + linesp) * p->b.lines * p->b.pages
		+ p->b.page_space * (p->b.pages - 1)
		+ p->b.margin.x1 + p->b.margin.x2;
	
	dst->h = (fonth + p->b.char_space) * p->b.chars - p->b.char_space
		+ p->b.margin.y1 + p->b.margin.y2;
}

/** 指定スタイルを読み込み */

void StyleWork_readStyle(StyleWork *p,const char *name)
{
	StyleWork_freeFull(p);

	//スタイル読み込み

	StyleConf_readStyle(&p->b, name);

	//各作成

	StyleWork_createFont(p);

	p->img_bkgnd = _load_image(&p->b.str_bkgndimg);
}

/** 現在のスタイルに値を適用
 *
 * return: [0] 更新なし [1] 画面のみ更新 [2] 再レイアウト */

int StyleWork_apply(StyleWork *p,StyleDef *ps)
{
	mlkbool change_font,change_chars,change_bkgndimg;
	uint32_t f1,f2;
	int ret;

	//フォントが変わったか

	change_font = (!mStrCompareEq(&ps->str_fontmain, p->b.str_fontmain.buf)
		|| !mStrCompareEq(&ps->str_fontruby, p->b.str_fontruby.buf)
		|| !mStrCompareEq(&ps->str_fontbold, p->b.str_fontbold.buf)
		|| !mStrCompareEq(&ps->str_fontinfo, p->b.str_fontinfo.buf));

	//文字列挙が変わったか

	change_chars = (_ischange_chars(ps->u32_nohead, p->b.u32_nohead)
		|| _ischange_chars(ps->u32_nobottom, p->b.u32_nobottom)
		|| _ischange_chars(ps->u32_hanging, p->b.u32_hanging)
		|| _ischange_chars(ps->u32_nosep, p->b.u32_nosep)
		|| _ischange_chars(ps->u32_replace, p->b.u32_replace));

	//背景画像が変わったか

	change_bkgndimg = !mStrPathCompareEq(&ps->str_bkgndimg, p->b.str_bkgndimg.buf);

	//更新判定

	f1 = STYLE_F_HANGING | STYLE_F_ENABLE_PICTURE;
	f2 = STYLE_F_BKGND_TILE | STYLE_F_DASH_TO_LINE | STYLE_F_REPLACE_PRINT;

	if(ps->pages != p->b.pages
		|| ps->chars != p->b.chars
		|| ps->lines != p->b.lines
		|| (ps->flags & f1) != (p->b.flags & f1)
		|| change_font
		|| change_chars)
		//再レイアウト
		ret = 2;

	else if(ps->char_space != p->b.char_space
		|| ps->line_space != p->b.line_space
		|| ps->page_space != p->b.page_space
		|| (ps->flags & f2) != (p->b.flags & f2)
		|| ps->dakuten_type != p->b.dakuten_type
		|| ps->margin.x1 != p->b.margin.x1
		|| ps->margin.y1 != p->b.margin.y1
		|| ps->margin.x2 != p->b.margin.x2
		|| ps->margin.y2 != p->b.margin.y2
		|| ps->col_text != p->b.col_text
		|| ps->col_ruby != p->b.col_ruby
		|| ps->col_info != p->b.col_info
		|| ps->col_bkgnd != p->b.col_bkgnd
		|| change_bkgndimg)
		//画面更新のみ
		ret = 1;
	else
		ret = 0;

	//データコピー

	StyleDef_copy(&p->b, ps);

	//フォント再作成

	if(change_font)
		StyleWork_createFont(p);

	//背景画像読み込み

	if(change_bkgndimg)
	{
		mImageBuf_free(p->img_bkgnd);

		p->img_bkgnd = _load_image(&ps->str_bkgndimg);
	}

	return ret;
}


//***********************************
// 設定ファイル
//***********************************


/* 設定ファイルから文字列挙読み込み */

static void _readconfig_chars(mIniRead *ini,const char *key,uint32_t **ppdst)
{
	const char *txt;

	txt = mIniRead_getText(ini, key, (const char *)1);

	if(txt == (const char *)1)
		//キーがなければデフォルト
		*ppdst = STYLE_CHARS_DEFAULT;
	else if(!(*txt))
		//空文字列
		*ppdst = NULL;
	else
		*ppdst = mUTF8toUTF32_alloc(txt, -1, NULL);
}

/* 文字列挙を書き込み */

static void _writeconfig_chars(FILE *fp,const char *key,uint32_t *str)
{
	if(str != STYLE_CHARS_DEFAULT)
	{
		if(!str)
			mIniWrite_putText(fp, key, NULL);
		else
		{
			char *buf;

			buf = mUTF32toUTF8_alloc(str, -1, NULL);

			mIniWrite_putText(fp, key, buf);

			mFree(buf);
		}
	}
}


/** スタイルの設定ファイルを開く
 *
 * return: スタイルの数 */

int StyleConf_openRead(mIniRead **dst)
{
	mIniRead *ini;
	int num;

	mIniRead_loadFile_join(&ini, mGuiGetPath_config_text(), STYLE_CONFIGNAME);

	//

	mIniRead_setGroup(ini, "styles");

	if(mIniRead_getInt(ini, "ver", 0) != 1)
		mIniRead_setEmpty(ini);

	num = mIniRead_getInt(ini, "num", 0);

	//

	*dst = ini;

	return num;
}

/** 設定ファイルから指定スタイルを読み込み */

void StyleConf_readStyle(StyleDef *p,const char *name)
{
	mIniRead *ini;
	int i,num;

	num = StyleConf_openRead(&ini);

	//スタイル名から検索して読み込み

	for(i = 0; i < num; i++)
	{
		if(mIniRead_setGroupNo(ini, i)
			&& mIniRead_compareText(ini, "name", name, FALSE))
		{
			StyleConf_readDefine(ini, p);
			break;
		}
	}

	//見つからなければ、デフォルト

	if(i == num)
		StyleDef_setDefault(p, "default");

	mIniRead_end(ini);
}

/** 設定ファイルから値読み込み
 *
 * (グループはセット済み、StyleDef は空状態) */

void StyleConf_readDefine(mIniRead *ini,StyleDef *p)
{
	mIniRead_getTextStr(ini, "name", &p->str_stylename, NULL);

	p->pages = mIniRead_getInt(ini, "pages", 1);
	p->chars = mIniRead_getInt(ini, "chars", 32);
	p->lines = mIniRead_getInt(ini, "lines", 15);
	p->char_space = mIniRead_getInt(ini, "charspace", 0);
	p->line_space = mIniRead_getInt(ini, "linespace", 80);
	p->page_space = mIniRead_getInt(ini, "pagespace", 30);
	
	p->flags = mIniRead_getInt(ini, "flags", STYLE_F_HANGING | STYLE_F_ENABLE_PICTURE);

	p->dakuten_type = mIniRead_getInt(ini, "dakuten", STYLE_DAKUTEN_COMBINE_HORZ);

	p->margin.x1 = mIniRead_getInt(ini, "mgleft", 30);
	p->margin.x2 = mIniRead_getInt(ini, "mgright", 30);
	p->margin.y1 = mIniRead_getInt(ini, "mgtop", 30);
	p->margin.y2 = mIniRead_getInt(ini, "mgbottom", 30);

	p->col_text = mIniRead_getHex(ini, "coltext", 0);
	p->col_ruby = mIniRead_getHex(ini, "colruby", 0x400000);
	p->col_info = mIniRead_getHex(ini, "colinfo", MLK_RGB(0,114,0));
	p->col_bkgnd = mIniRead_getHex(ini, "colbkgnd", 0xfaf2e3);

	mIniRead_getTextStr(ini, "fontmain", &p->str_fontmain, "size=13");
	mIniRead_getTextStr(ini, "fontruby", &p->str_fontruby, "size=7");
	mIniRead_getTextStr(ini, "fontbold", &p->str_fontbold, "size=13");
	mIniRead_getTextStr(ini, "fontinfo", &p->str_fontinfo, "size=9");
	mIniRead_getTextStr(ini, "bkgndfile", &p->str_bkgndimg, NULL);

	_readconfig_chars(ini, "nohead", &p->u32_nohead);
	_readconfig_chars(ini, "nobottom", &p->u32_nobottom);
	_readconfig_chars(ini, "hanging", &p->u32_hanging);
	_readconfig_chars(ini, "nosep", &p->u32_nosep);
	_readconfig_chars(ini, "replace", &p->u32_replace);

	//文字置換はデフォルトなし

	if(p->u32_replace == STYLE_CHARS_DEFAULT)
		p->u32_replace = NULL;
}

/** 設定ファイルを書き込み用で開く
 *
 * num: スタイルの数 */

void *StyleConf_openWrite(int num)
{
	FILE *fp;

	fp = mIniWrite_openFile_join(mGuiGetPath_config_text(), STYLE_CONFIGNAME);
	if(!fp) return NULL;

	mIniWrite_putGroup(fp, "styles");

	mIniWrite_putInt(fp, "ver", 1);
	mIniWrite_putInt(fp, "num", num);

	return (void *)fp;
}

/** スタイルを書き込み */

void StyleConf_writeDefine(void *ptr,int no,StyleDef *p)
{
	FILE *fp = (FILE *)ptr;

	mIniWrite_putGroupInt(fp, no);

	mIniWrite_putStr(fp, "name", &p->str_stylename);

	mIniWrite_putInt(fp, "pages", p->pages);
	mIniWrite_putInt(fp, "chars", p->chars);
	mIniWrite_putInt(fp, "lines", p->lines);
	mIniWrite_putInt(fp, "charspace", p->char_space);
	mIniWrite_putInt(fp, "linespace", p->line_space);
	mIniWrite_putInt(fp, "pagespace", p->page_space);
	mIniWrite_putInt(fp, "flags", p->flags);

	mIniWrite_putInt(fp, "dakuten", p->dakuten_type);
	
	mIniWrite_putInt(fp, "mgleft", p->margin.x1);
	mIniWrite_putInt(fp, "mgright", p->margin.x2);
	mIniWrite_putInt(fp, "mgtop", p->margin.y1);
	mIniWrite_putInt(fp, "mgbottom", p->margin.y2);

	mIniWrite_putHex(fp, "coltext", p->col_text);
	mIniWrite_putHex(fp, "colruby", p->col_ruby);
	mIniWrite_putHex(fp, "colinfo", p->col_info);
	mIniWrite_putHex(fp, "colbkgnd", p->col_bkgnd);

	mIniWrite_putStr(fp, "fontmain", &p->str_fontmain);
	mIniWrite_putStr(fp, "fontruby", &p->str_fontruby);
	mIniWrite_putStr(fp, "fontbold", &p->str_fontbold);
	mIniWrite_putStr(fp, "fontinfo", &p->str_fontinfo);
	mIniWrite_putStr(fp, "bkgndfile", &p->str_bkgndimg);

	_writeconfig_chars(fp, "nohead", p->u32_nohead);
	_writeconfig_chars(fp, "nobottom", p->u32_nobottom);
	_writeconfig_chars(fp, "hanging", p->u32_hanging);
	_writeconfig_chars(fp, "nosep", p->u32_nosep);
	_writeconfig_chars(fp, "replace", p->u32_replace);
}
