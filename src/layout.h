/*$
aobook
Copyright (c) 2014-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/********************************
 * レイアウト定義
 ********************************/

typedef struct _mPopupProgress mPopupProgress;

/* 見出しのリストアイテム */

typedef struct
{
	mListItem i;

	int type,		//0:大 1:中 2:小
		pageno;		//ページ番号
	char text[1];	//タイトル文字列
}TitleItem;

/* レイアウト情報 */

typedef struct _LayoutDat
{
	int page_num,		//ページ数
		title_num[3];	//各見出しの個数
	mList list_page,	//ページのリスト
		list_title;		//見出しのリスト
	uint8_t	*buf_hflags;	//本文フォントの各文字(Unicode下位16bit分)の高さフラグ
							//ON=一度取得し、全角高さと同じ。OFF=高さが異なる、または横組みなど。
}LayoutDat;


LayoutDat *LayoutAlloc(void);
void LayoutFreeData(LayoutDat *p);
void LayoutFree(LayoutDat *p);

void LayoutRunFirst(LayoutDat *p,mPopupProgress *prog);
void LayoutDrawPage(LayoutDat *info,mPixbuf *img,PageInfoItem *page);

PageInfoItem *LayoutGetPage_homeEnd(LayoutDat *p,mlkbool end);
PageInfoItem *LayoutGetPage_pageno(LayoutDat *p,int page);
PageInfoItem *LayoutGetPage_lineno(LayoutDat *p,int line,mlkbool wrap_top);
PageInfoItem *LayoutGetPage_move(LayoutDat *p,PageInfoItem *pi,int dir);

int LayoutGetPageNo(PageInfoItem *pi);
int LayoutGetPageLineNo(PageInfoItem *pi);

