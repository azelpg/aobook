/*$
aobook
Copyright (c) 2014-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/********************************
 * スタイル関連関数
 ********************************/

/* StyleDef */

void StyleDef_free(StyleDef *p);
void StyleDef_freeChars(StyleDef *p);

void StyleDef_getCharsArray(StyleDef *p,uint32_t ***dst);
void StyleDef_setDefault(StyleDef *p,const char *name);
void StyleDef_copy(StyleDef *dst,StyleDef *src);
void StyleDef_setCharsText(uint32_t **ppdst,const char *text);

const uint32_t *StyleDef_getCharsDefault(int no);

uint32_t *StyleDef_createLayoutChars_nohead(StyleDef *p,int *plen);
uint32_t *StyleDef_createLayoutChars_nobottom(StyleDef *p,int *plen);
uint32_t *StyleDef_createLayoutChars_hanging(StyleDef *p,int *plen);
uint32_t *StyleDef_createLayoutChars_nosep(StyleDef *p,int *plen);
uint32_t *StyleDef_createLayoutChars_replace(StyleDef *p,int *plen);

/* StyleWork */

void StyleWork_free(StyleWork *p);
void StyleWork_freeFull(StyleWork *p);
void StyleWork_freeFont(StyleWork *p);

void StyleWork_createFont(StyleWork *p);
void StyleWork_getScreenSize(StyleWork *p,mSize *dst);
void StyleWork_readStyle(StyleWork *p,const char *name);
int StyleWork_apply(StyleWork *p,StyleDef *ps);

/* config */

int StyleConf_openRead(mIniRead **dst);
void StyleConf_readStyle(StyleDef *p,const char *name);
void StyleConf_readDefine(mIniRead *ini,StyleDef *p);

void *StyleConf_openWrite(int num);
void StyleConf_writeDefine(void *fp,int no,StyleDef *p);
