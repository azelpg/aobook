/*$
aobook
Copyright (c) 2014-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/********************************
 * ウィジェット関連の関数
 ********************************/

/* MainWindow */

enum
{
	PAGENO_PREV = -1,
	PAGENO_NEXT = -2,
	PAGENO_HOME = -3,
	PAGENO_END = -4
};

void MainWindowNew(mToplevelSaveState *state);
void MainWindow_update(MainWindow *p);
void MainWindow_setTitle(MainWindow *p);
void MainWindow_setRecentFileMenu(MainWindow *p);
void MainWindow_setMenuCheck_bookmark(MainWindow *p);

void MainWindow_loadText(MainWindow *p,const char *filename,int code,int no,mlkbool reload);
void MainWindow_loadText_nextprev(MainWindow *p,mlkbool next);
void MainWindow_loadText_recent(MainWindow *p,int no);
void MainWindow_reloadFile(MainWindow *p);

void MainWindow_text_layout(MainWindow *p);
void MainWindow_setWindowSize(MainWindow *p);
void MainWindow_changePage(MainWindow *p,PageInfoItem *page);
void MainWindow_movePage(MainWindow *p,int no);
void MainWindow_movePage_lineno(MainWindow *p,int no,mlkbool wrap_top);
void MainWindow_execTool(MainWindow *p,int no);

/* ツールバー */

void Toolbar_changePageNum(Toolbar *p);
void Toolbar_changePagePos(Toolbar *p);

/* しおりウィンドウ */

void BkmWindow_new(void);
void BkmWindow_toggle(BkmWindow *p);
void BkmWindow_addGlobal(BkmWindow *p);
void BkmWindow_addLocal(BkmWindow *p);

/* スタイル設定 */

enum STYLEDLG_FLAGS
{
	STYLEDLG_F_UPDATE = 1<<0,		//画面の更新
	STYLEDLG_F_LAYOUT = 1<<1,		//再レイアウトを行う
	STYLEDLG_F_UPDATE_LIST = 1<<2	//スタイルのリストが更新
};

int StyleOptDialog(mWindow *parent);

/* 環境設定 */

enum
{
	ENVDLG_F_UPDATE_KEY = 1<<0,
	ENVDLG_F_UPDATE_TOOL = 1<<1
};

int EnvOptDialog(mWindow *parent);
