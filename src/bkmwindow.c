/*$
aobook
Copyright (c) 2014-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * BkmWindow : しおりウィンドウ
 *****************************************/

#include "mlk_gui.h"
#include "mlk_widget_def.h"
#include "mlk_widget.h"
#include "mlk_window.h"
#include "mlk_event.h"
#include "mlk_sysdlg.h"
#include "mlk_listview.h"
#include "mlk_iconbar.h"
#include "mlk_tab.h"
#include "mlk_columnitem.h"

#include "mlk_str.h"
#include "mlk_list.h"
#include "mlk_imagelist.h"

#include "def_global.h"
#include "func_widget.h"
#include "bookmark.h"
#include "trid.h"


//-------------------

struct _BkmWindow
{
	MLK_TOPLEVEL_DEF

	mTab *tab;
	mListView *lv;
	mIconBar *ib;
};

//-------------------

#define _TABNO_GLOBAL 0
#define _TABNO_LOCAL  1

enum
{
	WID_TAB = 100,
	WID_LIST
};

enum
{
	TRID_TITLE = 0,
	TRID_GLOBAL,
	TRID_LOCAL,
	TRID_OPEN,
	TRID_ADD,
	TRID_DEL,
	TRID_PAGEMOVE,
	TRID_EDIT,
	TRID_SAVE,
	TRID_CLEAR,
	TRID_MES_CLEAR,
	TRID_MES_ADD_BOOKMARK
};

enum
{
	ICONIMG_ADD,
	ICONIMG_DEL,
	ICONIMG_EDIT,
	ICONIMG_PAGE,
	ICONIMG_OPEN,
	ICONIMG_SAVE,
	ICONIMG_CLEAR
};

//-------------------

static const uint8_t g_iconimg[190] = {
0x89,0x50,0x4e,0x47,0x0d,0x0a,0x1a,0x0a,0x00,0x00,0x00,0x0d,0x49,0x48,0x44,0x52,
0x00,0x00,0x00,0x69,0x00,0x00,0x00,0x0f,0x01,0x00,0x00,0x00,0x00,0xcf,0x54,0xdc,
0xd0,0x00,0x00,0x00,0x85,0x49,0x44,0x41,0x54,0x78,0x5e,0x63,0xf8,0x8f,0x0c,0x1a,
0x18,0xfe,0xd4,0x03,0xa9,0x1f,0xf5,0xfc,0xff,0x3f,0x30,0x24,0x30,0x80,0xb9,0x7f,
0x18,0x98,0xe7,0xbc,0x91,0xff,0xfe,0x3f,0xff,0x1f,0x98,0xfb,0xf7,0xff,0xe7,0xc7,
0x2f,0xf8,0x81,0xdc,0x7d,0x50,0xae,0xf0,0xf7,0x17,0x0c,0xe6,0xff,0x73,0xb7,0x41,
0xb9,0x9b,0xbf,0xbf,0xfe,0xbf,0xbd,0x3f,0x6f,0x5d,0x03,0x43,0x03,0x33,0x03,0x2b,
0x90,0x0b,0x94,0xed,0x07,0xe9,0x45,0xe2,0x66,0x3c,0x42,0xe2,0x9a,0x31,0x43,0x4c,
0x86,0xc9,0xb2,0x83,0xf4,0x22,0x4c,0xee,0x87,0x9b,0xfc,0xa1,0x9c,0x83,0xc1,0xfc,
0x3e,0xd4,0xde,0x3f,0x0c,0x0c,0x22,0x40,0x2e,0xcc,0x55,0x40,0xc0,0x50,0xc1,0x60,
0x00,0x76,0x33,0xc4,0x2b,0x07,0xa1,0x3e,0x02,0x00,0x5b,0xca,0x85,0x58,0x1d,0xb3,
0x95,0x22,0x00,0x00,0x00,0x00,0x49,0x45,0x4e,0x44,0xae,0x42,0x60,0x82 };

//アイコンバーボタン

static const uint16_t g_btt_global[] = {
	ICONIMG_PAGE | (TRID_OPEN << 8),
	0xffff,
	ICONIMG_ADD | (TRID_ADD << 8),
	ICONIMG_DEL | (TRID_DEL << 8),
	0
};

static const uint16_t g_btt_local[] = {
	ICONIMG_PAGE | (TRID_PAGEMOVE << 8),
	0xffff,
	ICONIMG_ADD | (TRID_ADD << 8),
	ICONIMG_DEL | (TRID_DEL << 8),
	ICONIMG_EDIT | (TRID_EDIT << 8),
	0xffff,
	ICONIMG_OPEN | (TRID_OPEN << 8),
	ICONIMG_SAVE | (TRID_SAVE << 8),
	ICONIMG_CLEAR | (TRID_CLEAR << 8),
	0
};

//-------------------



//==============================
// sub
//==============================


/* アイコンバーのボタンセット */

static void _set_buttons(BkmWindow *p)
{
	mIconBar *ib = p->ib;
	const uint16_t *pdat;
	int img,trid;

	mIconBarDeleteAll(ib);

	pdat = (GDAT->bkmwin_tabno == 0)? g_btt_global: g_btt_local;

	for(; *pdat; pdat++)
	{
		if(*pdat == 0xffff)
			mIconBarAddSep(ib);
		else
		{
			img = *pdat & 0xff;
			trid = *pdat >> 8;

			mIconBarAdd(ib, trid, img, trid, 0);
		}
	}
}

/* リストをセット
 *
 * local_sel: ローカル時に選択するアイテム */

static void _set_list(BkmWindow *p,BkmLocal *local_sel)
{
	BkmGlobal *pg;
	BkmLocal *pl;
	mColumnItem *item,*selitem = NULL;
	mStr str = MSTR_INIT;

	mListViewDeleteAllItem(p->lv);

	if(GDAT->bkmwin_tabno == _TABNO_GLOBAL)
	{
		//グローバル

		for(pg = BKM_GLOBAL(GDAT->list_bkm_global.top); pg; pg = BKM_GLOBAL(pg->i.next))
		{
			BkmGlobal_getListStr(&str, pg);

			mListViewAddItem_text_copy_param(p->lv, str.buf, (intptr_t)pg);
		}
	}
	else
	{
		//ローカル

		for(pl = BKM_LOCAL(GDAT->list_bkm_local.top); pl; pl = BKM_LOCAL(pl->i.next))
		{
			BkmLocal_getListStr(&str, pl);

			item = mListViewAddItem_text_copy_param(p->lv, str.buf, (intptr_t)pl);

			if(local_sel == pl)
				selitem = item;
		}
	}

	mStrFree(&str);

	//選択

	if(selitem)
		mListViewSetFocusItem_scroll(p->lv, selitem);
}


//==============================
// イベント
//==============================


/* グローバル開く */

static void _open_global(BkmWindow *p,mColumnItem *item)
{
	BkmGlobal *pi;

	if(!item) return;

	pi = BKM_GLOBAL(item->param);

	if(mStrPathCompareEq(&GDAT->strFileName, pi->fname))
		//現在のファイル
		MainWindow_movePage_lineno(GDAT->mainwin, pi->lineno, FALSE);
	else
		MainWindow_loadText(GDAT->mainwin, pi->fname, -1, pi->lineno, FALSE);
}

/* ローカルページ移動 */

static void _move_local(BkmWindow *p,mColumnItem *item)
{
	if(item)
		MainWindow_movePage_lineno(GDAT->mainwin, BKM_LOCAL(item->param)->lineno, FALSE);
}

/* アイテム削除 */

static void _delete_item(BkmWindow *p,mColumnItem *item)
{
	if(!item) return;

	//データのアイテム削除

	mListDelete(
		(GDAT->bkmwin_tabno == _TABNO_GLOBAL)? &GDAT->list_bkm_global: &GDAT->list_bkm_local,
		MLISTITEM(item->param));

	//リストのアイテム削除

	mListViewDeleteItem_focus(p->lv);
}

/* コメント編集 */

static void _edit_comment(BkmWindow *p,mColumnItem *item)
{
	mStr str = MSTR_INIT;
	BkmLocal *pi;

	if(!item) return;

	pi = BKM_LOCAL(item->param);

	mStrSetText(&str, pi->comment);

	if(mSysDlg_inputText(MLK_WINDOW(p),
		MLK_TR2(TRGROUP_ID_BOOKMARK, TRID_EDIT),
		NULL, MSYSDLG_INPUTTEXT_F_SET_DEFAULT, &str))
	{
		//データ
		
		mFree(pi->comment);

		if(mStrIsEmpty(&str))
			pi->comment = NULL;
		else
			pi->comment = mStrdup(str.buf);

		//リスト

		BkmLocal_getListStr(&str, pi);

		mListViewSetItemText_copy(p->lv, item, str.buf);
	}

	mStrFree(&str);
}

/* ローカル・読み込み */

static void _read_local(BkmWindow *p)
{
	mStr str = MSTR_INIT;

	//ファイル名取得

	if(!mSysDlg_openFile(MLK_WINDOW(p),
		"txt (*.txt)\t*.txt\tall files\t*", 0,
		GDAT->strBkmLocalDir.buf, 0, &str))
		return;

	//ディレクトリ記録

	mStrPathGetDir(&GDAT->strBkmLocalDir, str.buf);

	//読み込み

	BkmLocal_loadFile(str.buf);

	mStrFree(&str);

	//リスト

	_set_list(p, NULL);
}

/* ローカル・保存 */

static void _save_local(BkmWindow *p)
{
	mStr str = MSTR_INIT;

	if(GDAT->list_bkm_local.num == 0) return;

	//ファイル名取得

	if(!mSysDlg_saveFile(MLK_WINDOW(p),
		"txt (*.txt)\ttxt", 0,
		GDAT->strBkmLocalDir.buf, 0, &str, NULL))
		return;

	//ディレクトリ記録

	mStrPathGetDir(&GDAT->strBkmLocalDir, str.buf);

	//保存

	BkmLocal_saveFile(str.buf);

	mStrFree(&str);
}


//=================


/* コマンドイベント */

static void _event_command(BkmWindow *p,mEventCommand *ev)
{
	mColumnItem *item;
	int is_global;

	is_global = (GDAT->bkmwin_tabno == _TABNO_GLOBAL);

	item = mListViewGetFocusItem(p->lv);

	switch(ev->id)
	{
		//開く
		case TRID_OPEN:
			if(is_global)
				_open_global(p, item);
			else
				_read_local(p);
			break;
		//移動 (local)
		case TRID_PAGEMOVE:
			_move_local(p, item);
			break;
		//現在のページを追加
		case TRID_ADD:
			if(is_global)
				BkmWindow_addGlobal(p);
			else
				BkmWindow_addLocal(p);
			break;
		//削除
		case TRID_DEL:
			if(item)
			{
				_delete_item(p, item);

				if(is_global)
					GDAT->fchange_bkm_global = TRUE;
			}
			break;
		//コメント編集 (local)
		case TRID_EDIT:
			_edit_comment(p, item);
			break;
		//保存 (local)
		case TRID_SAVE:
			_save_local(p);
			break;
		//クリア (local)
		case TRID_CLEAR:
			if(GDAT->list_bkm_local.num)
			{
				if(mMessageBox(MLK_WINDOW(p), NULL,
					MLK_TR2(TRGROUP_ID_BOOKMARK, TRID_MES_CLEAR),
					MMESBOX_YES | MMESBOX_NO, MMESBOX_YES)
					== MMESBOX_YES)
				{
					mListDeleteAll(&GDAT->list_bkm_local);
					mListViewDeleteAllItem(p->lv);
				}
			}
			break;
	}
}

/* 通知イベント */

static void _event_notify(BkmWindow *p,mEventNotify *ev)
{
	switch(ev->id)
	{
		//リスト
		case WID_LIST:
			if(ev->notify_type == MLISTVIEW_N_ITEM_L_DBLCLK)
			{
				//ダブルクリック

				if(GDAT->bkmwin_tabno == _TABNO_GLOBAL)
					_open_global(p, MLK_COLUMNITEM(ev->param1));
				else
					_move_local(p, MLK_COLUMNITEM(ev->param1));
			}
			break;
		//タブ
		case WID_TAB:
			if(ev->notify_type == MTAB_N_CHANGE_SEL)
			{
				GDAT->bkmwin_tabno = ev->param1;

				_set_buttons(p);
				_set_list(p, NULL);
			}
			break;
	}
}

/* イベント */

static int _event_handle(mWidget *wg,mEvent *ev)
{
	switch(ev->type)
	{
		case MEVENT_NOTIFY:
			_event_notify((BkmWindow *)wg, (mEventNotify *)ev);
			break;
		case MEVENT_COMMAND:
			_event_command((BkmWindow *)wg, (mEventCommand *)ev);
			break;
	
		//閉じる -> 破棄
		case MEVENT_CLOSE:
			BkmWindow_toggle((BkmWindow *)wg);
			break;

		default:
			return FALSE;
	}

	return TRUE;
}


//==============================
// 作成
//==============================


/* ウィジェット作成 */

static void _create_widget(BkmWindow *p)
{
	mTab *tab;
	mImageList *img;

	//タブ

	p->tab = tab = mTabCreate(MLK_WIDGET(p), WID_TAB, MLF_EXPAND_W, 0, MTAB_S_TOP | MTAB_S_HAVE_SEP);

	mTabAddItem_text_static(tab, MLK_TR(TRID_GLOBAL));
	mTabAddItem_text_static(tab, MLK_TR(TRID_LOCAL));

	mTabSetSel_atIndex(tab, GDAT->bkmwin_tabno);

	//アイコンバー

	p->ib = mIconBarCreate(MLK_WIDGET(p), 0, MLF_EXPAND_W, 0,
		MICONBAR_S_TOOLTIP | MICONBAR_S_DESTROY_IMAGELIST);

	img = mImageListLoadPNG_buf(g_iconimg, sizeof(g_iconimg), 15);

	mImageListReplaceTextColor_mono(img);

	mIconBarSetImageList(p->ib, img);
	mIconBarSetTooltipTrGroup(p->ib, TRGROUP_ID_BOOKMARK);

	_set_buttons(p);

	//リスト

	p->lv = mListViewCreate(MLK_WIDGET(p), WID_LIST, MLF_EXPAND_WH, 0,
		MLISTVIEW_S_AUTO_WIDTH, MSCROLLVIEW_S_HORZVERT_FRAME);

	_set_list(p, NULL);
}

/** しおりウィンドウ作成 */

void BkmWindow_new(void)
{
	BkmWindow *p;

	if(GDAT->bkmwin) return;

	//作成
	
	p = (BkmWindow *)mToplevelNew(MLK_WINDOW(GDAT->mainwin), sizeof(BkmWindow),
		MTOPLEVEL_S_NORMAL | MTOPLEVEL_S_PARENT | MTOPLEVEL_S_TAB_MOVE | MTOPLEVEL_S_NO_INPUT_METHOD);
	if(!p) return;

	GDAT->bkmwin = p;

	p->wg.event = _event_handle;
	p->ct.sep = 6;
	p->top.accelerator = MLK_TOPLEVEL(GDAT->mainwin)->top.accelerator;

	mContainerSetPadding_same(MLK_CONTAINER(p), 3);


	//

	MLK_TRGROUP(TRGROUP_ID_BOOKMARK);

	mToplevelSetTitle(MLK_TOPLEVEL(p), MLK_TR(TRID_TITLE));

	//ウィジェット

	_create_widget(p);

	//表示

	if(!GDAT->bkmwin_state.w || !GDAT->bkmwin_state.h)
	{
		GDAT->bkmwin_state.w = 300;
		GDAT->bkmwin_state.h = 400;
	}

	mToplevelSetSaveState(MLK_TOPLEVEL(p), &GDAT->bkmwin_state);

	mWidgetShow(MLK_WIDGET(p), 1);
}


//==============================
//
//==============================


/** ウィンドウの表示状態切り替え */

void BkmWindow_toggle(BkmWindow *p)
{
	GDAT->viewflags ^= VIEWFLAGS_BOOKMARK;

	if(GDAT->bkmwin)
	{
		//破棄
		
		mToplevelGetSaveState(MLK_TOPLEVEL(p), &GDAT->bkmwin_state);

		mWidgetDestroy(MLK_WIDGET(p));

		GDAT->bkmwin = NULL;
	}
	else
		BkmWindow_new();

	//メニューのチェック

	MainWindow_setMenuCheck_bookmark(GDAT->mainwin);
}

/** グローバルに追加 */

void BkmWindow_addGlobal(BkmWindow *p)
{
	BkmGlobal *pi;

	//データ追加

	pi = BkmGlobal_add();
	if(!pi) return;

	//

	if(!p)
	{
		//しおりウィンドウが非表示の時はメッセージ

		mMessageBoxOK(MLK_WINDOW(GDAT->mainwin), MLK_TR2(TRGROUP_ID_BOOKMARK, TRID_MES_ADD_BOOKMARK));
	}
	else if(GDAT->bkmwin_tabno == _TABNO_GLOBAL)
	{
		//リストに追加
		
		mStr str = MSTR_INIT;
		mColumnItem *item;

		BkmGlobal_getListStr(&str, pi);

		item = mListViewAddItem_text_copy_param(p->lv, str.buf, (intptr_t)pi);

		mListViewSetFocusItem_scroll(p->lv, item);

		mStrFree(&str);
	}
}

/** ローカルに追加 */

void BkmWindow_addLocal(BkmWindow *p)
{
	BkmLocal *pi;

	//データ追加

	pi = BkmLocal_add();
	if(!pi) return;

	//

	if(!p)
		mMessageBoxOK(MLK_WINDOW(GDAT->mainwin), MLK_TR2(TRGROUP_ID_BOOKMARK, TRID_MES_ADD_BOOKMARK));
	else if(GDAT->bkmwin_tabno == _TABNO_LOCAL)
	{
		//リストアイテムを再セット
		//(行番号順にソートされるため)

		_set_list(p, pi);
	}
}

