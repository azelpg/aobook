/*$
aobook
Copyright (c) 2014-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/**********************************
 * レイアウト - 描画
 **********************************/

#include <stdio.h>

#include "mlk_gui.h"
#include "mlk_pixbuf.h"
#include "mlk_imagebuf.h"
#include "mlk_loadimage.h"
#include "mlk_str.h"
#include "mlk_rectbox.h"
#include "mlk_font.h"

#include "def_global.h"
#include "def_style.h"
#include "def_textdata.h"
#include "font.h"
#include "pv_layout.h"


//---------------

//傍点文字
static const uint32_t g_bouten_char[] = {
	U'﹅', U'﹆', U'◉', U'・', U'○', U'▲', U'△', U'◎', U'×'
};

mlkbool extractZipFile(const char *zipname,const char *filename,mBufSize *dst);

//---------------


//=============================
// 傍線描画
//=============================


/* mPixbuf 1xN パターンを描画 */

static void _draw_pixbuf_pattern(mPixbuf *img,
	int x,int y,int h,uint32_t col,uint8_t pat,int patw)
{
	int iy,pcnt;
	uint8_t f;

	for(iy = 0, f = 0x80, pcnt = patw; iy < h; iy++)
	{
		if(pat & f)
			mPixbufSetPixel(img, x, y + iy, col);
	
		//

		pcnt--;
		if(pcnt)
			f >>= 1;
		else
		{
			f = 0x80;
			pcnt = patw;
		}
	}
}

/* mPixbuf 波線を描画 */

static void _draw_pixbuf_wave(mPixbuf *img,int x,int y,int h,uint32_t col)
{
	int iy,py;
	uint8_t pat[] = { 1,1,2,2,2,1,1,0,0,0 };

	for(iy = 0, py = 0; iy < h; iy++)
	{
		mPixbufSetPixel(img, x + pat[py], y + iy, col);
	
		py++;
		if(py == 10) py = 0;
	}
}

/* 傍線描画 */

static void _draw_pixbuf_bousen(mPixbuf *img,int x,int y,int h,uint32_t col,int type)
{
	switch(type)
	{
		//通常傍線
		case BOUSEN_TYPE_NORMAL:
			mPixbufLineV(img, x, y, h, col);
			break;
		//二重傍線
		case BOUSEN_TYPE_DOUBLE:
			mPixbufLineV(img, x, y, h, col);
			mPixbufLineV(img, x + 2, y, h, col);
			break;
		//鎖線
		case BOUSEN_TYPE_KUSARI:
			_draw_pixbuf_pattern(img, x, y, h, col, 0xc0, 4);
			break;
		//破線
		case BOUSEN_TYPE_HASEN:
			_draw_pixbuf_pattern(img, x, y, h, col, 0xf8, 8);
			break;
		//波線
		case BOUSEN_TYPE_NAMISEN:
			_draw_pixbuf_wave(img, x, y, h, col);
			break;
	}
}


//=============================
// 1行描画
//=============================


/* 本文描画 */

static void _draw_text(LayoutWork *p,int xtop,int ytop)
{
	mPixbuf *img;
	CharItem *pi,*next;
	mFont *font[2];
	uint32_t flags_base,flags,col,c,charflags;
	int x,y,n1,n2,fontno,fonth[2];
	uint8_t fdash_to_line,dakuten_type;

	font[0] = p->font_main;
	font[1] = p->font_bold;
	fonth[0] = p->fontmain_h;
	fonth[1] = p->fontbold_h;

	col = p->stdef->col_text;
	img = p->img;

	fdash_to_line = ((p->stdef->flags & STYLE_F_DASH_TO_LINE) != 0);
	dakuten_type = p->stdef->dakuten_type;

	//印刷標準字体に置き換え

	flags_base = (p->stdef->flags & STYLE_F_REPLACE_PRINT)? FONT_DRAW_F_PRINT: 0;

	//

	for(pi = (CharItem *)p->list_char.top; pi; pi = next)
	{
		next = (CharItem *)pi->i.next;
	
		if(pi->x < 0) break;
		if(pi->x >= p->pageW) continue; //前ページの折り返し前部分はスキップ

		x = xtop + pi->x;
		y = ytop + pi->y + pi->padding;
		charflags = pi->flags;

		fontno = ((charflags & CHARITEM_F_BOLD) != 0);

		//------- 文字

		if(pi->chartype == CHARITEM_TYPE_HORZ)
		{
			//----- [縦中横]

			FontDrawText_horz(font[fontno], img,
				x + (fonth[0] - pi->width) / 2, y,
				pi->horzchar, pi->horzcnt, col, 0);
		}
		else
		{
			//----- [通常 or 横組み]

			//フラグ
		
			flags = flags_base;

			if(pi->chartype == CHARITEM_TYPE_ROTATE)
				flags |= FONT_DRAW_F_ROTATE;

			//文字

			if(fdash_to_line
				&& (pi->code == 0x2015 || pi->code == 0x2500)	//全角ダッシュと罫線
				&& pi->chartype == CHARITEM_TYPE_NORMAL)
			{
				//横線文字を直線にして描画

				n1 = pi->height;

				if(next
					&& (next->code == 0x2015 || next->code == 0x2500)
					&& next->chartype == CHARITEM_TYPE_NORMAL
					&& !(next->flags & CHARITEM_F_WRAP_TOP))
					//次の文字も横線なら、字間分を加算
					n1 += p->stdef->char_space;

				mPixbufLineV(img, x + (fonth[0] >> 1), y, n1, p->pixcol_text);
			}
			else
				//通常
				FontDrawText_vert(font[fontno], img, x, y, &pi->code, 1, col, flags);

			//濁点/半濁点

			if(charflags & (CHARITEM_F_DAKUTEN | CHARITEM_F_HANDAKUTEN))
			{
				if(dakuten_type < STYLE_DAKUTEN_COMBINE)
					//そのまま
					c = (charflags & CHARITEM_F_DAKUTEN)? U'゛': U'゜';
				else
					//結合文字
					c = (charflags & CHARITEM_F_DAKUTEN)? 0x3099: 0x309a;

				//位置

				n1 = n2 = 0;

				if(dakuten_type == STYLE_DAKUTEN_NORMAL_HORZ
					|| dakuten_type == STYLE_DAKUTEN_COMBINE_HORZ)
					n1 = fonth[fontno];
				else if(dakuten_type == STYLE_DAKUTEN_NORMAL_VERT
					|| dakuten_type == STYLE_DAKUTEN_COMBINE_VERT)
				{
					n2 = -fonth[fontno];
					flags |= FONT_DRAW_F_DAKUTEN_VERT;
				}

				FontDrawText_vert(font[fontno], img, x + n1, y + n2, &c, 1, col, flags);
			}
		}

		//----- 傍点

		if(pi->bouten)
		{
			mFontDrawText_pixbuf_utf32(p->font_kenten, img,
				x + fonth[fontno],
				y + (pi->height - p->fontkenten_h) / 2,
				&g_bouten_char[pi->bouten - 1], 1,
				col);
		}
	}
}

/* ルビ描画 */

static void _draw_ruby(LayoutWork *p,int xtop,int ytop,int pagepos)
{
	RubyItem *pi;
	CharItem *pichar;
	mFont *font;
	int i,x,y,addx,rlen,pad,padtop,pad2,n,overh,last_x,last_y;
	uint32_t col;

	//文字幅分を加算
	xtop += p->fontmain_h;

	font = p->font_ruby;
	col = p->stdef->col_ruby;
	overh = p->fontruby_h / 2;
	last_x = last_y = -1;

	for(pi = (RubyItem *)p->list_ruby.top; pi; pi = (RubyItem *)pi->i.next)
	{
		//次ページへの折り返し
		if(pi->x < 0) break;

		x = pi->x;
		y = pi->y;
		rlen = pi->rubylen;

		//前のルビが折り返し後、現在のルビ先頭を超えている場合は、続きから

		if(x == last_x && y < last_y)
			y = last_y;

		//親文字列の先頭

		pichar = pi->char_top;

		//傍線がある場合は x+2

		addx = (pichar->bousen)? 2: 0;

		//ルビ余白

		pad = pi->char_h - pi->ruby_h;

		if(pi->char_h == RUBYITEM_CHARH_NO_PADDING)
			//親文字列先頭から、余白なし
			padtop = 0, pad2 = 0;
		else if(rlen == 1)
			//ルビが1文字の場合
			padtop = pad / 2;
		else
		{
			//ルビが2文字以上の場合
			padtop = pad / (rlen * 2);
			pad2 = pad - padtop * 2;
			pad = 0;
		}

		//各ルビ文字
		// :基本的に親文字列に合わせて描画するが、ルビの上端が親文字列の範囲内にあれば、
		// :下ははみ出す場合もある。
		// :また、ルビのない文字にかかった場合も、以降の本文文字は位置合わせの対象となる。

		for(i = 0; i < rlen; i++)
		{
			//余白追加

			if(i == 0)
				y += padtop;
			else if(pad2)
			{
				//ルビ2文字以上の場合、一定間隔の値では、ルビ数が多いと等間隔にならないため、
				//位置に応じて余白幅を計算。
				//pad = 前回の余白位置, n = 現在のルビの余白位置
				
				n = (int)((double)i / (rlen - 1) * pad2 + 0.5); 
				y += n - pad;
				pad = n;
			}

			//現在の親文字を超える場合、次の親文字へ
			// : 最後のルビの場合は、親の下端 -1 px の位置でも描画
			// : それ以外は、ルビの半分の高さまではみ出し可能

			n = (i == rlen - 1)? 0: overh;

			if(pichar
				&& y >= pichar->y + pichar->height - n)
			{
				pichar = (CharItem *)pichar->i.next;
				
				for(; pichar; pichar = (CharItem *)pichar->i.next)
				{
					if((pichar->flags & CHARITEM_F_WRAP_TOP)
						|| y <= pichar->y + pichar->height - pichar->height / 4)
						break;
				}

				//次が折り返しの場合、親文字に合わせてルビも折り返し

				if(pichar && (pichar->flags & CHARITEM_F_WRAP_TOP))
				{
					x -= p->line_width;
					y = p->jisage_wrap_y;

					//見開き右ページの終端行の場合、左ページ位置へ
					
					if(pagepos == 0 && x < 0)
						x -= p->stdef->page_space;
				}
			}
		
			//ルビ描画
			// 前ページからの折り返しの文字が存在する場合があるため、
			// x の範囲は1文字ずつ判定する (x は本文文字の位置)

			if(x >= 0 && x < p->pageW)
			{
				y += FontDrawText_vert(font, p->img,
					x + xtop + addx, y + ytop,
					pi->rubytxt + i, 1, col, FONT_DRAW_F_RUBY);
			}
			else
			{
				//前ページからの折り返しの位置の場合、高さのみ加算
				
				y += FontGetTextWidth_vert(font, pi->rubytxt + i, 1, FONT_DRAW_F_RUBY);
			}
		}

		last_x = x;
		last_y = y;
	}
}

/* 傍線を描画 */

static void _draw_bousen(LayoutWork *p,int xtop,int ytop)
{
	CharItem *pi,*top,*next;
	int h,x,y,type,charspace;
	uint32_t col;

	col = p->pixcol_text;

	charspace = p->stdef->char_space;

	for(top = (CharItem *)p->list_char.top; top; top = pi)
	{
		//傍線開始位置

		for(pi = top; pi && !pi->bousen; pi = (CharItem *)pi->i.next);

		if(!pi) break;

		//傍線終了まで描画

		x = pi->x;
		y = pi->y;
		h = 0;
		type = pi->bousen;

		for(; pi && pi->bousen == type; pi = next)
		{
			next = (CharItem *)pi->i.next;

			h += pi->height;

			if(!next
				|| (next->bousen != type || (next->flags & CHARITEM_F_WRAP_TOP)))
			{
				//傍線の終端、または描画上の1行の終端時にまとめて描画
				
				if(x >= 0 && x < p->pageW)
				{
					_draw_pixbuf_bousen(p->img,
						x + p->fontmain_h + xtop, y + ytop, h, col, type);
				}

				if(next)
				{
					x = next->x;
					y = next->y;
				}
				
				h = 0;
			}
			else
				h += charspace;
		}
	}
}

/** 1行分を描画 */

void layout_draw_line(LayoutWork *p,int pagepos)
{
	int xtop,ytop;

	xtop = p->stdef->margin.x1;
	ytop = p->stdef->margin.y1;

	//見開きで右ページの場合

	if(pagepos == 0)
		xtop += p->stdef->page_space + p->pageW;

	//本文

	_draw_text(p, xtop, ytop);

	//傍線

	_draw_bousen(p, xtop, ytop);

	//ルビ

	_draw_ruby(p, xtop, ytop, pagepos);
}


//============================
//
//============================


/** ページ情報を描画
 *
 * pos: [-1] 単ページ [0] 右側 [1] 左側 */

void layout_draw_pageinfo(LayoutWork *p,int pageno,int pos)
{
	char m[64];
	int x;

	if(pos == -1 || pos == 0)
		//右側
		snprintf(m, 64, "%d/%d", pageno + 1, p->pagenum);
	else
		//左
		snprintf(m, 64, "%d", pageno + 1);

	//

	if(pos == -1 || pos == 1)
		//単ページまたは左側
		x = 4;
	else
	{
		//見開きの右側

		x = p->stdef->margin.x1 + p->stdef->margin.x2
			+ p->pageW * 2 + p->stdef->page_space
			- 4 - mFontGetTextWidth(p->style->font_info, m, -1);
	}

	mFontDrawText_pixbuf(p->style->font_info, p->img,
		x, 4, m, -1, p->stdef->col_info);
}

/* 画像読み込み */

static mImageBuf *_load_image(const char *fname,mlkbool fzip)
{
	mLoadImageType tp;
	mLoadImageOpen open;
	mBufSize buf;
	mImageBuf *img;
	mFuncLoadImageCheck funcs[] = {
		mLoadImage_checkPNG, mLoadImage_checkJPEG, mLoadImage_checkBMP, 0
	};

	if(fzip)
	{
		if(!extractZipFile(GDAT->strFileName.buf, fname, &buf))
			return NULL;
	
		open.type = MLOADIMAGE_OPEN_BUF;
		open.buf = buf.buf;
		open.size = buf.size;
	}
	else
	{
		open.type = MLOADIMAGE_OPEN_FILENAME;
		open.filename = fname;
	}

	if(mLoadImage_checkFormat(&tp, &open, funcs, -1))
		return NULL;

	img = mImageBuf_loadImage(&open, &tp, 24, -4);

	if(fzip) mFree(buf.buf);

	return img;
}

/** 挿絵描画 */

void layout_draw_picture(LayoutWork *p,int pagepos)
{
	mStr str = MSTR_INIT;
	mImageBuf *img;
	mBox box;
	int x,y;

	//ファイル名

	if(GDAT->is_file_zip)
	{
		//ZIP

		mStrSetText(&str, (const char *)(p->pagestate.picture + 2));
	}
	else
	{
		//通常ファイル:テキストファイルのパスを基点とする
		
		mStrPathGetDir(&str, GDAT->strFileName.buf);
		mStrPathJoin(&str, (const char *)(p->pagestate.picture + 2));
	}

	//読み込み

	img = _load_image(str.buf, GDAT->is_file_zip);

	mStrFree(&str);
	
	if(!img) return;

	//位置・サイズ

	box.x = box.y = 0;
	box.w = img->width, box.h = img->height;

	mBoxResize_keepaspect(&box, p->pageW, p->pageH, TRUE);

	x = p->stdef->margin.x1 + box.x;
	y = p->stdef->margin.y1 + box.y;

	if(pagepos == 0)
		x += p->stdef->page_space + p->pageW;

	//描画

	mPixbufBltScale_oversamp_imagebuf(p->img, x, y, box.w, box.h, img, 5);

	mImageBuf_free(img);
}
