/*$
aobook
Copyright (c) 2014-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/********************************
 * スタイルデータ定義
 ********************************/

typedef struct _StyleDef StyleDef;
typedef struct _StyleWork StyleWork;

#define STYLE_CHARS_DEFAULT ((uint32_t *)1)

//スタイル定義

struct _StyleDef
{
	int chars,		//文字数
		lines,		//行数
		char_space,	//字間 (px)
		line_space,	//行間 (フォント高さに対する%)
		page_space;	//ページ間の空白
	uint32_t flags;

	uint8_t pages,		//ページ数 (1or2)
		dakuten_type;	//濁点/半濁点の処理

	mRect margin;	//画面余白

	mRgbCol col_text,	//本文文字色
		col_ruby,		//ルビ文字色
		col_info,		//ページ情報色
		col_bkgnd;		//背景色

	uint32_t *u32_nohead,	//行頭禁則 (NULL でなし。1 でデフォルト)
		*u32_nobottom,		//行末禁則
		*u32_hanging,		//ぶら下げ対象文字
		*u32_nosep,			//分割禁止
		*u32_replace;		//置換

	mStr str_stylename,	//スタイル名
		str_fontmain,	//本文フォント
		str_fontruby,	//ルビフォント
		str_fontbold,	//太字フォント
		str_fontinfo,	//ページ情報フォント
		str_bkgndimg;	//背景画像ファイル名
};

//現在使用中のスタイル情報

struct _StyleWork
{
	StyleDef b;

	mFont *font_main,
		*font_ruby,
		*font_bold,
		*font_info;
	mImageBuf *img_bkgnd;
};

//濁点タイプ

enum
{
	STYLE_DAKUTEN_NORMAL,		//そのまま
	STYLE_DAKUTEN_NORMAL_HORZ,	//そのまま:横に全角幅ずらす
	STYLE_DAKUTEN_NORMAL_VERT,	//そのまま:縦に全角幅ずらす
	STYLE_DAKUTEN_COMBINE,		//結合文字
	STYLE_DAKUTEN_COMBINE_HORZ,	//結合文字:横に全角幅
	STYLE_DAKUTEN_COMBINE_VERT	//結合文字:縦に全角幅
};

//フラグ

enum STYLE_FLAGS
{
	STYLE_F_BKGND_TILE = 1<<0,		//背景画像はタイル状に並べる
	STYLE_F_HANGING = 1<<1, 		//ぶら下げ有効
	STYLE_F_ENABLE_PICTURE = 1<<2,	//挿絵表示
	STYLE_F_DASH_TO_LINE = 1<<3,	//横線文字を直線で描画
	STYLE_F_REPLACE_PRINT = 1<<4	//印刷標準字体に置換
};

