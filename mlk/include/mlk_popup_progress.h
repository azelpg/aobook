/*$
mlk
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_POPUP_PROGRESS_H
#define MLK_POPUP_PROGRESS_H

#define MLK_POPUPPROGRESS(p)  ((mPopupProgress *)(p))
#define MLK_POPUPPROGRESS_DEF MLK_POPUP_DEF mPopupProgressData pg;

typedef struct
{
	mProgressBar *progress;
	intptr_t param;
}mPopupProgressData;

struct _mPopupProgress
{
	MLK_POPUP_DEF
	mPopupProgressData pg;
};


#ifdef __cplusplus
extern "C" {
#endif

mPopupProgress *mPopupProgressNew(int size,uint32_t progress_style);
void mPopupProgressRun(mPopupProgress *p,mWidget *parent,int x,int y,mBox *box,uint32_t popup_flags,
	int width,void (*threadfunc)(mThread *));

void mPopupProgressThreadEnd(mPopupProgress *p);
void mPopupProgressThreadSetMax(mPopupProgress *p,int max);
void mPopupProgressThreadSetPos(mPopupProgress *p,int pos);
void mPopupProgressThreadIncPos(mPopupProgress *p);
void mPopupProgressThreadAddPos(mPopupProgress *p,int add);
void mPopupProgressThreadSubStep_begin(mPopupProgress *p,int stepnum,int max);
void mPopupProgressThreadSubStep_begin_onestep(mPopupProgress *p,int stepnum,int max);
void mPopupProgressThreadSubStep_inc(mPopupProgress *p);

#ifdef __cplusplus
}
#endif

#endif
