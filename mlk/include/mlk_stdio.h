/*$
mlk
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_STDIO_H
#define MLK_STDIO_H

#ifdef __cplusplus
extern "C" {
#endif

FILE *mFILEopen(const char *filename,const char *mode);
void mFILEclose(FILE *fp);
void mFILEclose_null(FILE **fp);

mlkfoff mFILEgetSize(FILE *fp);

int mFILEreadOK(FILE *fp,void *buf,int32_t size);
int mFILEreadByte(FILE *fp,void *buf);
int mFILEreadLE16(FILE *fp,void *buf);
int mFILEreadLE32(FILE *fp,void *buf);
int mFILEreadBE16(FILE *fp,void *buf);
int mFILEreadBE32(FILE *fp,void *buf);
uint16_t mFILEgetLE16(FILE *fp);
uint32_t mFILEgetLE32(FILE *fp);

int mFILEreadStr_compare(FILE *fp,const char *cmp);
int mFILEreadStr_variable(FILE *fp,char **ppdst);
int mFILEreadStr_lenBE16(FILE *fp,char **ppdst);

int mFILEreadArrayLE16(FILE *fp,void *buf,int num);
int mFILEreadArrayBE16(FILE *fp,void *buf,int num);
int mFILEreadArrayBE32(FILE *fp,void *buf,int num);

int mFILEreadFormatLE(FILE *fp,const char *format,...);
int mFILEreadFormatBE(FILE *fp,const char *format,...);

int mFILEwriteOK(FILE *fp,const void *buf,int32_t size);
int mFILEwriteByte(FILE *fp,uint8_t val);
int mFILEwriteLE16(FILE *fp,uint16_t val);
int mFILEwriteLE32(FILE *fp,uint32_t val);
int mFILEwriteBE16(FILE *fp,uint16_t val);
int mFILEwriteBE32(FILE *fp,uint32_t val);
int mFILEwrite0(FILE *fp,int size);

int mFILEwriteStr_variable(FILE *fp,const char *text,int len);
int mFILEwriteStr_lenBE16(FILE *fp,const char *text,int len);

void mFILEwriteArrayBE16(FILE *fp,void *buf,int num);
void mFILEwriteArrayBE32(FILE *fp,void *buf,int num);

#ifdef __cplusplus
}
#endif

#endif
