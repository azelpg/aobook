/*$
mlk
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_LISTVIEW_H
#define MLK_LISTVIEW_H

#include "mlk_scrollview.h"

#define MLK_LISTVIEW(p)  ((mListView *)(p))
#define MLK_LISTVIEW_DEF MLK_SCROLLVIEW_DEF mListViewData lv;

typedef struct
{
	mCIManager manager;
	mImageList *imglist;
	uint32_t fstyle;
	int item_height,
		item_height_min,
		horz_width;
}mListViewData;

struct _mListView
{
	MLK_SCROLLVIEW_DEF
	mListViewData lv;
};

enum MLISTVIEW_STYLE
{
	MLISTVIEW_S_MULTI_COLUMN = 1<<0,
	MLISTVIEW_S_MULTI_SEL = 1<<1,
	MLISTVIEW_S_HAVE_HEADER = 1<<2,
	MLISTVIEW_S_CHECKBOX  = 1<<3,
	MLISTVIEW_S_GRID_ROW  = 1<<4,
	MLISTVIEW_S_GRID_COL  = 1<<5,
	MLISTVIEW_S_AUTO_WIDTH = 1<<6,
	MLISTVIEW_S_DESTROY_IMAGELIST = 1<<7,
	MLISTVIEW_S_HEADER_SORT = 1<<8
};

enum MLISTVIEW_NOTIFY
{
	MLISTVIEW_N_CHANGE_FOCUS,
	MLISTVIEW_N_CLICK_ON_FOCUS,
	MLISTVIEW_N_CHANGE_CHECK,
	MLISTVIEW_N_ITEM_L_DBLCLK,
	MLISTVIEW_N_ITEM_R_CLICK,
	MLISTVIEW_N_CHANGE_SORT
};


#ifdef __cplusplus
extern "C" {
#endif

mListView *mListViewNew(mWidget *parent,int size,uint32_t fstyle,uint32_t fstyle_scrollview);
mListView *mListViewCreate(mWidget *parent,int id,uint32_t flayout,uint32_t margin_pack,
	uint32_t fstyle,uint32_t fstyle_scrollview);

void mListViewDestroy(mWidget *p);
void mListViewHandle_calcHint(mWidget *p);
int mListViewHandle_event(mWidget *wg,mEvent *ev);

void mListViewSetImageList(mListView *p,mImageList *img);
void mListViewSetItemHeight_min(mListView *p,int h);
void mListViewSetItemDestroyHandle(mListView *p,void (*func)(mList *,mListItem *));

mListHeader *mListViewGetHeaderWidget(mListView *p);
int mListViewGetItemHeight(mListView *p);
int mListViewCalcItemWidth(mListView *p,int itemw);
int mListViewCalcWidgetWidth(mListView *p,int itemw);

int mListViewGetItemNum(mListView *p);
mColumnItem *mListViewGetTopItem(mListView *p);
mColumnItem *mListViewGetFocusItem(mListView *p);
mColumnItem *mListViewGetItem_atIndex(mListView *p,int index);
mColumnItem *mListViewGetItem_fromParam(mListView *p,intptr_t param);
mColumnItem *mListViewGetItem_fromText(mListView *p,const char *text);

void mListViewDeleteAllItem(mListView *p);
void mListViewDeleteItem(mListView *p,mColumnItem *item);
mColumnItem *mListViewDeleteItem_focus(mListView *p);

mColumnItem *mListViewInsertItem(mListView *p,mColumnItem *ins,const char *text,int icon,uint32_t flags,intptr_t param);
mColumnItem *mListViewAddItem(mListView *p,const char *text,int icon,uint32_t flags,intptr_t param);
mColumnItem *mListViewAddItem_size(mListView *p,int size,const char *text,int icon,uint32_t flags,intptr_t param);
mColumnItem *mListViewAddItem_text_static(mListView *p,const char *text);
mColumnItem *mListViewAddItem_text_static_param(mListView *p,const char *text,intptr_t param);
mColumnItem *mListViewAddItem_text_copy(mListView *p,const char *text);
mColumnItem *mListViewAddItem_text_copy_param(mListView *p,const char *text,intptr_t param);

void mListViewSetFocusItem(mListView *p,mColumnItem *item);
void mListViewSetFocusItem_scroll(mListView *p,mColumnItem *item);
mColumnItem *mListViewSetFocusItem_index(mListView *p,int index);
mColumnItem *mListViewSetFocusItem_param(mListView *p,intptr_t param);
mColumnItem *mListViewSetFocusItem_text(mListView *p,const char *text);

int mListViewGetItemIndex(mListView *p,mColumnItem *item);
intptr_t mListViewGetItemParam(mListView *p,int index);
void mListViewGetItemColumnText(mListView *p,mColumnItem *pi,int index,mStr *str);

void mListViewSetItemText_static(mListView *p,mColumnItem *pi,const char *text);
void mListViewSetItemText_copy(mListView *p,mColumnItem *pi,const char *text);
void mListViewSetItemColumnText(mListView *p,mColumnItem *pi,int col,const char *text);

mlkbool mListViewMoveItem_updown(mListView *p,mColumnItem *item,mlkbool down);
mlkbool mListViewMoveItem(mListView *p,mColumnItem *item,mColumnItem *insert);

void mListViewSortItem(mListView *p,int (*comp)(mListItem *,mListItem *,void *),void *param);
void mListViewSetAutoWidth(mListView *p,mlkbool hintsize);
void mListViewSetColumnWidth_auto(mListView *p,int index);
void mListViewScrollToItem(mListView *p,mColumnItem *pi,int align);
void mListViewScrollToFocus(mListView *p);
void mListViewScrollToFocus_margin(mListView *p,int margin_itemnum);

#ifdef __cplusplus
}
#endif

#endif
