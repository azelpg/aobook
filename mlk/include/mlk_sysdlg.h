/*$
mlk
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_SYSDLG_H
#define MLK_SYSDLG_H

typedef struct _mFontInfo mFontInfo;

enum MMESBOX_BUTTONS
{
	MMESBOX_OK      = 1<<0,
	MMESBOX_CANCEL  = 1<<1,
	MMESBOX_YES     = 1<<2,
	MMESBOX_NO      = 1<<3,
	MMESBOX_SAVE    = 1<<4,
	MMESBOX_NOTSAVE = 1<<5,
	MMESBOX_ABORT   = 1<<6,
	MMESBOX_NOTSHOW = 1<<16,

	MMESBOX_OKCANCEL = MMESBOX_OK | MMESBOX_CANCEL,
	MMESBOX_YESNO = MMESBOX_YES | MMESBOX_NO
};

enum MSYSDLG_INPUTTEXT_FLAGS
{
	MSYSDLG_INPUTTEXT_F_NOT_EMPTY = 1<<0,
	MSYSDLG_INPUTTEXT_F_SET_DEFAULT = 1<<1
};

enum MSYSDLG_FILE_FLAGS
{
	MSYSDLG_FILE_F_MULTI_SEL = 1<<0,
	MSYSDLG_FILE_F_NO_OVERWRITE_MES = 1<<1,
	MSYSDLG_FILE_F_DEFAULT_FILENAME = 1<<2
};

enum MSYSDLF_FONT_FLAGS
{
	MSYSDLG_FONT_F_STYLE = 1<<0,
	MSYSDLG_FONT_F_SIZE = 1<<1,
	MSYSDLG_FONT_F_FILE = 1<<2,
	MSYSDLG_FONT_F_PREVIEW = 1<<3,
	MSYSDLG_FONT_F_EX = 1<<4,

	MSYSDLG_FONT_F_NORMAL = MSYSDLG_FONT_F_STYLE | MSYSDLG_FONT_F_SIZE | MSYSDLG_FONT_F_PREVIEW,
	MSYSDLG_FONT_F_ALL = 0xff
};


#ifdef __cplusplus
extern "C" {
#endif

uint32_t mMessageBox(mWindow *parent,const char *title,const char *message,uint32_t btts,uint32_t defbtt);
void mMessageBoxOK(mWindow *parent,const char *message);
void mMessageBoxErr(mWindow *parent,const char *message);
void mMessageBoxErrTr(mWindow *parent,uint16_t groupid,uint16_t strid);

void mSysDlg_about(mWindow *parent,const char *label);
void mSysDlg_about_license(mWindow *parent,const char *copying,const char *license);

mlkbool mSysDlg_inputText(mWindow *parent,
	const char *title,const char *message,uint32_t flags,mStr *strdst);
mlkbool mSysDlg_inputTextNum(mWindow *parent,
	const char *title,const char *message,uint32_t flags,int min,int max,int *dst);

mlkbool mSysDlg_selectColor(mWindow *parent,mRgbCol *col);

mlkbool mSysDlg_selectFont(mWindow *parent,uint32_t flags,mFontInfo *info);
mlkbool mSysDlg_selectFont_text(mWindow *parent,uint32_t flags,mStr *str);

mlkbool mSysDlg_openFile(mWindow *parent,const char *filter,int def_filter,
	const char *initdir,uint32_t flags,mStr *strdst);

mlkbool mSysDlg_saveFile(mWindow *parent,const char *filter,int def_filter,
	const char *initdir,uint32_t flags,mStr *strdst,int *pfiltertype);

mlkbool mSysDlg_selectDir(mWindow *parent,const char *initdir,uint32_t flags,mStr *strdst);

mlkbool mSysDlg_openFontFile(mWindow *parent,const char *filter,int def_filter,
	const char *initdir,mStr *strdst,int *pindex);

#ifdef __cplusplus
}
#endif

#endif
