/*$
mlk
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_SLIDERBAR_H
#define MLK_SLIDERBAR_H

#define MLK_SLIDERBAR(p)  ((mSliderBar *)(p))
#define MLK_SLIDERBAR_DEF mWidget wg; mSliderBarData sl;

typedef void (*mFuncSliderBarHandle_action)(mWidget *p,int pos,uint32_t flags);

typedef struct
{
	uint32_t fstyle;
	int min,max,pos,fgrab;
	mFuncSliderBarHandle_action action;
}mSliderBarData;

struct _mSliderBar
{
	mWidget wg;
	mSliderBarData sl;
};

enum MSLIDERBAR_STYLE
{
	MSLIDERBAR_S_HORZ  = 0,
	MSLIDERBAR_S_VERT  = 1<<0,
	MSLIDERBAR_S_SMALL = 1<<1
};

enum MSLIDERBAR_NOTIFY
{
	MSLIDERBAR_N_ACTION
};

enum MSLIDERBAR_ACTION_FLAGS
{
	MSLIDERBAR_ACTION_F_CHANGE_POS = 1<<0,
	MSLIDERBAR_ACTION_F_KEY_PRESS  = 1<<1,
	MSLIDERBAR_ACTION_F_BUTTON_PRESS = 1<<2,
	MSLIDERBAR_ACTION_F_DRAG         = 1<<3,
	MSLIDERBAR_ACTION_F_BUTTON_RELEASE = 1<<4,
	MSLIDERBAR_ACTION_F_SCROLL     = 1<<5
};


#ifdef __cplusplus
extern "C" {
#endif

mSliderBar *mSliderBarNew(mWidget *parent,int size,uint32_t fstyle);
mSliderBar *mSliderBarCreate(mWidget *parent,int id,uint32_t flayout,uint32_t margin_pack,uint32_t fstyle);
void mSliderBarDestroy(mWidget *wg);

void mSliderBarHandle_draw(mWidget *p,mPixbuf *pixbuf);
int mSliderBarHandle_event(mWidget *wg,mEvent *ev);

int mSliderBarGetPos(mSliderBar *p);
void mSliderBarSetStatus(mSliderBar *p,int min,int max,int pos);
void mSliderBarSetRange(mSliderBar *p,int min,int max);
mlkbool mSliderBarSetPos(mSliderBar *p,int pos);

#ifdef __cplusplus
}
#endif

#endif
