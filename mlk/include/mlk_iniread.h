/*$
mlk
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_INIREAD_H
#define MLK_INIREAD_H

#ifdef __cplusplus
extern "C" {
#endif

mlkerr mIniRead_loadFile(mIniRead **dst,const char *filename);
mlkerr mIniRead_loadFile_join(mIniRead **dst,const char *path,const char *filename);
void mIniRead_end(mIniRead *p);

mlkbool mIniRead_isEmpty(mIniRead *p);
void mIniRead_setEmpty(mIniRead *p);

mlkbool mIniRead_setGroup(mIniRead *p,const char *name);
mlkbool mIniRead_setGroupNo(mIniRead *p,int no);
const char *mIniRead_setGroupTop(mIniRead *p);
const char *mIniRead_setGroupNext(mIniRead *p);

int mIniRead_getGroupItemNum(mIniRead *p);
mlkbool mIniRead_groupIsHaveKey(mIniRead *p,const char *key);

mlkbool mIniRead_getNextItem(mIniRead *p,const char **key,const char **param);
mlkbool mIniRead_getNextItem_keyno_int32(mIniRead *p,int *keyno,void *buf,mlkbool hex);

int mIniRead_getInt(mIniRead *p,const char *key,int def);
uint32_t mIniRead_getHex(mIniRead *p,const char *key,uint32_t def);
mlkbool mIniRead_compareText(mIniRead *p,const char *key,const char *comptxt,mlkbool iscase);

const char *mIniRead_getText(mIniRead *p,const char *key,const char *def);
char *mIniRead_getText_dup(mIniRead *p,const char *key,const char *def);
int mIniRead_getTextBuf(mIniRead *p,const char *key,char *buf,uint32_t size,const char *def);
mlkbool mIniRead_getTextStr(mIniRead *p,const char *key,mStr *str,const char *def);
mlkbool mIniRead_getTextStr_keyno(mIniRead *p,int keyno,mStr *str,const char *def);

void mIniRead_getTextStrArray(mIniRead *p,int keytop,mStr *array,int arraynum);
int mIniRead_getNumbers(mIniRead *p,const char *key,void *buf,int maxnum,int bytes,mlkbool hex);
void *mIniRead_getNumbers_alloc(mIniRead *p,const char *key,int bytes,mlkbool hex,int append_bytes,uint32_t *psize);
mlkbool mIniRead_getPoint(mIniRead *p,const char *key,mPoint *pt,int defx,int defy);
mlkbool mIniRead_getSize(mIniRead *p,const char *key,mSize *size,int defw,int defh);
mlkbool mIniRead_getBox(mIniRead *p,const char *key,mBox *box,int defx,int defy,int defw,int defh);
void *mIniRead_getBase64(mIniRead *p,const char *key,uint32_t *psize);

#ifdef __cplusplus
}
#endif

#endif
