/*$
mlk
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_LINEEDIT_H
#define MLK_LINEEDIT_H

#define MLK_LINEEDIT(p)  ((mLineEdit *)(p))
#define MLK_LINEEDIT_DEF mWidget wg; mLineEditData le;

typedef struct
{
	mWidgetTextEdit dat;
	mWidgetLabelText label;
	uint32_t fstyle;
	int scrx,
		pos_last,
		num_min, num_max, num_dig,
		spin_val,
		fpress;
}mLineEditData;

struct _mLineEdit
{
	mWidget wg;
	mLineEditData le;
};


enum MLINEEDIT_STYLE
{
	MLINEEDIT_S_NOTIFY_CHANGE = 1<<0,
	MLINEEDIT_S_NOTIFY_ENTER  = 1<<1,
	MLINEEDIT_S_READ_ONLY = 1<<2,
	MLINEEDIT_S_SPIN     = 1<<3,
	MLINEEDIT_S_NO_FRAME = 1<<4
};

enum MLINEEDIT_NOTIFY
{
	MLINEEDIT_N_CHANGE,
	MLINEEDIT_N_ENTER
};


#ifdef __cplusplus
extern "C" {
#endif

mLineEdit *mLineEditNew(mWidget *parent,int size,uint32_t fstyle);
mLineEdit *mLineEditCreate(mWidget *parent,int id,uint32_t flayout,uint32_t margin_pack,uint32_t fstyle);
void mLineEditDestroy(mWidget *p);

void mLineEditHandle_calcHint(mWidget *p);
void mLineEditHandle_draw(mWidget *p,mPixbuf *pixbuf);
int mLineEditHandle_event(mWidget *wg,mEvent *ev);

void mLineEditSetLabelText(mLineEdit *p,const char *text,int copy);
void mLineEditSetNumStatus(mLineEdit *p,int min,int max,int dig);
void mLineEditSetWidth_textlen(mLineEdit *p,int len);
void mLineEditSetSpinValue(mLineEdit *p,int val);
void mLineEditSetReadOnly(mLineEdit *p,int type);

void mLineEditSetText(mLineEdit *p,const char *text);
void mLineEditSetText_utf32(mLineEdit *p,const mlkuchar *text);
void mLineEditSetInt(mLineEdit *p,int n);
void mLineEditSetNum(mLineEdit *p,int num);
void mLineEditSetDouble(mLineEdit *p,double d,int dig);

mlkbool mLineEditIsEmpty(mLineEdit *p);
void mLineEditGetTextStr(mLineEdit *p,mStr *str);
int mLineEditGetInt(mLineEdit *p);
double mLineEditGetDouble(mLineEdit *p);
int mLineEditGetNum(mLineEdit *p);

void mLineEditSelectAll(mLineEdit *p);
void mLineEditMoveCursor_toRight(mLineEdit *p);

#ifdef __cplusplus
}
#endif

#endif
