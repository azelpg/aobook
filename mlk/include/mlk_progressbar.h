/*$
mlk
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_PROGRESSBAR_H
#define MLK_PROGRESSBAR_H

#define MLK_PROGRESSBAR(p)  ((mProgressBar *)(p))
#define MLK_PROGRESSBAR_DEF mWidget wg; mProgressBar pb;

typedef struct
{
	char *text;
	uint32_t fstyle;
	int min, max, pos,
		sub_step, sub_max, sub_toppos,
		sub_curcnt, sub_curstep, sub_nextcnt;
}mProgressBarData;

struct _mProgressBar
{
	mWidget wg;
	mProgressBarData pb;
};

enum MPROGRESSBAR_STYLE
{
	MPROGRESSBAR_S_FRAME = 1<<0,
	MPROGRESSBAR_S_TEXT  = 1<<1,
	MPROGRESSBAR_S_TEXT_PERS = 1<<2,
};


#ifdef __cplusplus
extern "C" {
#endif

mProgressBar *mProgressBarNew(mWidget *parent,int size,uint32_t fstyle);
mProgressBar *mProgressBarCreate(mWidget *parent,int id,uint32_t flayout,uint32_t margin_pack,uint32_t fstyle);
void mProgressBarDestroy(mWidget *p);

void mProgressBarHandle_calcHint(mWidget *p);
void mProgressBarHandle_draw(mWidget *p,mPixbuf *pixbuf);

void mProgressBarSetStatus(mProgressBar *p,int min,int max,int pos);
void mProgressBarSetText(mProgressBar *p,const char *text);
mlkbool mProgressBarSetPos(mProgressBar *p,int pos);
void mProgressBarIncPos(mProgressBar *p);
mlkbool mProgressBarAddPos(mProgressBar *p,int add);

void mProgressBarSubStep_begin(mProgressBar *p,int num,int maxcnt);
void mProgressBarSubStep_begin_onestep(mProgressBar *p,int num,int maxcnt);
mlkbool mProgressBarSubStep_inc(mProgressBar *p);

#ifdef __cplusplus
}
#endif

#endif
