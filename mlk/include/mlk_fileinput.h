/*$
mlk
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_FILEINPUT_H
#define MLK_FILEINPUT_H

#define MLK_FILEINPUT(p)  ((mFileInput *)(p))
#define MLK_FILEINPUT_DEF MLK_CONTAINER_DEF mFileInputData fi;

typedef struct
{
	mLineEdit *edit;
	mButton *btt;
	mStr str_filter,
		str_initdir;
	int filter_def;
	uint32_t fstyle;
}mFileInputData;

struct _mFileInput
{
	MLK_CONTAINER_DEF
	mFileInputData fi;
};


enum MFILEINPUT_STYLE
{
	MFILEINPUT_S_DIRECTORY = 1<<0,
	MFILEINPUT_S_READ_ONLY = 1<<1
};

enum MFILEINPUT_NOTIFY
{
	MFILEINPUT_N_CHANGE
};


#ifdef __cplusplus
extern "C" {
#endif

mFileInput *mFileInputNew(mWidget *parent,int size,uint32_t fstyle);
mFileInput *mFileInputCreate_file(mWidget *parent,int id,uint32_t flayout,uint32_t margin_pack,uint32_t fstyle,
	const char *filter,int filter_def,const char *initdir);
mFileInput *mFileInputCreate_dir(mWidget *parent,int id,uint32_t flayout,uint32_t margin_pack,uint32_t fstyle,
	const char *path);

void mFileInputDestroy(mWidget *p);
int mFileInputHandle_event(mWidget *wg,mEvent *ev);

void mFileInputSetFilter(mFileInput *p,const char *filter,int def);
void mFileInputSetInitDir(mFileInput *p,const char *dir);
void mFileInputSetPath(mFileInput *p,const char *path);
void mFileInputGetPath(mFileInput *p,mStr *str);

#ifdef __cplusplus
}
#endif

#endif
