/*$
mlk
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_ARROWBUTTON_H
#define MLK_ARROWBUTTON_H

#include "mlk_button.h"

#define MLK_ARROWBUTTON(p)  ((mArrowButton *)(p))
#define MLK_ARROWBUTTON_DEF MLK_BUTTON_DEF mArrowButtonData arrbtt;

typedef struct
{
	uint32_t fstyle;
}mArrowButtonData;

struct _mArrowButton
{
	MLK_BUTTON_DEF
	mArrowButtonData arrbtt;
};

enum MARROWBUTTON_STYLE
{
	MARROWBUTTON_S_DOWN  = 1<<0,
	MARROWBUTTON_S_UP    = 1<<1,
	MARROWBUTTON_S_LEFT  = 1<<2,
	MARROWBUTTON_S_RIGHT = 1<<3,
	MARROWBUTTON_S_FONTSIZE = 1<<4,
	MARROWBUTTON_S_DIRECT_PRESS = 1<<5
};


#ifdef __cplusplus
extern "C" {
#endif

mArrowButton *mArrowButtonNew(mWidget *parent,int size,uint32_t fstyle);
mArrowButton *mArrowButtonCreate(mWidget *parent,int id,uint32_t flayout,uint32_t margin_pack,uint32_t fstyle);
mArrowButton *mArrowButtonCreate_minsize(mWidget *parent,int id,uint32_t flayout,uint32_t margin_pack,uint32_t fstyle,int size);

void mArrowButtonDestroy(mWidget *p);

void mArrowButtonHandle_calcHint(mWidget *p);
void mArrowButtonHandle_draw(mWidget *p,mPixbuf *pixbuf);

#ifdef __cplusplus
}
#endif

#endif
