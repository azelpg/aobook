/*$
mlk
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_PAGER_H
#define MLK_PAGER_H

#define MLK_PAGER(p)  ((mPager *)(p))
#define MLK_PAGER_DEF MLK_CONTAINER_DEF mPagerData pg;

typedef struct _mPagerInfo mPagerInfo;

typedef mlkbool (*mFuncPagerCreate)(mPager *p,mPagerInfo *);
typedef mlkbool (*mFuncPagerSetData)(mPager *p,void *pagedat,void *src);
typedef mlkbool (*mFuncPagerGetData)(mPager *p,void *pagedat,void *dst);
typedef void (*mFuncPagerDestroy)(mPager *p,void *pagedat);
typedef int (*mFuncPagerEvent)(mPager *p,mEvent *ev,void *pagedat);

struct _mPagerInfo
{
	void *pagedat;
	mFuncPagerSetData setdata;
	mFuncPagerGetData getdata;
	mFuncPagerDestroy destroy;
	mFuncPagerEvent event;
};

typedef struct
{
	mPagerInfo info;
	void *data;
}mPagerData;

struct _mPager
{
	MLK_CONTAINER_DEF
	mPagerData pg;
};


#ifdef __cplusplus
extern "C" {
#endif

mPager *mPagerNew(mWidget *parent,int size);
mPager *mPagerCreate(mWidget *parent,uint32_t flayout,uint32_t margin_pack);

void mPagerDestroy(mWidget *wg);
int mPagerHandle_event(mWidget *wg,mEvent *ev);

void *mPagerGetDataPtr(mPager *p);
void mPagerSetDataPtr(mPager *p,void *data);
mlkbool mPagerGetPageData(mPager *p);
mlkbool mPagerSetPageData(mPager *p);

void mPagerDestroyPage(mPager *p);
mlkbool mPagerSetPage(mPager *p,mFuncPagerCreate create);

#ifdef __cplusplus
}
#endif

#endif
