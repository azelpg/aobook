/*$
mlk
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_CLIPBOARD_H
#define MLK_CLIPBOARD_H

typedef struct _mClipboardSendData mClipboardSendData;

typedef int (*mFuncClipboardSend)(mClipboardSendData *p);
typedef int (*mFuncClipboardRecv)(void *buf,uint32_t size,void *param);

struct _mClipboardSendData
{
	const char *mimetype;
	void *buf;
	int type;
	uint32_t size;
};

enum MCLIPBOARD_TYPE
{
	MCLIPBOARD_TYPE_DATA,
	MCLIPBOARD_TYPE_TEXT,
	MCLIPBOARD_TYPE_USER = 1000
};

enum MCLIPBOARD_RET
{
	MCLIPBOARD_RET_OK,
	MCLIPBOARD_RET_NONE,
	MCLIPBOARD_RET_SEND_RAW,
	MCLIPBOARD_RET_ERR,
	MCLIPBOARD_RET_ERR_TYPE
};


#ifdef __cplusplus
extern "C" {
#endif

mlkbool mClipboardRelease(void);
mlkbool mClipboardSetData(int type,const void *buf,uint32_t size,const char *mimetypes,mFuncClipboardSend handle);
mlkbool mClipboardSetText(const char *text,int len);
mlkbool mClipboardSend(const void *buf,int size);

int mClipboardGetText(mStr *str);
int mClipboardGetData(const char *mimetype,mFuncClipboardRecv handle,void *param);

char **mClipboardGetMimeTypeList(void);
char *mClipboardFindMimeType(const char *mime_types);

#ifdef __cplusplus
}
#endif

#endif
