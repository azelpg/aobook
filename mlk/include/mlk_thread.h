/*$
mlk
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_THREAD_H
#define MLK_THREAD_H

struct _mThread
{
	intptr_t id;
	void (*run)(mThread *);
	void *param;
	mThreadMutex mutex;
	mThreadCond cond;
};

#ifdef __cplusplus
extern "C" {
#endif

mThread *mThreadNew(int size,void (*run)(mThread *),void *param);
void mThreadDestroy(mThread *p);
mlkbool mThreadRun(mThread *p);
mlkbool mThreadWait(mThread *p);

mThreadMutex mThreadMutexNew(void);
void mThreadMutexDestroy(mThreadMutex p);
void mThreadMutexLock(mThreadMutex p);
void mThreadMutexUnlock(mThreadMutex p);

mThreadCond mThreadCondNew(void);
void mThreadCondDestroy(mThreadCond p);
void mThreadCondSignal(mThreadCond p);
void mThreadCondBroadcast(mThreadCond p);
void mThreadCondWait(mThreadCond p,mThreadMutex mutex);

#ifdef __cplusplus
}
#endif

#endif
