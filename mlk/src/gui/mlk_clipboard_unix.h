/*$
mlk
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_CLIPBOARD_UNIX_H
#define MLK_CLIPBOARD_UNIX_H

typedef struct
{
	void *dat_buf;	//自身が持っているクリップボードデータ (NULL でなし)
	int dat_type;
	uint32_t dat_size;

	mList list_mime;	//持っているデータが送信可能な MIME タイプのリスト
	
	mFuncEmpty sendfunc;	//データを送る時の関数 (NULL でタイプごとのデフォルト)

	void *send_data;		//[送信中] バックエンドごとのデータ
	uint8_t send_is_self;	//[送信中] 自身に対する送信か (TRUE の場合、send_data は mBuf *)

	//各バックエンド用関数
	void (*setmimetype_default)(mList *,int);
	void (*addmimetype)(mList *,const char *);
	mlkbool (*setselection)(void);
	mlkbool (*sendto)(const void *,int);
	int (*gettext)(mStr *);
	int (*getdata)(const char *,mFuncEmpty,void *);
	char **(*getmimetypelist)(void);
}mClipboardUnix;


void mClipboardUnix_destroy(mClipboardUnix *p);
void mClipboardUnix_freeData(mClipboardUnix *p);

mlkbool mClipboardUnix_setData(mClipboardUnix *p,
	int type,const void *buf,uint32_t size,const char *mimetypes,mFuncEmpty handle);

mlkbool mClipboardUnix_send(mClipboardUnix *p,const void *buf,int size);

int mClipboardUnix_getText(mClipboardUnix *p,mStr *str);
int mClipboardUnix_getData(mClipboardUnix *p,const char *mimetype,mFuncEmpty handle,void *param);

char **mClipboardUnix_getMimeTypeList(mClipboardUnix *p);
char *mClipboardUnix_findMimeType(mClipboardUnix *p,const char *mime_types);

#endif
