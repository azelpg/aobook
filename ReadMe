**********************************************
  aobook
  Copyright (C) 2014-2022 Azel

  http://azsky2.html.xdomain.jp/
  https://gitlab.com/azelpg/aobook
  <azelpg@gmail.com>
**********************************************


このソフトウェアは MIT ライセンスです。
詳しくは COPYING ファイルをご覧ください。

kenten.otf は、"源ノ明朝" のグリフを改変して作成した圏点用フォントです。


=====================================
 * 概要
=====================================

青空文庫テキストビューアです。

- FreeType による縦書き表示で描画します。

- Shift-JIS/EUC-JP/UTF-8/UTF-16BE/UTF-16LE のテキストの読み込みに対応しています。

- 一般的な青空文庫の注記に対応しています。

- ZIP で圧縮されたテキストファイルを読み込むことができます。
  また、ZIP 内の画像を挿絵として表示することもできます。

- しおりを付けることができます。

- ツール (外部コマンドの実行) によって、
  現在のページの行番号位置を指定してテキストエディタ等を開くことができるため、
  編集時の確認用としても使えます。


=====================================
 * ver 1.xx からの変更点
=====================================

設定ファイルの保存先が、"~/.aobook" から "~/.config/aobook" に変更されました。

以前の設定ファイルの継承は行われないため、新たに設定し直してください。

以前の設定ファイルを新しいディレクトリにコピーしてきても、使用できません。

なお、ローカルしおりのテキストファイルに関しては、
前バージョンで保存したものをそのまま読み込むことができます。


=====================================
 * 動作環境
=====================================

- Linux、macOS(要XQuartz) ほか
- X11R6 以降


=====================================
 * コンパイルに必要なもの
=====================================

- C11 対応の C コンパイラ
- ninja (ビルドツール)
- pkg-config
- 以下のライブラリの開発用ファイル
  x11 xext xcursor zlib libpng jpeglib(jpeg-turbo) fontconfig freetype2
  Linux 以外: libiconv


---- 必要なパッケージ ----

※パッケージ名は変化している場合があります。

(Debian/Ubuntu 系)

  gcc or clang, ninja-build pkg-config
  libx11-dev libxext-dev libxcursor-dev
  libfreetype6-dev libfontconfig1-dev zlib1g-dev libpng-dev libjpeg-dev

(RedHat 系)

  gcc or clang, ninja-build pkg-config
  libX11-devel libXext-devel libXcursor-devel
  libfreetype6-devel libfontconfig-devel zlib-devel libpng-devel libjpeg-devel

(Arch Linux)

  gcc or clang, ninja pkgconf
  libx11 libxext libxcursor freetype2 fontconfig zlib libpng libjpeg-turbo
  
  [*] ライブラリ等は、GUI 環境がインストールされていれば、ほぼインストールされています。

(macOS)

  xcode ninja pkg-config libpng jpeg-turbo

  [*] あらかじめ XQuartz がインストールされていることが前提です。


=====================================
 * コンパイル/インストール
=====================================

ソースアーカイブの展開後、展開したディレクトリ内に移動して、以下を実行してください。
build ディレクトリ内に、ファイルが生成されます。

$ ./configure
$ cd build
$ ninja
# ninja install


- デフォルトのインストール先は、/usr/local です。
- "$ ./configure --help" でヘルプを表示できます。

## インストール先を /usr にする
$ ./configure --prefix=/usr

## アンインストール (build/ 内で)
# ninja uninstall

## パッケージファイル用にファイルを生成 (build/ 内で)
$ DESTDIR=<DIR> ninja install


=====================================
 * ファイル
=====================================

<< インストールされるファイル >>

<INSTDIR>/bin/aobook : 実行ファイル
<INSTDIR>/share
  |- doc/aobook/* : ドキュメントファイル
  |- applications
      |- aobook.desktop : デスクトップファイル
  |- icons/hicolor : アイコンファイル
      |- 48x48/apps/aobook.png
      |- scalable/apps/aobook.svg
  |- aobook : アプリで使用されるファイル
      |- kenten.otf : 圏点(傍点)用フォント


<< 作成されるファイル >>

"~/.config/aobook" ディレクトリが作成され、ここに各設定ファイルが保存されます。


=====================================
 * 使い方
=====================================

デスクトップのメインメニューにショートカットが作成されるので、
そこから起動できます。
(ショートカットはすぐに反映されない場合もあります)

端末から起動する場合は、以下のコマンドで実行します。

$ aobook


=====================================
 * mlk について
=====================================

GUI の構築には、自作のライブラリ mlk を使用しています。

GUI のフォントや色は、"az-mlk-style.conf" の設定ファイルで指定することができます。

詳しくは、"about-mlk.txt" をご覧ください。

